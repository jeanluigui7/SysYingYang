﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using CapaEntidades;
namespace CapaAccesoDatos
{
    public class datProducto
    {
        private static readonly datProducto _instancia = new datProducto();
        public static datProducto Instancia
        {
            get { return datProducto._instancia; }
        }

        public List<entProducto> ListarProductosxCategoria(int idcategoria)
        {
            List<entProducto> lista = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = new SqlCommand("spListarProductosxCategoria", Conexion.GetConnection());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idcategoria", idcategoria);
                dr = cmd.ExecuteReader();
                lista = new List<entProducto>();
                while (dr.Read())
                {
                    entProducto objpro = new entProducto();
                    objpro.ProId = Convert.ToInt32(dr["ProId"]);

                    entCategoria c = new entCategoria();
                    c.CatId = Convert.ToInt32(dr["CatId"]);
                    c.CatNombre = Convert.ToString(dr["DCategoria"]);
                    objpro.Categoria = c; 
                     
                    objpro.ProCodigo = Convert.ToString(dr["ProCodigo"]);
                    objpro.ProNombre = Convert.ToString(dr["ProNombre"]);
                    objpro.ProDescripcion = Convert.ToString(dr["ProDescripcion"]);
                    objpro.ProStock = Convert.ToInt32(dr["ProStock"]);
                    objpro.ProImagen = Convert.ToString(dr["ProImagen"]);
                    objpro.Precio = Convert.ToDecimal(dr["Precio"]);

                    lista.Add(objpro);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { Conexion.DisposeCommand(cmd); }
            return lista;
        }
        public List<entProducto> ListarProductos()
        {
            List<entProducto> lista = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = new SqlCommand("spListarProductos", Conexion.GetConnection());
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                lista = new List<entProducto>();
                while (dr.Read())
                {
                    entProducto objpro = new entProducto();
                    objpro.ProId = Convert.ToInt32(dr["ProId"]);

                    entCategoria c = new entCategoria();
                    c.CatId = Convert.ToInt32(dr["CatId"]);
                    c.CatNombre = Convert.ToString(dr["DCategoria"]);
                    objpro.Categoria = c;

                    objpro.ProCodigo = Convert.ToString(dr["ProCodigo"]);
                    objpro.ProNombre = Convert.ToString(dr["ProNombre"]);
                    objpro.ProDescripcion = Convert.ToString(dr["ProDescripcion"]);
                    objpro.ProStock = Convert.ToInt32(dr["ProStock"]);
                    objpro.ProImagen = Convert.ToString(dr["ProImagen"]);
                    objpro.Precio = Convert.ToDecimal(dr["Precio"]);

                    lista.Add(objpro);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { Conexion.DisposeCommand(cmd); }
            return lista; 
        }

        public entProducto ListarProductosId(int id)
        {
            entProducto objpro = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = new SqlCommand("spListarProductosxId", Conexion.GetConnection());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdProducto", id);
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    objpro = new entProducto();
                    objpro.ProId = Convert.ToInt32(dr["ProId"]);
                    objpro.ProCodigo = Convert.ToString(dr["ProCodigo"]);
                    objpro.ProNombre = Convert.ToString(dr["ProNombre"]);
                    objpro.ProDescripcion = Convert.ToString(dr["ProDescripcion"]);
                    objpro.ProStock = Convert.ToInt32(dr["ProStock"]);
                    objpro.ProImagen = Convert.ToString(dr["ProImagen"]);
                    objpro.Precio = Convert.ToDecimal(dr["Precio"]);
                    objpro.ProEstado = Convert.ToInt32(dr["ProEstado"]);
                   
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { Conexion.DisposeCommand(cmd); }
            return objpro;
        }
    }
}
