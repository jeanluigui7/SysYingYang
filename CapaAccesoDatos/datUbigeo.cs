﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


namespace CapaAccesoDatos
{
    public class datUbigeo
    {
        private static readonly datUbigeo _instance = new datUbigeo();
        public static datUbigeo Instance
        {
            get { return datUbigeo._instance; }
        }

        public List<DataTable> ListarUbigeo()
        {
            SqlDataAdapter ad = null;
            DataSet ds = new DataSet();
            List<DataTable> lstDT = null;
            try
            {
                ad = new SqlDataAdapter("spListarUbigeo", Conexion.GetConnection());
                ad.SelectCommand.CommandType = CommandType.StoredProcedure;
                ad.Fill(ds);
                lstDT = new List<DataTable>();
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    DataTable dt = ds.Tables[i];
                    switch (dt.TableName)
                    {
                        case "Table":
                            dt.TableName = "Paises";
                            break;

                        case "Table1":
                            dt.TableName = "Departamentos";
                            break;

                        case "Table2":
                            dt.TableName = "Provincias";
                            break;

                        case "Table3":
                            dt.TableName = "Distritos";
                            break;
                    }
                    lstDT.Add(dt);
                }

            }
            catch (Exception ex)
            {
                lstDT = null;
                throw ex;
            }
            finally
            {
                ad.SelectCommand.Connection.Close();
            }
            return lstDT;
        }
    }
}
