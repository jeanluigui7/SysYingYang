﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using CapaEntidades;
namespace CapaAccesoDatos
{
    public class datPedido
    {
        private static readonly datPedido _instancia = new datPedido();
        public static datPedido Instancia { get { return datPedido._instancia; } }

        public int Insertar_Pedido(entPedido p, DataTable detail)
        {
            SqlCommand cmd = null;
            int i = 0;
            try
            {
                cmd = new SqlCommand("SP_INSERTAR_PEDIDO", Conexion.GetConnection());
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter parmIdPedidoOut = cmd.Parameters.Add("@PedidoId", SqlDbType.Int);
                parmIdPedidoOut.Direction = ParameterDirection.Output;
                cmd.Parameters.AddWithValue("@CliId", p.CliId);
                cmd.Parameters.AddWithValue("@Total", p.Total);
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@TY_DetallePedido", Value = detail, SqlDbType = SqlDbType.Structured, TypeName = "TY_DetallePedido" });
                i = cmd.ExecuteNonQuery();
                
            }
            catch (Exception ex)
            {
                i = 0;
                throw ex;
            }
            finally {
                Conexion.DisposeCommand(cmd);
            }
            return i;
        }
    }
}
