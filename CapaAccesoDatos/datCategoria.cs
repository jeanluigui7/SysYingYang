﻿using CapaEntidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaAccesoDatos
{
    public class datCategoria
    {
        private static readonly datCategoria _instancia = new datCategoria();
        public static datCategoria Instancia
        {
            get { return datCategoria._instancia; }
        }

        public List<entCategoria> ListaCategoria()
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<entCategoria> lista = null;
            try
            {
                cmd = new SqlCommand("spListaCategorias", Conexion.GetConnection());
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                lista = new List<entCategoria>();
                while (dr.Read())
                {
                    entCategoria objCat = new entCategoria();
                    objCat.CatId = Convert.ToInt32(dr["CatId"]);
                    objCat.CatCodigo = Convert.ToString(dr["CatCodigo"]);
                    objCat.CatNombre = Convert.ToString(dr["CatNombre"]);

                    lista.Add(objCat);
                }
          
            }
            catch (Exception ex)
            {throw ex;}
            finally { Conexion.DisposeCommand(cmd); }
            return lista;
        }
    }
}
