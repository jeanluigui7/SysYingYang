﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CapaAccesoDatos
{
    public class Conexion
    {
        //private static readonly Conexion _instancia = new Conexion();
        //public static Conexion Instancia
        //{
        //    get { return Conexion._instancia; }
        //}

        //public SqlConnection Conectar()
        //{
        //    try
        //    {
        //        SqlConnection cn = new SqlConnection();
        //        cn.ConnectionString = "Data source=.;Initial Catalog=BD_YINYANG;User ID=sa; Password=12345678";
        //        return cn;
        //    }
        //    catch (Exception ex)
        //    { throw ex;}

        //}
        public static SqlConnection GetConnection()
        {
            String connString = "";
            if (ConfigurationManager.ConnectionStrings["yingyang_db"] != null)
            {
                connString = ConfigurationManager.ConnectionStrings["yingyang_db"].ConnectionString;
            }
            SqlConnection objconexion = new SqlConnection(connString);
            objconexion.Open();
            return objconexion;
        }
        public static void DisposeCommand(SqlCommand cmd)
        {
            try
            {
                if (cmd != null)
                {
                    if (cmd.Connection != null)
                    {
                        cmd.Connection.Close();
                        cmd.Connection.Dispose();
                    }
                    cmd.Dispose();
                }
            }
            catch { } //don't blow up
        }
    }
}
