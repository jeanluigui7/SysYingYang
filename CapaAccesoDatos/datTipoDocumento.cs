﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidades;
using System.Data;
using System.Data.SqlClient;
namespace CapaAccesoDatos
{
    public class datTipoDocumento
    {
        private static readonly datTipoDocumento _instancia = new datTipoDocumento();
        public static datTipoDocumento Instancia
        {
            get { return datTipoDocumento._instancia; }
        }

        public DataTable ListarTiposDocumento()
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = new SqlCommand("spListarTipoDocumentos", Conexion.GetConnection());
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Conexion.DisposeCommand(cmd);
            }
            return dt;
        }

    }
}
