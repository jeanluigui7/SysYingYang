﻿using Librarys;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaAccesoDatos
{
    public class datDireccion
    {
        private static readonly datDireccion _instancia = new datDireccion();
        public static datDireccion Instancia
        {
            get { return datDireccion._instancia; }
        }

        public DataTable ListarDireccionByClientId(ref BaseEntity Base, Int32 IdDireccion) {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = new SqlCommand("sp_ListarDireccionByClientId", Conexion.GetConnection());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DirId", IdDireccion);
                dr = cmd.ExecuteReader();
                if (dr.HasRows) {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
                Base.Errors.Add(new BaseEntity.ListError(ex, ex.Message));
            }
            finally
            {
                Conexion.DisposeCommand(cmd);
            }
            return dt;
        }

    }
}
