﻿using System;
using System.Data;
using System.Data.SqlClient;

using CapaEntidades;
using Librarys;

namespace CapaAccesoDatos
{
    public class datCliente
    {
        private static readonly datCliente _instancia = new datCliente();
        public static datCliente Instancia
        {
            get { return datCliente._instancia; }
        }

        public Boolean RegistrarCliente(entCliente obj, entDireccion objD)
        {
            SqlCommand cmd = null;
            Boolean success = false;
            try
            {
                cmd = new SqlCommand("spRegistrarClientes", Conexion.GetConnection());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DirDescripcion1", objD.DirDescripcion1);
                cmd.Parameters.AddWithValue("@DirDescripcion2", objD.DirDescripcion2);
                cmd.Parameters.AddWithValue("@DirCodPostal", objD.DirCodPostal);
                cmd.Parameters.AddWithValue("@DirEstado", objD.DirEstado);

                SqlParameter IdDirOut = cmd.Parameters.Add("@NewIdDir", SqlDbType.Int);
                IdDirOut.Direction = ParameterDirection.Output;

                cmd.Parameters.AddWithValue("@PaisId", obj.PaisId);
                cmd.Parameters.AddWithValue("@DistId", obj.DistId);
                cmd.Parameters.AddWithValue("@ProvId", obj.ProvId);
                cmd.Parameters.AddWithValue("@DepId", obj.DepId);
                cmd.Parameters.AddWithValue("@CliNombre", obj.CliNombre);
                cmd.Parameters.AddWithValue("@CliApellidoP", obj.CliApellidoP);
                cmd.Parameters.AddWithValue("@CliApellidoM", obj.CliApellidoM);
                cmd.Parameters.AddWithValue("@CliTelefono", obj.CliTelefono);
                cmd.Parameters.AddWithValue("@CliCelular", obj.CliCelular);
                cmd.Parameters.AddWithValue("@CliEmail", obj.CliEmail);
                cmd.Parameters.AddWithValue("@CliPassword", obj.CliPassword);
                cmd.Parameters.AddWithValue("@CliFoto", obj.CliFoto);

                cmd.Parameters.AddWithValue("@CliUsuarioRegistro", obj.CliUsuarioRegistro);

                cmd.Parameters.AddWithValue("@CliUsuarioModificacion", obj.CliUsuarioModificacion);
                cmd.Parameters.AddWithValue("@CliEstado", obj.CliEstado);

                if (cmd.ExecuteNonQuery() == 2)
                {
                    success = true;
                }

            }
            catch (Exception ex)
            {
                success = false;
                throw ex;
            }
            finally
            {
                Conexion.DisposeCommand(cmd);
            }
            return success;
        }

        public entCliente AccesoCliente(string usu, string pass)
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            entCliente c = null;
            try
            {
                cmd = new SqlCommand("spAccesoCliente", Conexion.GetConnection());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CliEmail", usu);
                cmd.Parameters.AddWithValue("@CliPassword", pass);
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    c = new entCliente();
                    c.CliId = Convert.ToInt32(dr["CliId"]);
                    c.CliNombre = dr["CliNombre"].ToString();
                    c.CliApellidoP = dr["CliApellidoP"].ToString();
                    c.CliApellidoM = dr["CliApellidoM"].ToString();
                    c.CliCelular = dr["CliCelular"].ToString();
                    c.CliEmail = dr["CliEmail"].ToString();
                    c.CliFoto = dr["CliFoto"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Conexion.DisposeCommand(cmd);
            }
            return c;
        }

        public DataTable ListarCliente_ById(ref BaseEntity Base, Int32 IdCliente, Int32 Estado)
        {
            SqlCommand cmd = null;
            DataTable dt = null;
            SqlDataReader dr = null; 
            try
            {
                cmd = new SqlCommand("spObtenerCliente", Conexion.GetConnection());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdCliente", IdCliente);
                cmd.Parameters.AddWithValue("@Estado", Estado);

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
                Base.Errors.Add(new BaseEntity.ListError(ex, ex.Message));
            }
            finally
            {
                Conexion.DisposeCommand(cmd);
            }
            return dt;
        }
    }
}
