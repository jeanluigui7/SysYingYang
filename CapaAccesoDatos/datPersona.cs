﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using CapaEntidades;
using Library;

namespace CapaAccesoDatos
{
    public class datPersona
    {
        private static readonly datPersona _instance = new datPersona();
        public static datPersona Intancia
        {
            get { return datPersona._instance; }
        }
        public DataTable ListarPersonas(Int32 status)
        {
            DataTable dt = null;
            SqlDataReader dr = null;
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand("spListarPersonas", Conexion.GetConnection());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Estado", status);
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Conexion.DisposeCommand(cmd);
            }
            return dt;
        }

        public Boolean RegistrarPersonas(entPersona objPer, entDireccion objDir)
        {
            Boolean success = false;
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand("spRegistrarPersona", Conexion.GetConnection());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DIRECCION1", objDir.DirDescripcion1);
                cmd.Parameters.AddWithValue("@DIRECCION2", objDir.DirDescripcion2);
                cmd.Parameters.AddWithValue("@CODIGOPOSTAL", objDir.DirCodPostal);
                cmd.Parameters.AddWithValue("@ESTADO", objDir.DirEstado);

                cmd.Parameters.AddWithValue("@IDPAIS", objPer.PaisId);
                cmd.Parameters.AddWithValue("@IDDISTRITO", objPer.DistId);
                cmd.Parameters.AddWithValue("@IDPROVINCIA", objPer.ProvId);
                cmd.Parameters.AddWithValue("@IDDEPARTAMENTO", objPer.DepId);
                cmd.Parameters.AddWithValue("@IDTIPODOCUMENTO", objPer.TipoDocId);
                cmd.Parameters.AddWithValue("@NUMDOC", objPer.NroDocumento);
                cmd.Parameters.AddWithValue("@NOMBRE", objPer.PerNombre);
                cmd.Parameters.AddWithValue("@APELLIDOPATERNO", objPer.PerApellidoP);
                cmd.Parameters.AddWithValue("@APELLIDOMATERNO", objPer.PerApellidoM);
                cmd.Parameters.AddWithValue("@EDAD", objPer.PerEdad);
                cmd.Parameters.AddWithValue("@SEXO", objPer.PerSexo);
                cmd.Parameters.AddWithValue("@EMAIL", objPer.PerEmail);
                cmd.Parameters.AddWithValue("@TELEFONO", objPer.PerTelefono);
                cmd.Parameters.AddWithValue("@CELULAR", objPer.PerCelular);
                cmd.Parameters.AddWithValue("@FOTO", objPer.PerFoto);
                cmd.Parameters.AddWithValue("@ESTADOP", objPer.PerEstado);

                if (cmd.ExecuteNonQuery() == 2)
                {
                    success = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Conexion.DisposeCommand(cmd);
            }
            return success;
        }

        public Boolean ActualizarPersona(entPersona objPer, entDireccion objDir)
        {

            Boolean success = false;
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand("spActualizarPersona", Conexion.GetConnection());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPER", objPer.PerId);
                cmd.Parameters.AddWithValue("@IDDIR", objDir.DirId);
                cmd.Parameters.AddWithValue("@DIRECCION1", objDir.DirDescripcion1);
                cmd.Parameters.AddWithValue("@DIRECCION2", objDir.DirDescripcion2);
                cmd.Parameters.AddWithValue("@CODIGOPOSTAL", objDir.DirCodPostal);
                cmd.Parameters.AddWithValue("@ESTADO", objDir.DirEstado);

                cmd.Parameters.AddWithValue("@IDPAIS", objPer.PaisId);
                cmd.Parameters.AddWithValue("@IDDISTRITO", objPer.DistId);
                cmd.Parameters.AddWithValue("@IDPROVINCIA", objPer.ProvId);
                cmd.Parameters.AddWithValue("@IDDEPARTAMENTO", objPer.DepId);
                cmd.Parameters.AddWithValue("@IDTIPODOCUMENTO", objPer.TipoDocId);
                cmd.Parameters.AddWithValue("@NUMDOC", objPer.NroDocumento);
                cmd.Parameters.AddWithValue("@NOMBRE", objPer.PerNombre);
                cmd.Parameters.AddWithValue("@APELLIDOPATERNO", objPer.PerApellidoP);
                cmd.Parameters.AddWithValue("@APELLIDOMATERNO", objPer.PerApellidoM);
                cmd.Parameters.AddWithValue("@EDAD", objPer.PerEdad);
                cmd.Parameters.AddWithValue("@SEXO", objPer.PerSexo);
                cmd.Parameters.AddWithValue("@EMAIL", objPer.PerEmail);
                cmd.Parameters.AddWithValue("@TELEFONO", objPer.PerTelefono);
                cmd.Parameters.AddWithValue("@CELULAR", objPer.PerCelular);
                cmd.Parameters.AddWithValue("@FOTO", objPer.PerFoto);
                cmd.Parameters.AddWithValue("@ESTADOP", objPer.PerEstado);

                if (cmd.ExecuteNonQuery() == 2)
                {
                    success = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Conexion.DisposeCommand(cmd);
            }
            return success;
        }
        public entPersona ListarPersonaxId(Int32 Id)
        {
            entPersona objPer = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            DataTable dt = null;
            try
            {
                cmd = new SqlCommand("spListarPersonasxId", Conexion.GetConnection());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", Id);
                dr = cmd.ExecuteReader();
                objPer = new entPersona();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);

                    foreach (DataRow drw in dt.Rows)
                    {
                        objPer.PerId = Utility.ValidateDataRowKeyDefault(drw, "ID", "");
                        objPer.PerNombre = Utility.ValidateDataRowKeyDefault(drw, "NOMBRE", "");
                        objPer.PerCodigo = Utility.ValidateDataRowKeyDefault(drw, "CODIGO", "");
                        objPer.PerApellidoP = Utility.ValidateDataRowKeyDefault(drw, "APELLIDOPATERNO", "");
                        objPer.PerApellidoM = Utility.ValidateDataRowKeyDefault(drw, "APELLIDOMATERNO", "");
                        objPer.PerEdad = Utility.ValidateDataRowKeyDefault(drw, "EDAD", "");
                        objPer.PerEmail = Utility.ValidateDataRowKeyDefault(drw, "EMAIL", "");
                        objPer.PerSexo = Utility.ValidateDataRowKeyDefault(drw, "SEXO", "");
                        objPer.PerCelular = Utility.ValidateDataRowKeyDefault(drw, "CELULAR", "");
                        objPer.PerTelefono = Utility.ValidateDataRowKeyDefault(drw, "TELEFONO", "");
                        objPer.PerFoto = Utility.ValidateDataRowKeyDefault(drw, "FOTO", "");
                        objPer.TipoDocId = Convert.ToInt32(Utility.ValidateDataRowKeyDefault(drw, "TIPODOCUMENTO", ""));
                        objPer.NroDocumento = Utility.ValidateDataRowKeyDefault(drw, "NRODOCUMENTO", "");
                        objPer.PaisId = Convert.ToInt32(Utility.ValidateDataRowKeyDefault(drw, "PAIS", ""));
                        objPer.DepId = Convert.ToInt32(Utility.ValidateDataRowKeyDefault(drw, "DEPARTAMENTO", ""));
                        objPer.ProvId = Convert.ToInt32(Utility.ValidateDataRowKeyDefault(drw, "PROVINCIA", ""));
                        objPer.DistId = Convert.ToInt32(Utility.ValidateDataRowKeyDefault(drw, "DISTRITO", ""));
                        objPer.PerEstado = Utility.ValidateDataRowKeyDefault(drw, "ESTADO", "");
                        objPer.DirId = Convert.ToInt32(Utility.ValidateDataRowKeyDefault(drw, "DIRECCION", ""));
                        entDireccion objd = new entDireccion();
                        objd.DirId = Convert.ToInt32(Utility.ValidateDataRowKeyDefault(drw, "DIRECCION", ""));
                        objd.DirDescripcion1 = Utility.ValidateDataRowKeyDefault(drw, "DIRECCION1", "");
                        objd.DirDescripcion2 = Utility.ValidateDataRowKeyDefault(drw, "DIRECCION2", "");
                        objd.DirCodPostal = Utility.ValidateDataRowKeyDefault(drw, "CODIGOPOSTAL", "");
                        objPer.Direccion = objd;
                    }
                }
            }
            catch (Exception ex)
            {
                objPer = null;
                throw ex;
            }
            finally
            {
                Conexion.DisposeCommand(cmd);
            }
            return objPer;
        }
    }
}
