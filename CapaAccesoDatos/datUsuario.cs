﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using CapaEntidades;
using Library;
namespace CapaAccesoDatos
{
    public class datUsuario
    {
        private static readonly datUsuario _instance = new datUsuario();
        public static datUsuario Intancia
        {
            get { return datUsuario._instance; }
        }
        public DataTable ListarUsuarios(Int32 status)
        {
            DataTable dt = null;
            SqlDataReader dr = null;
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand("spListarUsuarios", Conexion.GetConnection());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Estado", status);
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Conexion.DisposeCommand(cmd);
            }
            return dt;
        }

    }
}
