﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaAccesoDatos;
using CapaEntidades;
namespace CapaNegocio
{
    public class negCategoria
    {
        private static readonly negCategoria _instancia = new negCategoria();
        public static negCategoria Instancia
        {
            get { return negCategoria._instancia; }
        }

        public List<entCategoria> ListaCategorias()
        {
            try
            {
                List<entCategoria> lst = new List<entCategoria>();
                lst = datCategoria.Instancia.ListaCategoria();
                if (lst != null)
                {
                    if (lst.Count > 0)
                    {
                    }
                    else { throw new ApplicationException("No hay Marcas para mostrar"); }
                }
                else
                { throw new ApplicationException("Ocurrio un error al cargar las Marcas");}
                 return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }
    }
}
