﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaAccesoDatos;
using CapaEntidades;
using System.Data;

namespace CapaNegocio
{
    public class negPedido
    {
        private static readonly negPedido _instancia = new negPedido();
        public static negPedido  Instancia
        {
            get { return negPedido._instancia; }
        }

        public int Insertar_Pedido(entPedido p, DataTable detail)
        {
            int i;
            try
            {
               
               i = datPedido.Instancia.Insertar_Pedido(p, detail);
            }
            catch (Exception ex)
            {
                throw ex;
            }
                return i;
        }
    }
}
