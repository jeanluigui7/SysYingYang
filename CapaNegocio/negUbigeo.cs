﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidades;
using CapaAccesoDatos;
using System.Data;

namespace CapaEntidades
{
    public class negUbigeo
    {
        private static readonly negUbigeo _instancia = new negUbigeo();
        public static negUbigeo Instancia
        {
            get { return negUbigeo._instancia; }
        }

        public List<DataTable> ListarUbigeo()
        {
            List<DataTable> lista = datUbigeo.Instance.ListarUbigeo();
            try
            {
                if (lista != null)
                {
                    if (lista.Count <= 0)
                    {
                        throw new ApplicationException("No hay lista de ubigeo");
                    }
                }
                else
                {
                    throw new ApplicationException("Ocurrio un error al cargar la Data de Ubigeo");
                }
            }
            catch (ApplicationException msg)
            {
                throw msg;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return lista;
        }

    }
}
