﻿using System;
using System.Data;
using Librarys;
using CapaAccesoDatos;
using CapaEntidades;
namespace CapaNegocio
{
    public class negDireccion
    {
        private static readonly negDireccion _instancia = new negDireccion();
        public static negDireccion Instancia {
            get { return negDireccion._instancia; }
        }

        public DataTable ListarDireccionByClientId(ref BaseEntity Base, Int32 IdDireccion) {
            DataTable dt = new DataTable();
            Base = new BaseEntity();
            try
            {
                dt = datDireccion.Instancia.ListarDireccionByClientId(ref Base, IdDireccion);
            }
            catch (Exception ex)
            {
                Base.Errors.Add(new BaseEntity.ListError(ex, "Ocurrio un error al cargar los datos"));
            }
            return dt;
        }
    }
}
