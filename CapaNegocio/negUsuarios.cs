﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidades;
using CapaAccesoDatos;
using System.Data;


namespace CapaNegocio
{
    public class negUsuarios
    {
        private static readonly negUsuarios _instancia = new negUsuarios();
        public static negUsuarios Instancia
        {
            get { return negUsuarios._instancia; }
        }
        public DataTable ListaUsuarios(Int32 status)
        {
            try
            {
                DataTable dt = null;
                dt = datUsuario .Intancia.ListarUsuarios(status);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
