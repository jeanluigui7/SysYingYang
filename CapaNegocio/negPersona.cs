﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidades;
using CapaAccesoDatos;
using System.Data;

namespace CapaNegocio
{
   public class negPersona
    {
        private static readonly negPersona _instancia = new negPersona();
        public static negPersona Instancia
        {
            get { return negPersona._instancia; }
        }
        public DataTable ListarPersonas(Int32 status)
        {
            try
            {
                DataTable dt = null;
                dt = datPersona.Intancia.ListarPersonas(status);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Boolean RegistrarPersonas(entPersona objPer, entDireccion objDir)
        {
            Boolean success = false;
            try
            {
                success = datPersona.Intancia.RegistrarPersonas(objPer, objDir);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return success;
        }
        public Boolean ActualizarPersona(entPersona objPer, entDireccion objDir)
        {
            Boolean success = false;
            try
            {
                success = datPersona.Intancia.ActualizarPersona(objPer, objDir);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return success;
        }
        public entPersona ListarPersonaxId(Int32 Id)
        {
            entPersona objp = null;
            try
            {
                objp = new entPersona();
                objp = datPersona.Intancia.ListarPersonaxId(Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objp;
        }
    }
}
