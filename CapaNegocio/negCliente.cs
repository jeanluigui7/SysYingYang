﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidades;
using CapaAccesoDatos;
using System.Data;
using Librarys;

namespace CapaNegocio
{
    public class negCliente
    {
        private static readonly negCliente _instancia = new negCliente();
        public static negCliente Instancia { get { return negCliente._instancia; } }

        public Boolean RegistrarCliente(entCliente obj, entDireccion objD)
        {
            Boolean success = false;
            try
            {
                success = datCliente.Instancia.RegistrarCliente(obj, objD);
                if (!success)
                {
                    throw new ApplicationException("Ocurrio un problema al Registrarse");
                }
            }
            catch (ApplicationException msg)
            {
                throw msg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return success;
        }

        public entCliente AccesoCliente(string usu, string pass)
        {
            entCliente c = null;
            try
            {
                //if (String.IsNullOrEmpty(usu))
                //{
                //    throw new ApplicationException("Ingrese un Usuario, Por favor");
                //}
                //if (String.IsNullOrEmpty(pass))
                //{
                //    throw new ApplicationException("Ingrese un Password, Por favor");
                //}
                 c = datCliente.Instancia.AccesoCliente(usu, pass);

                //if (c == null)
                //{
                //    throw new ApplicationException("Usuario o Password Incorrecto, Intentelo nuevamente...");
                //}
               }
            catch (ApplicationException msg)
            {
                throw msg;
            }
            catch (Exception ex)
            {
                throw ex; 
            }
            return c;
        }

        public DataTable ListarCliente_ById(ref BaseEntity Base,Int32 idcliente, Int32 estado)
        {
            DataTable dt = new DataTable();
            Base = new BaseEntity();
            try
            {
                dt = datCliente.Instancia.ListarCliente_ById(ref Base, idcliente, estado);
            }
            catch (Exception ex)
            {
                dt = null;
                Base.Errors.Add(new BaseEntity.ListError(ex, "An error occurred while sending data."));
            }
            return dt;
        }

    }
}
