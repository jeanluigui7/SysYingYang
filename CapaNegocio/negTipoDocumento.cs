﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaAccesoDatos;
using CapaEntidades;
using System.Data;

namespace CapaNegocio
{
    public class negTipoDocumento
    {
        private static readonly negTipoDocumento _instancia = new negTipoDocumento();
        public static negTipoDocumento Instancia
        {
            get { return negTipoDocumento._instancia; }
        }

        public DataTable ListarTiposDocumento()
        {
            DataTable dt = null;
            try
            {
                 dt = datTipoDocumento.Instancia.ListarTiposDocumento();
            }
            catch (Exception ex)
            {
                throw ex; 
            }
            return dt;
        }
    }
}
