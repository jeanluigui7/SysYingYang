﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidades;
using CapaAccesoDatos;
using System.Data;
using System.Data.SqlClient;
namespace CapaNegocio
{
    public class negProducto
    {
        private static readonly negProducto _instancia = new negProducto();
        public static negProducto Instancia
        {
            get { return negProducto._instancia; }
        }

        public List<entProducto> ListarProductosxCategoria(int idcategoria)
        {
            try
            {
                List<entProducto> lista = new List<entProducto>();
                lista = datProducto.Instancia.ListarProductosxCategoria(idcategoria);
                if (lista != null)
                {
                    //if (lista.Count > 0)
                    //{
                    //}
                    //else { throw new ApplicationException("No hay Productos para Mostrar"); }
                }
                else
                {
                    throw new ApplicationException("Ocurrio un error al cargar lista de Productos");
                }
                return lista;
            }
            catch (ApplicationException msg)
            {
                throw msg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<entProducto> ListarProductos()
        {
            try
            {
                List<entProducto> lista = new List<entProducto>();
                lista = datProducto.Instancia.ListarProductos();
                if (lista != null)
                {
                    //if (lista.Count > 0)
                    //{
                    //}
                    //else { throw new ApplicationException("No hay Productos para Mostrar"); }
                }
                else
                {
                    throw new ApplicationException("Ocurrio un error al cargar lista de Productos");
                }
                return lista;
            }
            catch (ApplicationException msg)
            {
                throw msg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public entProducto ListarProductosId(int id)
        {
            try
            {
                entProducto item = new entProducto();
                item = datProducto.Instancia.ListarProductosId(id);
                if (item != null)
                {
                    //if (lista.Count > 0)
                    //{
                    //}
                    //else { throw new ApplicationException("No hay Productos para Mostrar"); }
                }
                else
                {
                    throw new ApplicationException("Ocurrio un error al cargar lista de Productos");
                }
                return item;
            }
            catch (ApplicationException msg)
            {
                throw msg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
