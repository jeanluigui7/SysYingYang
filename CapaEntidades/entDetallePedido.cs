﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class entDetallePedido
    {
        public Int32 DetalleId { get; set; }
        public Int32 ProId { get; set; }
        public Int32 PedidoId { get; set; }
        public Decimal Precio { get; set; }
        public Int32 Cantidad { get; set; }
        public entProducto Products { get; set; }
        public List<entDetallePedido> DetallePedido { get; set; }

        public static Decimal ObtenerTotal(List<entDetallePedido> Detalle)
        {
            Decimal Total = 0.0M;
            foreach (entDetallePedido item in Detalle)
            {
                Total += item.Precio * item.Cantidad;
            }
            return Total;
        }
    }
}
