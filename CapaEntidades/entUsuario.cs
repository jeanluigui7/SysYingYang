﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class entUsuario
    {
        public String UsuId { get; set; }
        public String PerId { get; set; }
        public String CargoId { get; set; }
        public String UsuCodigo { get; set; }
        public String Usuario { get; set; }
        public String PerNombre { get; set; }
        public String Password { get; set; }
        public String UsuarioRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
        public String UsuarioModificacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public String UsuEstado { get; set; }
        public String isCheckbox { get; set; }
        public String Index { get; set; }
    }
}
