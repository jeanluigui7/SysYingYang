﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class entPais
    {
        public int PaisId { get; set; }
        public string PaisNombre { get; set; }
        public int PaisEstado { get; set; }

    }
}
