﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class entDepartamento
    {
        public int DepId { get; set; }
        public string DepNombre { get; set; }
        public int DepEstado { get; set; }

    }
}
