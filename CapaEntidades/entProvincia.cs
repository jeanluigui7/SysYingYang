﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class entProvincia
    {
        public int ProvId { get; set; }
        public string ProvNombre { get; set; }
        public entDepartamento entDepartamento { get; set; }
        public int ProvEstado { get; set; }

    }
}
