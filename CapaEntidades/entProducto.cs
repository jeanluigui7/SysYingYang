﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class entProducto
    {
        public int ProId { get; set; }
        public entCategoria Categoria { get; set; }
        public String ProCodigo { get; set; }
        public String ProNombre { get; set; }
        public String ProDescripcion { get; set; }
        public int ProStock { get; set; }
        public String ProImagen { get; set; }
        public Decimal Precio { get; set; }
        public String ProUsuarioRegistro { get; set; }
        public DateTime ProFechaRegistro { get; set; }
        public String ProUsuarioModificacion { get; set; }
        public DateTime ProFechaModificacion { get; set; }
        public int ProEstado { get; set; }
    }
}
