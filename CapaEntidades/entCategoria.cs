﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class entCategoria
    {
        public int CatId { get; set; }
        public String CatCodigo { get; set; }
        public String CatNombre { get; set; }
        public String CatUsuarioRegistro { get; set; }
        public DateTime CatFechaRegistro { get; set; }
        public DateTime CatFechaModificacion { get; set; }
        public String CatUsuarioModificacion { get; set; }
        public int CatEstado { get; set; }
    }
}
