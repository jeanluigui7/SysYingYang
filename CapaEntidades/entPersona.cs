﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class entPersona
    {
      public String PerId { get; set; }
        public Int32 DirId { get; set; }
        public entDireccion Direccion { get; set; }
        public Int32 PaisId { get; set; }
        public Int32 DistId { get; set; }
        public Int32 ProvId { get; set; }
        public Int32 DepId { get; set; }
        public Int32 TipoDocId { get; set; }
        public String PerCodigo { get; set; }
        public String PerNombre { get; set; }
        public String PerApellidoP { get; set; }
        public String PerApellidoM { get; set; }
        public String PerEdad { get; set; }
        public String PerSexo { get; set; }
        public String PerCelular { get; set; }
        public String PerEmail { get; set; }
        public String PerTelefono { get; set; }
        public String PerFoto { get; set; }
        public String PerEstado { get; set; }
        public String Distrito { get; set; }
        public String Provincia { get; set; }
        public String Departamento { get; set; }
        public String Pais { get; set; }
        public String TipoDocumento { get; set; }
        public String NroDocumento { get; set; }

        public String isCheckbox { get; set; }
        public String Index { get; set; }
        

    }
}
