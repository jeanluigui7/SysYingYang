﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class entTipoDocumento
    {
        public Int32 TipoDocId { get; set; }
        public String TipoDocNombre { get; set; }
        public String TipoDocDescripcion { get; set; }

    }
}
