﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
   public class entDireccion
    {
        public int DirId { get; set; }
        public string DirDescripcion1 { get; set; }
        public string DirDescripcion2 { get; set; }
        public string DirCodPostal { get; set; }
        public int DirEstado { get; set; }
    }
}
