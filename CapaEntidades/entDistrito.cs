﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class entDistrito
    {
        public int DistId { get; set; }
        public string DistNombre { get; set; }
        public entProvincia entProvincia { get; set; }
        public int DistEstado { get; set; }

    }
}
