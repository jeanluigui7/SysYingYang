﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class entPedido
    {
        public Int32 PedidoId {get;set;}
        public Int32 CliId { get; set; }
        public Decimal Total { get; set; }
        public DateTime PedidoFechaRegistro { get; set; }
        public Int32 PedidoEstado { get; set; }


       
    }
   
}
