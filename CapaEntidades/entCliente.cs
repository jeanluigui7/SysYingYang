﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class entCliente
    {
        public int CliId { get; set; }
        public int DirId { get; set; }
        public int PaisId { get; set; }
        public int DistId { get; set; }
        public int ProvId { get; set; }
        public int DepId { get; set; }
        public string DirCodPostal { get; set; }
        public string CliNombre { get; set; }
        public string CliApellidoP { get; set; }
        public string CliApellidoM { get; set; }
        public string CliTelefono { get; set; }
        public string CliCelular { get; set; }
        public string CliEmail { get; set; }
        public string CliPassword { get; set; }
        public string CliFoto { get; set; }
        public DateTime CliFechaRegistro { get; set; }
        public string CliUsuarioRegistro { get; set; }
        public DateTime CliFechaModificacion { get; set; }
        public string CliUsuarioModificacion{ get; set; }
        public int CliEstado { get; set; }

    }
}
