﻿
using System.Web.UI;
using System;

namespace Librarys
{
    public class BasePageModule : Page
    {

        public void Message(EnumAlertType type, String message)
        {
            ClientScript.RegisterStartupScript(typeof(Page), "message", @"<script type='text/javascript'>fn_message('" + type.GetStringValue() + "', '" + message + "');</script>", false);

        }
    }
}