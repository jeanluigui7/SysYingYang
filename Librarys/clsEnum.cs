﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Librarys
{
    public static class clsEnum
    {
        public static String GetStringValue(this Enum value)
        {
            string output = null;
            Type type = value.GetType();
            FieldInfo fi = type.GetField(value.ToString());
            StringValueAttribute[] attrs = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
            if (attrs.Length > 0)
            {
                output = attrs[0].StringValue;
            }
            return output;
        }
    }
    public class StringValueAttribute : Attribute
    {
        public string StringValue { get; protected set; }
        public StringValueAttribute(string value)
        {
            this.StringValue = value;
        }
    }
    public enum EnumAlertType
    {
        [StringValue("s")]
        Success = 1,
        [StringValue("e")]
        Error = 2,
        [StringValue("i")]
        Info = 3,
        [StringValue("c")]
        Confirm = 4,
        [StringValue("b")]
        Custom = 5
    }
}
