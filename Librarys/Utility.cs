﻿using CapaEntidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Library
{
    public static class Utility
    {
        public static string ValidateDataRowKeyDefault(DataRow dr, string key, string vdefault)
        {
            try
            {
                return dr[key] is DBNull ? vdefault : Convert.ToString(dr[key], CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                return vdefault;
            }
        }

        public static void AgregarPedido(entPedido Pedido, List<entDetallePedido> DetalleP)
        {
            Pedido.CliId = ((entCliente)HttpContext.Current.Session["Cliente"]).CliId;
            Pedido.PedidoFechaRegistro = DateTime.Now;
            Pedido.PedidoEstado = 1;
            Pedido.Total = entDetallePedido.ObtenerTotal(DetalleP);
            HttpContext.Current.Session["Pedido"] = Pedido;
        }


        public static void AgregarProductoDetalle(entProducto item, List<entDetallePedido> Detalle)
        {
                entDetallePedido dp = new entDetallePedido();
                dp.Products = new entProducto();
                dp.ProId = item.ProId;
                dp.Precio = item.Precio;
                dp.Cantidad = 1;
                dp.Products = item;
                Detalle.Add(dp);
                HttpContext.Current.Session["SessionDetalle"] = Detalle;
            
        }
        public static void EliminarProducto(Int32 idprod, List<entDetallePedido> Detalle) {
            for (int i = 0; i < Detalle.Count; i++)
            {
                if (idprod == Detalle[i].ProId)
                {
                    Detalle.Remove(Detalle[i]);
                    ((entPedido)HttpContext.Current.Session["Pedido"]).Total = entDetallePedido.ObtenerTotal(Detalle);
                    HttpContext.Current.Session["SessionDetalle"] = Detalle;
                }
            }
        }

    }
}
