﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class Utility
    {
        public static string ValidateDataRowKeyDefault(DataRow dr, string key, string vdefault)
        {
            try
            {
                return dr[key] is DBNull ? vdefault : Convert.ToString(dr[key], CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                return vdefault;
            }
        }

    }
}
