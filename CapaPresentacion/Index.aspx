﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrincipalShop.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="CapaPresentacion.Index" %>
<%@ Import Namespace="CapaEntidades" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="Estilos/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript">
        var listProducts = [];
        $(function () {
            Bind();
        });

        $(document).ready(function () { //Carga primero el html luego las funciones de jquery
            CreateProducto($("#<%=hfProductos.ClientID%>").val());
        });

        function Bind() {
        }
        function fn_addProduct(id) {
            try {

                var success = function (asw) {
                    if (asw != null) {
                        if (asw.d.rpta == "ProdExist") {
                            fn_message('i', 'El producto ya existe dentro del Carrito');
                        }
                        else if (asw.d.rpta == "Exist") {
                            //var data = sessionStorage.getItem('SessionPro');
                            //if (data != null)
                            //{
                                //itemProducto = asw.d.itemProducto = "" ? '{}' : asw.d.itemProducto;
                                //item = $.parseJSON(itemProducto);
                                //listProducts.push(item);
                                //var obj = JSON.stringify(listProducts);
                                //sessionStorage.setItem('SessionPro', obj);
                                window.location.href = 'frmShopCard.aspx';
                            //}
                           
                        }
                    }
                };
                var error = function (jqXHR, textStatus, errorThrown){
                    fn_message('e', 'An error occurred while loading data');
                };

                fn_callmethod("Index.aspx/GetProductId", '{ id: "' + id + '"}', success, error);

            } catch (e) {
                fn_message('e', 'An error occurred while loading data');
            }
        }
        function CreateProducto(ListProducts) {
            var sessionCliente = '<%=Session["Cliente"]%>';
                var lst = ListProducts = "" ? '{}' : ListProducts;
                lstProducts = $.parseJSON(lst);
                var cadena = '';
                for (var i = 0; i < lstProducts.length; i++) {

                    cadena += " <div class='col-md-3 col-sm-5'>"
                    cadena += "   <div class='product'>";
                    cadena += "        <div class='flip-container'>";
                    cadena += "            <div class='flipper'>";
                    cadena += "                <div class='front'>";
                    cadena += "                    <a href='detail.html'>"
                    cadena += "                        <img src='Imagenes/" + lstProducts[i].ProImagen + "' alt='' class='img-responsive'>";
                    cadena += "                    </a>";
                    cadena += "                </div>";
                    cadena += "                <div class='back'>";
                    cadena += "                    <a href='detail.html'>";
                    cadena += "                        <img src='Imagenes/" + lstProducts[i].ProImagen + "' alt='' class='img-responsive'>";
                    cadena += "                    </a>";
                    cadena += "                </div>";
                    cadena += "            </div>";
                    cadena += "        </div>";
                    cadena += "       <a href='detail.html' class='invisible'><img src='Imagenes/" + lstProducts[i].ProImagen + "' alt='' class='img-responsive'></a>";
                    cadena += "        <div class='text'>";
                    cadena += "            <h3><a href=''>" + lstProducts[i].ProDescripcion + "</a></h3>";
                    cadena += "            <h4 align='center' class='price'>" + "S/." + lstProducts[i].Precio + "</h4>";
                    cadena += "            <h6 align='center' class='price'>" + "Codigo: " + lstProducts[i].ProCodigo + "</h6>";
                    cadena += "            <p class='buttons'>";
                    cadena += "                <a href='detail.html' class='btn btn-default'>Ver Detalle</a>";
                    if (sessionCliente != "")
                    {
                        cadena += "         <a id='idaddproduct' class='btn btn-primary' onClick='fn_addProduct(" + lstProducts[i].ProId+");'><i class='fa fa-shopping-cart'></i>Add to cart</a>";
                     //   cadena += "         <label id='productid' style='display:none'>" + lstProducts[i].ProId+"</label>";
                    }
                    cadena += "            </p>";
                    cadena += "        </div>";
                    cadena += "   </div>";
                    cadena += " </div>";

                    $("#RowContentProducts").html(cadena);
                }
            }
           
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hfProductos" runat="server" />
    
</asp:Content>
