﻿<%@ Page Title="" Language="C#" MasterPageFile="~/src/master-page/Home.Master" AutoEventWireup="true" CodeBehind="frmSavePersona.aspx.cs" Inherits="CapaPresentacion.frmSavePersona" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var objPersona = new Object();
        var listMarketsIds = [];
        var $options = [];
        $(function () {
            fn_init();
        });

        function fn_init() {
            fn_plugin();
            fn_bind();
        }

        function fn_plugin() {
            $('#FormPerson').validationEngine();
        }
        function fn_bind() {
            $("#<%=cboDepartamento.ClientID%>").on("change", function () {

                var success = function (asw) {

                    if (asw != null) {

                        if (asw.d.Msg == "Saved Successfully") {
                            LoadProvincias(asw.d.consultaPro);
                            LoadDistritos(asw.d.consultaDist);
                        }
                    }
                };

                var error = function (jqXHR, textStatus, errorThrown) {
                    fn_message('e', 'An error ocurrred while loading data');
                };

                var DepId = $("#<%=cboDepartamento.ClientID%>").val();
                fn_callmethod("frmRegistrarClientes.aspx/SeleccionarDepartamento", '{ DepId: "' + DepId + '"}', success, error);
            });


            $("#<%=cboProvincia.ClientID%>").on("change", function () {
                var success = function (asw) {
                    if (asw != null) {
                        if (asw.d.Msg == "Saved Successfully") {
                            DistritoXProvincia(asw.d.consultaDistXPro);
                        }
                    }
                };


                var error = function (jqXHR, textStatus, errorThrown) {
                    fn_message('e', 'An error ocurrred while loading data');
                }

                var ProvId = $("#<%=cboProvincia.ClientID%>").val();
                fn_callmethod("frmRegistrarClientes.aspx/SeleccionarProvincia", '{ ProvId: "' + ProvId + '"}', success, error);
            });
            <%--$("#<%=btnSavePerson.ClientID%>").click(function (e) {
                if (fn_validateform("FormPerson") == false){
                    return;
                }
                fn_GuardarPersona();
            });--%>

            $("#<%=ddlMarket.ClientID%>").multiselect({
                includeSelectAllOption: true,
                onChange: function (option, checked) {
                    listMarketsIds = [];
                    if (option.val() == "2" && checked == true) {
                        $options = $("select[id$=ddlMarket] option:selected");
                        if ($options.length > 0) {
                            for (var i = 0; i < $options.length; i++) {
                                if ($options[i].value != 2) {
                                    $options.filter('[value="' + $options[i].value + '"]').prop('selected', false);
                                    $options.filter('[value="' + $options[i].value + '"]').prop('checked', false);
                                }
                            }
                            $options = $("select[id$=ddlMarket] option:selected");
                            $("#<%=ddlMarket.ClientID%>").multiselect('refresh');
                        }
                    }
                    else if ($options.length == 1) {
                        if (option.val() == "multiselect-all") {
                            $options = $("select[id$=ddlMarket] option:selected");
                            if ($options.length > 0) {
                                for (var i = 0; i < $options.length; i++) {
                                    if ($options[i].value != 2) {
                                        $options.filter('[value="' + $options[i].value + '"]').prop('selected', false);
                                        $options.filter('[value="' + $options[i].value + '"]').prop('checked', false);
                                    }
                                }
                                $options = $("select[id$=ddlMarket] option:selected");
                                $("#<%=ddlMarket.ClientID%>").multiselect('refresh');
                            }
                        }
                        else ($options[0].value == "2" && $options[0].value != option.val())
                        {
                            $options = $("select[id$=ddlMarket] option:selected");
                            $options.filter('[value="' + option.val() + '"]').prop('selected', false);
                            $options.filter('[value="' + option.val() + '"]').prop('checked', false);

                            $options = $("select[id$=ddlMarket] option:selected");
                            $("#<%=ddlMarket.ClientID%>").multiselect('refresh');
                        }
                    }
                    else
                    {
                        if (option.val() == "multiselect-all")
                        {
                            $options = $("select[id$=ddlMarket] option:selected");
                            if ($options.length > 0) {
                                for (var i = 0; i < $options.length; i++) {
                                    if ($options[i].value == "2") {
                                        $options.filter('[value="' + $options[i].value + '"]').prop('selected', false);
                                        $options.filter('[value="' + $options[i].value + '"]').prop('checked', false);
                                    }
                                }
                                $options = $("select[id$=ddlMarket] option:selected");
                                $("#<%=ddlMarket.ClientID%>").multiselect('refresh');
                            }
                        }
                    }

                    $($("#ContentPlaceHolder1_ddlMarket").val()).each(function () {

                        if (listMarketsIds.length > 0) {
                            var added = false;
                            for (var i = 0; i < listMarketsIds.length; i++) {
                                if (listMarketsIds[i] == this["trim"]()) {
                                    added = true;
                                }
                            }
                            if (!added) {
                                listMarketsIds.push(this["trim"]());
                            }
                        } else {
                            listMarketsIds.push(this["trim"]());
                        }
                    });
                }
            });
        }

        function DistritoXProvincia(dataDistxPro) {
            $("#<%=cboDistrito.ClientID%>").empty();
            $.each(dataDistxPro, function () {
                $("#<%=cboDistrito.ClientID%>").append($("<option></option>").attr("value", this.DistId).text(this.DistNombre))
            });
        }

        function LoadProvincias(dataProv) {
            $("#<%=cboProvincia.ClientID%>").empty();
            $.each(dataProv, function () {
                $("#<%=cboProvincia.ClientID%>").append($("<option></option>").attr("value", this.ProvId).text(this.ProvNombre))
            });
        }
        function LoadDistritos(dataDist) {

            $("#<%=cboDistrito.ClientID%>").empty();
            $.each(dataDist, function () {
                $("#<%=cboDistrito.ClientID%>").append($("<option></option>").attr("value", this.DistId).text(this.DistNombre))
            });
        }

        function fn_GuardarPersona() {
            try {

                if ($("#<%=hfNameFoto.ClientID%>").val() == "") {
                    fn_message('i', 'Debe seleccionar y cargar una Foto', 'message_row');
                    return;
                }
                if (fn_validateform('FormPerson') == false) {
                    return;
                }

                objPersona = {
                    PerId: $("#<%=hfIdPersona.ClientID%>").val(),
                    DirId: $("#<%=hfIdDireccion.ClientID%>").val(),

                    PaisId: $("#<%=cboPais.ClientID%>").val(),
                    DepId: $("#<%=cboDepartamento.ClientID%>").val(),
                    ProvId: $("#<%=cboProvincia.ClientID%>").val(),
                    DistId: $("#<%=cboDistrito.ClientID%>").val(),
                    TipoDocId: $("#<%=cboTipoDocumento.ClientID%>").val(),
                    NumDoc: $("#<%=txtNroDocumento.ClientID%>").val(),
                    Nombre: $("#<%=txtNombre.ClientID%>").val(),
                    ApellidoP: $("#<%=txtApellidoPaterno.ClientID%>").val(),
                    ApellidoM: $("#<%=txtApellidoMaterno.ClientID%>").val(),
                    Edad: $("#<%=txtEdad.ClientID%>").val(),
                    Sexo: $("#<%=cboSexo.ClientID%>").find('option:selected').text(),
                    Email: $("#<%=txtEmail.ClientID%>").val(),
                    Telefono: $("#<%=txtTelefono.ClientID%>").val(),
                    Celular: $("#<%=txtCelular.ClientID%>").val(),
                    Foto: $("#<%=hfNameFoto.ClientID%>").val(),
                    EstadoP: $("#<%=chkActivo.ClientID%>").is(':checked') ? 1 : 0,

                    DirDescripcion1: $("#<%=txtDireccion1.ClientID%>").val(),
                    DirDescripcion2: $("#<%=txtDireccion2.ClientID%>").val(),
                    DirCodPostal: $("#<%=txtCodPostal.ClientID%>").val(),
                    DirEstado: 1
                }

                var success = function (asw) {
                    if (asw != null) {
                        if (asw.d.Msg == "Saved Successfully") {
                            fn_message('s', 'Se guardo Satisfactoriamente', 'message_row');
                        }
                    }
                }
                var error = function (xhr, ajaxOptions, thrownError) {
                    fn_message('e', 'A ocurrido un error mientras se enviaba la Data', 'message_row');
                }

                var senddata = JSON.stringify({ data: JSON.stringify(objPersona) });
                fn_callmethod("frmSavePersona.aspx/GuardarPersona", senddata, success, error);

            }
            catch (e) {
                fn_message(e, 'A ocurrido un error al guardar los Datos', 'message_row');
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="page-header">
        <h2>Persona</h2>

        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="index.html">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Personas</span></li>
                <li><span>Listado</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>

    <div class="row">
        <div class="col-lg-12" id="FormPerson">
            <section class="panel">
                <%--<div id="Div1">
				</div>--%>

                <div class="panel-body">
                    <div id="message_row"></div>
                    <asp:Literal ID="ltTitle" runat="server" Text="Agregar Peronsa"></asp:Literal>
                    <div id="finfo" class="form-horizontal form-bordered">

                        <div class="form-group">
                            <div class="col-sm-6 col-md-4 col-lg-4 cnt-text-label text-custom">
                                <span>(*) Required fields.</span>
                                <%--<asp:Label ID="Label20" runat="server" Text="(*) Required fields."></asp:Label>--%>
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-sm-4 col-md-4 col-lg-3 cnt-text-label">* Tipo Periodo:</span>
                            <div class="col-sm-7 col-md-7 col-lg-7 cnt-controles">
                                <asp:ListBox ID="ddlMarket" multiple="multiple" SelectionMode="Multiple" class="multiselect-container" CssClass="col-sm-6 col-lg-4 col-md-6" runat="server"></asp:ListBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-sm-4 col-md-4 col-lg-3 cnt-text-label">* Nombres:</span>
                        <%--<asp:Label ID="Label1" runat="server" class="col-sm-4 col-md-4 col-lg-3 cnt-text-label" Text="* Name:"></asp:Label>--%>
                        <div class="col-sm-7 col-md-7 col-lg-7 cnt-controles">
                            <asp:TextBox ID="txtNombre" CssClass="form-control validate[custom[onlyLetterandothers],html]" MaxLength="50" runat="server"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <span class="col-sm-4 col-md-4 col-lg-3 cnt-text-label">* Apellido Paterno:</span>
                        <%--<asp:Label ID="Label2" runat="server" class="col-sm-4 col-md-4 col-lg-3 cnt-text-label" Text="* Description:"></asp:Label>--%>
                        <div class="col-sm-7 col-md-7 col-lg-7 cnt-controles">
                            <asp:TextBox ID="txtApellidoPaterno" CssClass="form-control validate[custom[onlyLetterandothers],html]" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-sm-4 col-md-4 col-lg-3 cnt-text-label">* Apellido Materno:</span>
                        <%--<asp:Label ID="Label99" runat="server" CssClass="col-sm-4 col-md-4 col-lg-3 cnt-text-label reset-control" Text="Company Code"></asp:Label>--%>
                        <div class="col-sm-7 col-md-7 col-lg-7 cnt-controles">
                            <asp:TextBox ID="txtApellidoMaterno" CssClass="form-control validate[custom[onlyLetterandothers],html]" runat="server"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <span class="col-sm-4 col-md-4 col-lg-3 cnt-text-label">* Edad:</span>
                        <div class="col-sm-7 col-md-7 col-lg-7 cnt-controles">
                            <asp:TextBox ID="txtEdad" CssClass="form-control validate[custom[onlyNumberSp]]" TextMode="Number" runat="server"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <span class="col-sm-4 col-md-4 col-lg-3 cnt-text-label">* Email:</span>
                        <%--<asp:Label ID="Label99" runat="server" CssClass="col-sm-4 col-md-4 col-lg-3 cnt-text-label reset-control" Text="Company Code"></asp:Label>--%>
                        <div class="col-sm-7 col-md-7 col-lg-7 cnt-controles">
                            <asp:TextBox ID="txtEmail" CssClass="form-control validate[required]" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-sm-4 col-md-4 col-lg-3 cnt-text-label">* Telefono:</span>
                        <%--<asp:Label ID="Label99" runat="server" CssClass="col-sm-4 col-md-4 col-lg-3 cnt-text-label reset-control" Text="Company Code"></asp:Label>--%>
                        <div class="col-sm-7 col-md-7 col-lg-7 cnt-controles">
                            <asp:TextBox ID="txtTelefono" CssClass="form-control validate[custom[onlyNumberSp]]" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-sm-4 col-md-4 col-lg-3 cnt-text-label">* Celular:</span>
                        <%--<asp:Label ID="Label99" runat="server" CssClass="col-sm-4 col-md-4 col-lg-3 cnt-text-label reset-control" Text="Company Code"></asp:Label>--%>
                        <div class="col-sm-7 col-md-7 col-lg-7 cnt-controles">
                            <asp:TextBox ID="txtCelular" CssClass="form-control validate[custom[onlyNumberSp]]" runat="server"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <span class="col-sm-4 col-md-4 col-lg-3 cnt-text-label">* Direccion 1:</span>
                        <%--<asp:Label ID="Label99" runat="server" CssClass="col-sm-4 col-md-4 col-lg-3 cnt-text-label reset-control" Text="Company Code"></asp:Label>--%>
                        <div class="col-sm-7 col-md-7 col-lg-7 cnt-controles">
                            <asp:TextBox ID="txtDireccion1" CssClass="form-control validate[required]" runat="server"></asp:TextBox>
                        </div>
                        <span class="col-sm-4 col-md-4 col-lg-3 cnt-text-label">* Direccion 2:</span>
                        <%--<asp:Label ID="Label99" runat="server" CssClass="col-sm-4 col-md-4 col-lg-3 cnt-text-label reset-control" Text="Company Code"></asp:Label>--%>
                        <div class="col-sm-7 col-md-7 col-lg-7 cnt-controles">
                            <asp:TextBox ID="txtDireccion2" CssClass="form-control validate[required]" runat="server"></asp:TextBox>
                        </div>
                        <span class="col-sm-4 col-md-4 col-lg-3 cnt-text-label">* Codigo Postal:</span>
                        <%--<asp:Label ID="Label99" runat="server" CssClass="col-sm-4 col-md-4 col-lg-3 cnt-text-label reset-control" Text="Company Code"></asp:Label>--%>
                        <div class="col-sm-7 col-md-7 col-lg-7 cnt-controles">
                            <asp:TextBox ID="txtCodPostal" CssClass="form-control validate[custom[onlyNumberSp]]" runat="server"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <span class="col-sm-4 col-md-4 col-lg-3 cnt-text-label">Sexo:</span>
                        <%--<asp:Label ID="Label99" runat="server" CssClass="col-sm-4 col-md-4 col-lg-3 cnt-text-label reset-control" Text="Company Code"></asp:Label>--%>
                        <div class="col-sm-7 col-md-7 col-lg-7   cnt-controles">
                            <asp:DropDownList ID="cboSexo" CssClass="form-control" runat="server">
                                <asp:ListItem Value="1" Text="Masculino"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Femenino"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-sm-4 col-md-4 col-lg-3 cnt-text-label">* Tipo Documento:</span>
                        <div class="col-sm-4 col-md-4 col-lg-4   cnt-controles">
                            <asp:DropDownList ID="cboTipoDocumento" CssClass="form-control" runat="server"></asp:DropDownList>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3 cnt-controles">
                            <asp:TextBox ID="txtNroDocumento" CssClass="form-control validate[custom[onlyNumberSp]]" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-sm-4 col-md-4 col-lg-3 cnt-text-label">Pais:</span>
                        <%--<asp:Label ID="Label99" runat="server" CssClass="col-sm-4 col-md-4 col-lg-3 cnt-text-label reset-control" Text="Company Code"></asp:Label>--%>
                        <div class="col-sm-7 col-md-7 col-lg-7   cnt-controles">
                            <asp:DropDownList ID="cboPais" CssClass="form-control" runat="server"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group">
                        <span class="col-sm-4 col-md-4 col-lg-3 cnt-text-label">Departamento:</span>
                        <%--<asp:Label ID="Label4" runat="server" class="col-sm-4 col-md-4 col-lg-3 cnt-text-label" Text="Primary Warehouse:"></asp:Label>--%>
                        <div class="col-sm-7 col-md-7 col-lg-7 cnt-text">
                            <asp:DropDownList ID="cboDepartamento" CssClass="form-control" runat="server"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group">
                        <span class="col-sm-4 col-md-4 col-lg-3 cnt-text-label">Provincia:</span>
                        <%--<asp:Label ID="Label7" runat="server" class="col-sm-4 col-md-4 col-lg-3 cnt-text-label" Text="Secondary Warehouse:"></asp:Label>--%>
                        <div class="col-sm-7 col-md-7 col-lg-7 cnt-text">
                            <asp:DropDownList ID="cboProvincia" CssClass="form-control" runat="server"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group">
                        <span class="col-sm-4 col-md-4 col-lg-3 cnt-text-label">Distrito:</span>
                        <%--<asp:Label ID="Label14" runat="server" class="col-sm-4 col-md-4 col-lg-3 cnt-text-label" Text="Time Zone:"></asp:Label>--%>
                        <div class="col-sm-7 col-md-7 col-lg-7 cnt-text">
                            <asp:DropDownList ID="cboDistrito" CssClass="form-control" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-sm-4 col-md-4 col-lg-3 cnt-text-label">Activo:</span>
                        <%--<asp:Label ID="Label14" runat="server" class="col-sm-4 col-md-4 col-lg-3 cnt-text-label" Text="Time Zone:"></asp:Label>--%>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 cnt-text">
                            <asp:CheckBox ID="chkActivo" runat="server" />
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-sm-4 col-md-4 col-lg-3 cnt-text-label">Foto:</span>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 cnt-text">
                            <asp:Image ID="ImgFoto" ClientIDMode="Static" runat="server" Width="130px" Height="130px"></asp:Image>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-lg-offset-3 cnt-text">
                            <asp:FileUpload ID="FU1" runat="server" class="mb-xs mt-xs mr-xs btn btn-primary" />
                            <asp:Button ID="btnUpload" runat="server" class="mb-xs mt-xs mr-xs btn btn-primary" Text="Cargar Imagen" OnClick="btnUpload_Click" />
                        </div>
                    </div>
                </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-sm-9 col-sm-offset-3">
                    <button type="button" runat="server" class="mb-xs mt-xs mr-xs btn btn-lg btn-primary" onclick="fn_GuardarPersona()">Guardar</button>
                    <%--<asp:Button ID="btnSavePerson" class="mb-xs mt-xs mr-xs btn btn-lg btn-primary" runat="server" Text="Guardar" OnClientClick="return fn_validateform('FormPerson');" />--%>
                    <button type="button" runat="server" class="mb-xs mt-xs mr-xs btn btn-lg btn-default">Cancelar</button>
                </div>
            </div>
        </footer>
        </section>
    </div>
    <asp:HiddenField runat="server" ID="hfNameFoto" />
    <asp:HiddenField runat="server" ID="hfIdPersona" Value="0" />
    <asp:HiddenField runat="server" ID="hfIdDireccion" Value="0" />
    </div>
</asp:Content>
