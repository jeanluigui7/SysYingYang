﻿using CapaEntidades;
using CapaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CapaPresentacion
{
    public partial class frmLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) {

            }
        }
        [WebMethod(EnableSession = true)]
        public static object Login(String user, String pass) 
        {            
            object obj = new object();
            try
            {
                entCliente c = negCliente.Instancia.AccesoCliente(user, pass);
                if (c != null)
                {
                    HttpContext.Current.Session["Cliente"] = c;
                    //HttpContext.Current.Response.Redirect("frmBienvenidaCliente.aspx");
                    obj = new
                    {
                        Result = "Ok"
                    };
                }
                else {
                    obj = new
                    {
                        Result = "NoCredenciales"
                    };
                }
                
            }
            catch (Exception ex)
            {
                obj = new
                {
                    Result = "NoOk"
                };
            }
            return obj;
        }
     
    }
}