﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaEntidades;
using CapaNegocio;
using Library;

namespace CapaPresentacion
{
    public partial class frmShopCard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        [WebMethod(EnableSession = true)]
        public static object ProductAddtoList(String trim)
        {
            object lst = new object();
            JavaScriptSerializer js = new JavaScriptSerializer();
          
            try
            {
                var objDetail = (List<entDetallePedido>)HttpContext.Current.Session["SessionDetalle"];
                var objPedido = (entPedido)HttpContext.Current.Session["Pedido"];
               
                    lst = new
                    {
                        lstDetail = js.Serialize(objDetail),
                        Total = objPedido.Total
                    };
                            
            }
            catch (Exception ex)
            {
                lst = new
                {
                    lstDetail = "",
                    Total = ""
                };
                throw ex;
            }
            return lst;
        }

        [WebMethod(EnableSession = true)]
        public static object ChangeQuantityProduct(String ID, String NewQuantity) {
            object lst = new object();
            try
            {
              
                JavaScriptSerializer js = new JavaScriptSerializer();
                List<entDetallePedido> ListDetail = (List<entDetallePedido>)HttpContext.Current.Session["SessionDetalle"];
                Int32 Id = Convert.ToInt32(ID); 
                Int32 NewCant = Convert.ToInt32(NewQuantity); 
                foreach (entDetallePedido item in ListDetail) {
                    if (Id == item.ProId) {
                        item.Cantidad = Convert.ToInt32(NewCant);
                    }
                }
                HttpContext.Current.Session["SessionDetalle"] = ListDetail;
                ((entPedido)HttpContext.Current.Session["Pedido"]).Total = entDetallePedido.ObtenerTotal(ListDetail);
                entPedido objPedido = (entPedido)HttpContext.Current.Session["Pedido"];
                lst = new
                {
                    lstDetail = js.Serialize(ListDetail),
                    Total = objPedido.Total
                };
            }
            catch (Exception ex)
            {

                lst = new
                {
                    lstDetail = "",
                    Total = ""
                };
                throw ex;
            }
            return lst;
        }

        [WebMethod(EnableSession =true)]
        public static String DeleteProduct(String idpro)
        {
            String Rpta = "false";
            try
            {
                Int32 id = 0;
                if (!Int32.TryParse(idpro, out id)) return Rpta = "false";
                
                List<entDetallePedido> Detalle = (List<entDetallePedido>)HttpContext.Current.Session["SessionDetalle"];
                var result = (from p in Detalle
                              select p.ProId == id).Any();
                if (result == true) {
                    Utility.EliminarProducto(id, Detalle);
                    Rpta = "true";
                }
            }
            catch (Exception ex)
            {
                Rpta = "false";
                throw ex;
            }
            return Rpta;
        }

    }
}