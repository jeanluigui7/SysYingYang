﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrincipalShop.Master" AutoEventWireup="true" CodeBehind="frmListProducts.aspx.cs" Inherits="CapaPresentacion.frmListProducts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="Estilos/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript">

        $(function () {
            Bind();
        });

        $(document).ready(function () { //Carga primero el html luego las funciones de jquery
            CreateProducto($("#<%=hfProducts.ClientID%>").val());
        });

        function Bind() {
           
        }
        
        function CreateProducto(ListProducts) {
            if (ListProducts != "") { 
            var lst = ListProducts = '' ? '{}' : ListProducts;
            lstProducts = $.parseJSON(lst);
            var cadena = '';
                for (var i = 0; i < lstProducts.length; i++) {

                    cadena += " <div class='col-md-3 col-sm-5'>"
                    cadena += "   <div class='product'>";
                    cadena += "        <div class='flip-container'>";
                    cadena += "            <div class='flipper'>";
                    cadena += "                <div class='front'>";
                    cadena += "                    <a href='detail.html'>"
                    cadena += "                        <img src='Imagenes/" + lstProducts[i].ProImagen + "' alt='' class='img-responsive'>";
                    cadena += "                    </a>";
                    cadena += "                </div>";
                    cadena += "                <div class='back'>";
                    cadena += "                    <a href='detail.html'>";
                    cadena += "                        <img src='Imagenes/" + lstProducts[i].ProImagen + "' alt='' class='img-responsive'>";
                    cadena += "                    </a>";
                    cadena += "                </div>";
                    cadena += "            </div>";
                    cadena += "        </div>";
                    cadena += "       <a href='detail.html' class='invisible'><img src='Imagenes/" + lstProducts[i].ProImagen + "' alt='' class='img-responsive'></a>";
                    cadena += "        <div class='text'>";
                    cadena += "            <h3><a href=''>" + lstProducts[i].ProNombre + "</a></h3>";
                    cadena += "            <h4 align='center' class='price'>" + "S/." + lstProducts[i].Precio + "</h4>";
                    cadena += "            <h6 align='center' class='price'>" + "Codigo: " + lstProducts[i].ProCodigo + "</h6>";
                    cadena += "            <p class='buttons'>";
                    cadena += "                <a href='detail.html' class='btn btn-default'>Ver Detalle</a>";
                    cadena += "                <a href='basket.html' class='btn btn-primary' id='btnAdd'><i class='fa fa-shopping-cart'></i>Add to cart</a>";
                    cadena += "            </p>";
                    cadena += "        </div>";
                    cadena += "   <label runat='server' value=" + lstProducts[i].ProId + "></label>";
                    cadena += "   </div>";
                    cadena += " </div>";

                    $("#RowContentProducts").html(cadena);

                }
            }
            else {
                fn_message('i', 'No hay Productos en esta Categoria', 'message_row');
            }
       }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:HiddenField ID="hfProducts" runat="server" />
</asp:Content>
