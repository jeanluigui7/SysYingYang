﻿using Librarys;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaEntidades;
using CapaNegocio;
using Library;
using System.Web.Services;
using System.Globalization;

namespace CapaPresentacion
{
    public partial class ProccessAddress : BasePageModule
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!Page.IsPostBack) {
                    ListarUbigeo();
                    LoadAdress();
                    LoadTotals();
                }

            }
            catch (Exception)
            {
                this.Message(EnumAlertType.Error, "Ocurrio un error al cargar los datos");
            }
        }

        private void LoadTotals()
        {
            Decimal total = 0.0m;
            Decimal TotalAmount = ((entPedido)HttpContext.Current.Session["Pedido"]).Total;

            if (!Decimal.TryParse(TotalAmount.ToString().Trim(), out total) || total <= 0)
                this.Message(EnumAlertType.Error, "El monto total no puede ser 0.0");
            else
            lblSubtotal.InnerText = Convert.ToString(Math.Round(total, 2), CultureInfo.InvariantCulture);
            lblTotalResumen.InnerHtml = Convert.ToString(Math.Round(total, 2), CultureInfo.InvariantCulture);
        }

        public void ListarUbigeo()
        {
            try
            {
                List<DataTable> dt = negUbigeo.Instancia.ListarUbigeo();
                if (dt.Count > 0)
                {
                    for (int i = 0; i < dt.Count; i++)
                    {
                        DataTable tabla = dt[i];
                        switch (tabla.TableName)
                        {
                            case "Departamentos":
                                ComboDepartamentos(tabla);
                                break;

                            case "Provincias":
                                Session["Provincias"] = tabla;
                                //var consultaProvincias = from t in tabla.AsEnumerable() where t.Field<int>("DepId") == 1 select new { ProvId = t.Field<int>("ProvId"), ProvNombre = t.Field<string>("ProvNombre") };
                                ComboProvincias(tabla);
                                break;

                            case "Distritos":
                                Session["Distritos"] = tabla;
                                //var consultaDistritos = from t in tabla.AsEnumerable() where t.Field<int>("ProvId") == 1 select new { DistId = t.Field<int>("DistId"), DistNombre = t.Field<string>("DistNombre") };
                                ComboDistritos(tabla);
                                break;
                        }
                    }
                }
            }
            catch (ApplicationException msg)
            {
                string script = @"<script type='text/javascript'>alert('" + msg.Message + "')</script>";
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, false);
            }
        }
        
        public void ComboDepartamentos(object dtDe)
        {
            cboDepartamento.DataSource = dtDe;
            cboDepartamento.DataTextField = "DepNombre";
            cboDepartamento.DataValueField = "DepId";
            cboDepartamento.DataBind();
        }
        public void ComboProvincias(object dtPr)
        {
            cboProvincia.DataSource = dtPr;
            cboProvincia.DataTextField = "ProvNombre";
            cboProvincia.DataValueField = "ProvId";
            cboProvincia.DataBind();
        }
        public void ComboDistritos(object dtDi)
        {
            cboDistrito.DataSource = dtDi;
            cboDistrito.DataTextField = "DistNombre";
            cboDistrito.DataValueField = "DistId";
            cboDistrito.DataBind();
        }


        [WebMethod(EnableSession = true)]
        public static object SeleccionarDepartamento(String DepId)
        {
            DataTable dtPro = (DataTable)HttpContext.Current.Session["Provincias"];
            var consultaPro = from dt in dtPro.AsEnumerable() where dt.Field<int>("DepId") == Convert.ToInt32(DepId) select new { ProvId = dt.Field<int>("ProvId"), ProvNombre = dt.Field<string>("ProvNombre") };

            //ComboProvincias(consultaPro);

            int idPro = consultaPro.FirstOrDefault().ProvId;
            DataTable dtDist = (DataTable)HttpContext.Current.Session["Distritos"];
            var consultaDist = from dt in dtDist.AsEnumerable() where dt.Field<int>("ProvId") == idPro select new { DistId = dt.Field<int>("DistId"), DistNombre = dt.Field<string>("DistNombre") };
            //ComboDistritos(consultaDist);

            return new
            {
                Result = "Ok",
                Msg = "Saved Successfully",
                consultaPro = consultaPro,
                consultaDist = consultaDist,
            };
        }

        [WebMethod(EnableSession = true)]
        public static object SeleccionarProvincia(String ProvId)
        {
            DataTable dtDist = (DataTable)HttpContext.Current.Session["Distritos"];
            var consultaDistXPro = from dt in dtDist.AsEnumerable() where dt.Field<int>("ProvId") == Convert.ToInt32(ProvId) select new { DistId = dt.Field<int>("DistId"), DistNombre = dt.Field<string>("DistNombre") };
            //ComboDistritos(consultaDist);

            return new
            {
                Result = "Ok",
                Msg = "Saved Successfully",
                consultaDistXPro = consultaDistXPro,
            };
        }

        private void LoadAdress()
        {
            try
            {
                BaseEntity Base = new BaseEntity();
                entCliente objCli = new entCliente();
                entDireccion objDir = new entDireccion();
                Int32 IdCliente = ((entCliente)HttpContext.Current.Session["Cliente"]).CliId;
                DataTable dt = negCliente.Instancia.ListarCliente_ById(ref Base, IdCliente, 1);
                if (Base.Errors.Count == 0)
                {
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow item in dt.Rows)
                            {
                                objCli.CliNombre = Utility.ValidateDataRowKeyDefault(item, "CliNombre", "");
                                objCli.CliApellidoP = Utility.ValidateDataRowKeyDefault(item, "CliApellidoP", "");
                                objCli.CliApellidoM = Utility.ValidateDataRowKeyDefault(item, "CliApellidoM", "");
                                objCli.CliCelular = Utility.ValidateDataRowKeyDefault(item, "CliCelular", "");
                                objCli.DirId = Convert.ToInt32(Utility.ValidateDataRowKeyDefault(item, "DirId", ""));
                                objCli.DirCodPostal = Utility.ValidateDataRowKeyDefault(item, "DirCodPostal", "");
                                objCli.DepId = Convert.ToInt32(Utility.ValidateDataRowKeyDefault(item, "DepId", ""));
                                objCli.ProvId = Convert.ToInt32(Utility.ValidateDataRowKeyDefault(item, "ProvId", ""));
                                objCli.DistId = Convert.ToInt32(Utility.ValidateDataRowKeyDefault(item, "DistId", ""));
                                objCli.CliEmail = Utility.ValidateDataRowKeyDefault(item, "CliEmail", "");

                            }

                            DataTable dtD = negDireccion.Instancia.ListarDireccionByClientId(ref Base, objCli.DirId);
                            if (Base.Errors.Count == 0)
                            {
                                if (dtD != null)
                                {
                                    if (dtD.Rows.Count > 0)
                                    {
                                        foreach (DataRow item in dtD.Rows)
                                        {
                                            objDir.DirDescripcion1 = Utility.ValidateDataRowKeyDefault(item, "DirDescripcion1", "");
                                            objDir.DirDescripcion2 = Utility.ValidateDataRowKeyDefault(item, "DirDescripcion2", "");
                                            objDir.DirCodPostal = Utility.ValidateDataRowKeyDefault(item, "DirCodPostal", "");
                                        }
                                    }
                                    else
                                    {
                                        this.Message(EnumAlertType.Error, "No tiene registrada ninguna direccion");
                                    }
                                }
                                else
                                {
                                    this.Message(EnumAlertType.Error, "Ocurrio un error al cargar los datos de la direccion");
                                }
                            }
                            else
                            {
                                this.Message(EnumAlertType.Error, "Ocurrio un error al cargar los datos de la direccion");
                            }

                            SetControls(objCli, objDir);
                        }
                        else
                        {
                            this.Message(EnumAlertType.Error, "Ocurrio un error al cargar los datos del cliente");
                        }
                    }
                    else
                    {
                        this.Message(EnumAlertType.Error, "Ocurrio un error al cargar los datos");
                    }
                }
                else
                {
                    this.Message(EnumAlertType.Error, "Ocurrio un error al cargar los datos");
                }
            }
            catch (Exception)
            {
                this.Message(EnumAlertType.Error, "Ocurrio un error al cargar los datos");
            }
        }

        private void SetControls(entCliente objCli, entDireccion objDir)
        {
            txtNombre.Text = objCli.CliNombre;
            txtApellidoM.Text = objCli.CliApellidoM;
            txtApellidoP.Text = objCli.CliApellidoP;
            txtCelular.Text = objCli.CliCelular;

            if (objCli.DepId > 0 && objCli.ProvId > 0 && objCli.DistId > 0) {
                if (cboDepartamento.Items.FindByValue(objCli.DepId.ToString().Trim()) != null) {
                    cboDepartamento.SelectedValue = objCli.DepId.ToString().Trim();
                }
                if (cboProvincia.Items.FindByValue(objCli.ProvId.ToString().Trim()) != null) {
                    cboProvincia.SelectedValue = objCli.ProvId.ToString().Trim();
                }
                if (cboDistrito.Items.FindByValue(objCli.DistId.ToString().Trim()) != null) {
                    cboDistrito.SelectedValue = objCli.DistId.ToString().Trim();
                }
            }
            txtEmail.Text = objCli.CliEmail;

            txtDireccionPrincipal.Text = objDir.DirDescripcion1;
            txtDireccionReferencial.Text = objDir.DirDescripcion2;
            txtCodigoPos.Text = objDir.DirCodPostal;

            DisabledControls();
        }

        private void DisabledControls()
        {
            txtNombre.Enabled = false;
            txtApellidoM.Enabled = false;
            txtApellidoP.Enabled = false;
            txtCelular.Enabled = false;
            txtEmail.Enabled = false;
            txtDireccionPrincipal.Enabled = false;
            txtDireccionReferencial.Enabled = false;
            txtCodigoPos.Enabled = false;
            cboDepartamento.Enabled = false;
            cboProvincia.Enabled = false;
            cboDistrito.Enabled = false;

        }
    }
}