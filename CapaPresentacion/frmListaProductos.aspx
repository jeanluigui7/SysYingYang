﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="frmListaProductos.aspx.cs" Inherits="CapaPresentacion.frmListaProductos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:DataList ID="dlsProductos" runat="server" RepeatColumns="4" OnItemCommand="dlsProductos_ItemCommand">
       <ItemTemplate>
           <div class="grid_1">
	     	<table>
                    <tr>
                        <td align="center">
                            <asp:Image ID="imgProducto" runat="server" Height="160px" 
                                Width="160px" ImageUrl='<%# Eval("ProImagen", "/Imagenes/{0}") %>'/>
                        </td>
                    </tr>

                    <tr>
                        <td>Código:
                            <asp:Label ID="lblCodigo" runat="server" Text='<%# Eval("ProCodigo") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>Marca:
                            <asp:Label ID="lblCategoria" runat="server" Text='<%# Eval("Categoria.CatNombre") %>'
                                Width="80px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblNombrePro" runat="server" Text='<%# Eval("ProNombre") %>'
                                Width="100%" ></asp:Label>
                        </td>
                    </tr>
                 <tr>
                     <td>Precio: S/.
                         <asp:Label ID="lblPrecio" runat="server" Text='<%# Eval("Precio") %>'></asp:Label> 
                     </td>
                 </tr>
                 <tr>
                     <td>Stock:
                         <asp:Label ID="lblStock" runat="server" Text='<%# Eval("ProStock") %>'></asp:Label>
                     </td>
                 </tr>  
                
                 <tr>
                     <td>
                         <% if (Session["Cliente"] != null)
                             {
                          %>
                         <asp:TextBox ID="txtCantidad" runat="server" Text="1" TextMode="Number" Width="60px"></asp:TextBox>
                         <asp:Button  ID="btnComprar" runat="server" Text="Comprar" CommandName="btnComprar"/>
                         <asp:Label ID="lblIdProducto" runat="server" Text='<%# Eval("ProId") %>' Visible="false"></asp:Label>
                         <% 
                             }
                         %>
                     </td>
                 </tr>

                </table>
     </div>

      </ItemTemplate>
    </asp:DataList>

</asp:Content>
