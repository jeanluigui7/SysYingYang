﻿<%@ Page Title="" Language="C#" MasterPageFile="~/src/master-page/Home.Master" AutoEventWireup="true" CodeBehind="frmRegistroUsuarios.aspx.cs" Inherits="CapaPresentacion.frmRegistroUsuarios1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Private/src/handlebars/handlebars-v2.0.0.min.js"></script>
    <style type="text/css">
        /*th, td { white-space: nowrap; }
           div.dataTables_wrapper {
        width: 1000px;
        margin: 0 auto;
    }*/
    </style>
    <script type="text/javascript">
        var idSelected = "";
        var obj;
        var table;
        var rows_selected = [];
        var isTooltip = false;
        $(function () {

            fn_init();

        });
        function fn_init() {
            fn_content();
            fn_bind();
        }
        function fn_content() {
            fn_fillTable2($("#<%=hfData.ClientID%>").val());
        }
        function fn_new() {
            window.location.href = "frmSaveUsuario.aspx";
        }
        function fn_fillTable2(data) {
            var glancedata = data;
            try {
                obj = $.parseJSON(glancedata);
                var object = {};
                object.request = obj;
                var item = fn_LoadTemplates("datatable-resources", object);
                $("#datatable-default tbody").html(item);
                table = $("#datatable-default").DataTable({
                    'columnDefs': [{
                        'targets': 0,
                        'searchable': false,
                        'orderable': false,
                        'className': 'dt-body-center',
                        'render': function (data, type, full, meta) {
                            return '<input type="checkbox">';
                        }
                    }],
                    'order': [[2, 'asc']],
                    'rowCallback': function (row, data, dataIndex) {
                        var rowId = data[0];
                        if ($.inArray(rowId, rows_selected) !== -1) {
                            $(row).find('input[type="checkbox"]').prop('checked', true);
                            $(row).addClass('selected');
                        }
                    }
                });
                table.on('draw', function () {
                    updateDataTableSelectAllCtrl(table);
                });
            }
            catch (e) {
                fn_message('e', 'An error occurred while loading data');
            }
        }

        function fn_bind() {
            $("td [role='gridcell'][aria-describedby='tbGrid_ACTION']").attr("title", '');
    <%--        $("#<%=lnkbtnlanguage.ClientID %>").click(function () {
                $("#popup-languages").modal("show");
                return false;
            });--%>
            $('#datatable-default tbody').on('click', 'input[type="checkbox"]', function (e) {
                var $row = $(this).closest('tr');
                var data = table.row($row).data();
                var rowId = data[0];
                var index = $.inArray(rowId, rows_selected);
                if (this.checked && index === -1) {
                    rows_selected.push(rowId);
                } else if (!this.checked && index !== -1) {
                    rows_selected.splice(index, 1);
                }
                if (this.checked) {
                    $row.addClass('selected');
                } else {
                    $row.removeClass('selected');
                }
                updateDataTableSelectAllCtrl(table);
                e.stopPropagation();
            });

            $('#datatable-default').on('click', 'tbody td, thead th:first-child', function (e) {
                $(this).parent().find('input[type="checkbox"]').trigger('click');
            });
            $('#datatable-default thead input[name="all"]').on('click', function (e) {
                if (this.checked) {
                    $('#datatable-default tbody input[type="checkbox"]:not(:checked)').trigger('click');
                } else {
                    $('#datatable-default tbody input[type="checkbox"]:checked').trigger('click');
                }
                e.stopPropagation();
            });
        }
        function fn_RowEdit(index) {
            //console.log(index);
            //var row = $('#tbDataTable').getRowData(index);
            window.location.href = "frmSavePersona.aspx?q=" + index;
        }
        function fn_DownloadFile(index) {
            var glancedata = $("#<%=hfData.ClientID%>").val();
            try {
                var obj = $.parseJSON(glancedata);
                var ind = index - 1;
                //var row = $('#tbDataTable').getRowData(index);
                //var file = obj[ind].NameResource;
                //var filenamesplited = obj[ind].NameResource.split('.');
                var extension = "docx";//filenamesplited[filenamesplited.length - 1].toLowerCase();

                if (extension != "pdf") {
                    window.location.href = file;
                }
                else {
                    window.open(file, "Download", "status=yes,min-width=300,height=300,scrollbars=yes");
                }

            } catch (e) {
                fn_message('e', 'An error occurred while downloading the file');
            }
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField runat="server" ID="hfData" />
    <header class="page-header">
        <h2>Usuarios</h2>

        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="index.html">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Usuarios</span></li>
                <li><span>Agregar</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>

    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
                <a href="#" class="fa fa-times"></a>
            </div>

            <h2 class="panel-title">Lista de Usuarios</h2>
        </header>
        <div class="panel-body">
              <div class="row" align="center">
                            <div class="col-md-9 cnt-controles">
                                <a class="mb-xs mt-xs mr-xs btn btn-primary" onclick="fn_new()"  id="a1"><i class="fa fa-plus"></i><span>&nbsp;Add</span> </a>
                                <a class="mb-xs mt-xs mr-xs btn btn-danger" onclick="fn_delete3()" id="A2"><i class="fa fa-times"></i><span>&nbsp;Delete</span> </a>
                            
                           
                            </div>
                </div>
            <table class="table table-bordered table-striped mb-none" id="datatable-default">
                <thead>
                    <tr>
                        <th style="display: none;"></th>
                        <th>
                            <input type="checkbox" id="all" name="all" /></th>
                        <th>#</th>
                        <th>Codigo</th>
                        <th>Cargo</th>
                        <th>Nombre</th>
                        <th>Usuario</th>
                        <th>Password</th>
                        <th>Estado</th>
                        <th>Accion</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </section>


    <script type="text/x-handlebars-template" id="datatable-resources">
        {{# each request}}
             <tr>
                 <td style="display: none;">{{UsuId}}</td>
                 {{#if isCheckbox}}
                    <td id='multiselect' style='text-align: center;'>
                        <input type='checkbox' id='msg_sel' name='msg_sel' /></td>
                 {{else}}
                    <td id='multiselect' style='text-align: center;'></td>
                 {{/if}} 
                 <td style='text-align: center;'>{{Index}}</td>
                 <td>{{UsuCodigo}}</td>
                 <td>{{CargoId}}</td>
                 <td>{{PerNombre}}</td>
                 <td>{{Usuario}}</td>
                 <td>{{Password}}</td>
                 {{#if UsuEstado}}
                    <td>Activo</td>
                 {{else}}
                    <td>Inactivo</td>
                 {{/if}} 
                 <td style='text-align: center;'><a onclick="fn_RowEdit('{{UsuId}}')" title='Edit' class='gridActionBtn'><i class='fa fa-edit'></i></a><a onclick="fn_DownloadFile('{{Index}}')" title='Download' class='gridActionBtn'><i class='fa fa-download'></i></a></td>
             </tr>
        {{/each}}
    </script>
</asp:Content>
