﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaNegocio;
using CapaEntidades;
using System.Data;

namespace CapaPresentacion
{
    public partial class Principal : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lvMarcas.DataSource = negCategoria.Instancia.ListaCategorias();
                lvMarcas.DataBind();
            }
            catch (ApplicationException msg)
            {
                MostrarMensaje(msg.Message);
            }
        }

        private void MostrarMensaje(string msg)
        {
            string script = @"<script type='text/javascript'>alert('" + msg + "');</script>";
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, false);
        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                entCliente c = negCliente.Instancia.AccesoCliente(txtUsuario.Text, txtPassword.Text);
                if (c != null)
                {
                    Session["Cliente"] = c;
                    Response.Redirect("frmBienvenidaCliente.aspx");
                }
            }
            catch (ApplicationException msg)
            {
                MostrarMensaje(msg.Message);
            }
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            Session.Remove("Cliente");
            Response.Redirect("Index.aspx");
        }
    }
}