﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaEntidades;
using CapaNegocio;
using Librarys;
namespace CapaPresentacion
{
    public partial class frmListProducts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) {
                ListarProductos();
            }
        }

        private void ListarProductos() {
            try
            {
                JavaScriptSerializer sr = new JavaScriptSerializer();
                List<entProducto> lstPro = new List<entProducto>();

                Int32 idmarca = Convert.ToInt32(Request.QueryString["idmarca"]);
                lstPro = negProducto.Instancia.ListarProductosxCategoria(idmarca);

                if (lstPro != null && lstPro.Count > 0) {
                    hfProducts.Value = sr.Serialize(lstPro);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}