﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaEntidades;
using CapaNegocio;
using System.Data;
using System.IO;
using System.Web.Services;
using CapaPresentacion.src;

namespace CapaPresentacion
{
    public partial class frmRegistrarClientes : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ListarUbigeo();
            }
        }
        public void ListarUbigeo()
        {
            try
            {
                List<DataTable> dt = negUbigeo.Instancia.ListarUbigeo();
                if (dt.Count > 0)
                {
                    for (int i = 0; i < dt.Count; i++)
                    {
                        DataTable tabla = dt[i];
                        switch (tabla.TableName)
                        {
                            case "Paises":
                                ComboPais(tabla);
                                break;

                            case "Departamentos":
                                ComboDepartamentos(tabla);
                                break;

                            case "Provincias":
                                Session["Provincias"] = tabla;
                                var consultaProvincias = from t in tabla.AsEnumerable() where t.Field<int>("DepId") == 1 select new { ProvId = t.Field<int>("ProvId"), ProvNombre = t.Field<string>("ProvNombre") };
                                ComboProvincias(consultaProvincias);
                                break;

                            case "Distritos":
                                Session["Distritos"] = tabla;
                                var consultaDistritos = from t in tabla.AsEnumerable() where t.Field<int>("ProvId") == 1 select new { DistId = t.Field<int>("DistId"), DistNombre = t.Field<string>("DistNombre") };
                                ComboDistritos(consultaDistritos);
                                break;
                        }
                    }
                }
            }
            catch (ApplicationException msg)
            {
                string script = @"<script type='text/javascript'>alert('" + msg.Message + "')</script>";
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, false);
            }
        }

        public void ComboPais(object dtPa)
        {
            cboPais.DataTextField = "PaisNombre";
            cboPais.DataValueField = "PaisId";
            cboPais.DataSource = dtPa;
            cboPais.DataBind();
        }
        public void ComboDepartamentos(object dtDe)
        {
            cboDepartamento.DataTextField = "DepNombre";
            cboDepartamento.DataValueField = "DepId";
            cboDepartamento.DataSource = dtDe;
            cboDepartamento.DataBind();
        }
        public void ComboProvincias(object dtPr)
        {
            cboProvincia.DataTextField = "ProvNombre";
            cboProvincia.DataValueField = "ProvId";
            cboProvincia.DataSource = dtPr;
            cboProvincia.DataBind();
        }
        public void ComboDistritos(object dtDi)
        {
            cboDistrito.DataTextField = "DistNombre";
            cboDistrito.DataValueField = "DistId";
            cboDistrito.DataSource = dtDi;
            cboDistrito.DataBind();
        }


        [WebMethod(EnableSession = true)]
        public static object SeleccionarDepartamento(String DepId)
        {
            DataTable dtPro = (DataTable)HttpContext.Current.Session["Provincias"];
            var consultaPro = from dt in dtPro.AsEnumerable() where dt.Field<int>("DepId") == Convert.ToInt32(DepId) select new { ProvId = dt.Field<int>("ProvId"), ProvNombre = dt.Field<string>("ProvNombre") };

            //ComboProvincias(consultaPro);

            int idPro = consultaPro.FirstOrDefault().ProvId;
            DataTable dtDist = (DataTable)HttpContext.Current.Session["Distritos"];
            var consultaDist = from dt in dtDist.AsEnumerable() where dt.Field<int>("ProvId") == idPro select new { DistId = dt.Field<int>("DistId"), DistNombre = dt.Field<string>("DistNombre") };
            //ComboDistritos(consultaDist);

            return new
            {
                Result = "Ok",
                Msg = "Saved Successfully",
                consultaPro = consultaPro,
                consultaDist = consultaDist,
            };
        }

        [WebMethod(EnableSession = true)]
        public static object SeleccionarProvincia(String ProvId)
        {
            DataTable dtDist = (DataTable)HttpContext.Current.Session["Distritos"];
            var consultaDistXPro = from dt in dtDist.AsEnumerable() where dt.Field<int>("ProvId") == Convert.ToInt32(ProvId) select new { DistId = dt.Field<int>("DistId"), DistNombre = dt.Field<string>("DistNombre") };
            //ComboDistritos(consultaDist);

            return new
            {
                Result = "Ok",
                Msg = "Saved Successfully",
                consultaDistXPro = consultaDistXPro,
            };
        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            try
            {
                entCliente obj = new entCliente()
                {
                    PaisId = Convert.ToInt32(cboPais.SelectedItem.Value),
                    DepId = Convert.ToInt32(cboDepartamento.SelectedItem.Value),
                    ProvId = Convert.ToInt32(cboProvincia.SelectedItem.Value),
                    DistId = Convert.ToInt32(cboDistrito.SelectedItem.Value),
                    CliNombre = txtNombre.Text,
                    CliApellidoP = txtApellidoP.Text.ToUpper(),
                    CliApellidoM = txtApellidoM.Text.ToUpper(),
                    CliTelefono = txtTelefono.Text,
                    CliCelular = txtCelular.Text,
                    CliEmail = txtEmail.Text.ToUpper(),
                    CliPassword = txtPassword.Text,
                    CliFoto = (Session["NombreFoto"] != null) ? Convert.ToString(Session["NombreFoto"]) : "userdefault.png",
                    CliUsuarioRegistro = txtNombre.Text,
                    CliUsuarioModificacion = "",
                    CliEstado = 1
                };

                entDireccion objD = new entDireccion()
                {
                    DirDescripcion1 = txtDireccion1.Text.ToUpper(),
                    DirDescripcion2 = txtDireccion2.Text.ToUpper(),
                    DirCodPostal = txtCodPostal.Text,
                    DirEstado = 1
                };

                Boolean success = negCliente.Instancia.RegistrarCliente(obj, objD);
                if (success)
                {
                    MostrarMensaje("Se registro Satisfactoriamente!");
                }
            }
            catch (ApplicationException msg)
            {
                MostrarMensaje(msg.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        public static object GuardarCliente(srRCliente c)
        {
            entCliente obj = new entCliente();
            entDireccion objd = new entDireccion();
            try
            {
                obj.PaisId = c.PaisId;
                obj.DepId = c.DepId;
                obj.ProvId = c.ProvId;
                obj.DistId = c.DistId;
                obj.CliNombre = c.CliNombre;
                obj.CliApellidoP = c.CliApellidoP;
                obj.CliApellidoM = c.CliApellidoM;
                obj.CliTelefono = c.CliTelefono;
                obj.CliCelular = c.CliCelular;
                obj.CliEmail = c.CliEmail;
                obj.CliPassword = c.CliPassword;
                obj.CliFoto = c.CliFoto;
                obj.CliUsuarioRegistro = c.CliNombre;
                obj.CliUsuarioModificacion = c.CliUsuarioModificacion;
                obj.CliEstado = c.CliEstado;

                objd.DirDescripcion1 = c.DirDescripcion1;
                objd.DirDescripcion2 = c.DirDescripcion2;
                objd.DirCodPostal = c.DirCodPostal;
                objd.DirEstado = c.DirEstado;

                Boolean success = negCliente.Instancia.RegistrarCliente(obj, objd);
                if (success)
                {
                    return new { Result = "Ok", Msg = "Saved Successfully" };
                }
                else {
                    return new { Result = "NoOk", Msg = "An error ocurred while saving data" };
                }
            }
            catch (Exception ex)
            {
                  return new { Result = "NoOk", Msg = "An error ocurred while saving data" };
            }
        }


        public void MostrarMensaje(string msg)
        {
            string script = @"<script type='text/javascript'>alert('" + msg + "')</script>";
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, false);
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (FU1.HasFile)
            {
                String ext = Path.GetExtension(FU1.FileName).ToLower();
                if (ext == ".jpg" || ext == ".gif" || ext == ".jpeg" || ext == ".png")
                {
                    String Name = FU1.FileName;
                    String uploadFilePath = Server.MapPath("~/Fotos/" + Name);
                    FU1.SaveAs(uploadFilePath);
                    ImgFoto.ImageUrl = "~/Fotos/" + Name;
                    hfNameFoto.Value = Name;
                }

            }
        }
    }
}