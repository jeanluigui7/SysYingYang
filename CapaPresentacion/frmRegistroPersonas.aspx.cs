﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaNegocio;
using CapaEntidades;
using System.Data;
using Library;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CapaPresentacion
{
    public partial class frmRegistroUsuarios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }
        private void LoadData()
        {
            object objPersona = new object();
            List<entPersona> list = new List<entPersona>();
            DataTable dt = negPersona.Instancia.ListarPersonas(1);
            if (dt != null && dt.Rows.Count > 0)
            {
                Int32 count = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    count++;
                    String sId = Server.UrlEncode(clsEncryption.Encrypt(Utility.ValidateDataRowKeyDefault(dr, "ID", "")));
                    list.Add(new entPersona()
                    {
                        isCheckbox = "1",
                        PerId = sId,
                        PerNombre = Utility.ValidateDataRowKeyDefault(dr, "NOMBRE", ""),
                        PerCodigo = Utility.ValidateDataRowKeyDefault(dr, "CODIGO", ""),
                        PerApellidoP = Utility.ValidateDataRowKeyDefault(dr, "APELLIDOPATERNO", ""),
                        PerApellidoM = Utility.ValidateDataRowKeyDefault(dr, "APELLIDOMATERNO", ""),
                        PerEdad = Utility.ValidateDataRowKeyDefault(dr, "EDAD", ""),
                        PerEmail = Utility.ValidateDataRowKeyDefault(dr, "EMAIL", ""),
                        PerSexo = Utility.ValidateDataRowKeyDefault(dr, "SEXO", ""),
                        PerCelular = Utility.ValidateDataRowKeyDefault(dr, "CELULAR", ""),
                        PerTelefono = Utility.ValidateDataRowKeyDefault(dr, "TELEFONO", ""),
                        TipoDocumento = Utility.ValidateDataRowKeyDefault(dr, "TIPODOCUMENTO", ""),
                        NroDocumento = Utility.ValidateDataRowKeyDefault(dr, "NRODOCUMENTO", ""),
                        Pais = Utility.ValidateDataRowKeyDefault(dr, "PAIS", ""),
                        Departamento = Utility.ValidateDataRowKeyDefault(dr, "DEPARTAMENTO", ""),
                        Provincia = Utility.ValidateDataRowKeyDefault(dr, "PROVINCIA", ""),
                        Distrito = Utility.ValidateDataRowKeyDefault(dr, "DISTRITO", ""),
                        Index = count.ToString(),

                    });
                }
                if (list != null)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string json = serializer.Serialize(list);
                    hfData.Value = json.ToString();
                }
            }
        }
        //[WebMethod]
        //public static object EliminarPersonas(String jsondata)
        //{
        //    try
        //    {

        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}
    }
}