﻿; (function ($, window, document, undefined) {

    var $wrap = undefined;
    var $tree = undefined;
    var defaults = undefined;
    var events = ["click", "tap"];
    var mobile = isMobile.All();
    var eventIndex = mobile ? 1 : 0;

    var jqtreexssFunctions = { //PRIVATE FUNCTIONS

        _init: function () {
            this._validateWrappers();
            this._buildTree();
        },
        _validateWrappers: function () {
            if ($tree.length <= 0) {
                $tree = $("<ul id='wrapper-tree'></ul>")
                $wrap.append($tree);
            }
            if (mobile == 1) { $wrap.css("overflow", "auto"); }
        },
        _buildTree: function () {
            if (defaults.data == undefined || defaults.data.length <= 0) { return; }

            for (var i = 0; i < defaults.data.length; i++) {
                if (defaults.data[i].ParentId == 0)
                    $tree.append("<li id='" + defaults.data[i].Id + "' data-code='" + defaults.data[i].Encry + "' data-expanded='true'>" +// item-expanded='true'
                                "<div class='showing-name' title='" + defaults.data[i].Name + "'>" + defaults.data[i].Name + "</div>" +
                                "<div class='showing-value'>" + defaults.data[i].Value + "</div>" +
                                "<div class='showing-icon'>" + defaults.data[i].Icon + "</div>" +
                              "</li>");
                else {
                    var $li = $("#" + defaults.data[i].ParentId);
                    if ($li.length <= 0) continue;

                    var $innerUl = $li.find("ul:first");
                    if ($li.find("ul").length <= 0) { $innerUl = $("<ul></ul>"); $li.append($innerUl); }

                    $innerUl.append("<li id='" + defaults.data[i].Id + "' data-code='" + defaults.data[i].Encry + "' data-expanded='false'>" +
                                        "<div class='showing-name' title='" + defaults.data[i].Name + "'>" + defaults.data[i].Name + "</div>" +
                                        "<div class='showing-value'>" + defaults.data[i].Value + "</div>" +
                                        "<div class='showing-icon'>" + defaults.data[i].Icon + "</div>" +
                                  "</li>");
                }
            }

            this._manageItems();
            if (mobile == 0) {
                try { $wrap.perfectScrollbar('destroy'); } catch (e) { }
                try { $wrap.perfectScrollbar(); } catch (e) { }
            }
        },
        _manageItems: function () {
            var arry = $tree.find("li");

            if (arry.length <= 0) return;

            for (var i = 0; i < arry.length; i++) {
                var $li = $(arry[i]);
                this._iconEvents($li);
            }
        },
        _iconEvents: function ($li) {
            if ($li.find("ul").length <= 0) {
                $li.data("children", false);
                if ($li.find(".arrow-state:first").length <= 0)
                    $li.prepend("<span class='arrow-state'></span>");
            }
            else {
                $li.data("children", true);
                if ($li.data("expanded") == true) {
                    if ($li.find(".arrow-state:first").length <= 0)
                        $li.prepend("<span class='arrow-state arrow-showing'></span>");
                    else
                        $li.find(".arrow-state:first").addClass("arrow-showing");
                }
                else {
                    $li.find("ul:first").hide();
                    if ($li.find(".arrow-state:first").length <= 0)
                        $li.prepend("<span class='arrow-state arrow-hidding'></span>");
                    else
                        $li.find(".arrow-state:first").addClass("arrow-hidding");
                }
            }

            this._bindEvents($li);
        },
        _bindEvents: function ($li) {

            var $div = $li.find("div.showing-name:first");
            if ($li.data("children")) {
                $div.on(events[eventIndex], function () {

                    var $span = $li.find(".arrow-state:first");
                    if ($li.data("expanded")) {
                        $li.find("ul:first").hide(0, function () {
                            $tree.css("width", $wrap.width() + "px");
                        });
                        $span.removeClass("arrow-showing").addClass("arrow-hidding");
                        $li.data("expanded", false);
                    }
                    else {
                        $li.find("ul:first").show(0, function () {
                            $tree.css("width", $wrap.width() + "px");
                        });
                        $span.removeClass("arrow-hidding").addClass("arrow-showing");
                        $li.data("expanded", true);
                    }

                    if (mobile == 0) {
                        try { $wrap.perfectScrollbar('destroy'); } catch (e) { }
                        try { $wrap.perfectScrollbar(); } catch (e) { }
                    }
                });
            }
            else {
                if (defaults.hasEvents == 1) {
                    var $this = this;
                    $div.on(events[eventIndex], function () {
                        try {
                            if (mobile == 0) { try { $wrap.perfectScrollbar('destroy'); } catch (e) { } }
                            $wrap.append("<div class='loader_fb_16x16'></div>");
                            var code = $li.data("code");
                            var success = function (asw) {
                                if (asw.d.length <= 0) { return; }
                                var obj = asw.d;
                                if (obj.Result == "NoOk") { alert(obj.Message); return; }
                                if (obj.Data == undefined || obj.Data.length == 0) { return; }

                                for (var i = 0; i < obj.Data.length; i++) {

                                    var $newLi = $("<li id='" + obj.Data[i].Id + "' data-code='" + obj.Data[i].Encry + "'>" +// item-expanded='true'
                                        "<div class='showing-name' title='" + obj.Data[i].Name + "'>" + obj.Data[i].Name + "</div>" +
                                        "<div class='showing-value'>" + obj.Data[i].Value + "</div>" +
                                        "<div class='showing-icon'>" + obj.Data[i].Icon + "</div>" +
                                      "</li>");

                                    var $ul = $li.find("ul:first");
                                    if ($ul.length <= 0) { $ul = $("<ul></ul>"); $li.append($ul); }
                                    $.when($ul.append($newLi)).then(function () { $tree.css("width", $wrap.width() + "px"); });

                                    $newLi.data("children", false);
                                    $this._iconEvents($newLi);
                                }

                                $li.data("children", true);
                                $li.attr("data-expanded", true);
                                $div.off(events[eventIndex]);
                                $this._iconEvents($li);
                            };
                            var error = function (xhr, ajaxOptions, thrownError) { };
                            var complete = function () { $wrap.find(".loader_fb_16x16").remove(); if (mobile == 0) { try { $wrap.perfectScrollbar(); } catch (e) { } } };
                            fn_callmethod("../../src/async/AsyncCall.aspx/SearchNextLevelDistributor", "{ID:'" + code + "', LEVEL:'" + 0/*selectedItem.level*/ + "'}", success, error, complete);
                        }
                        catch (e) { }
                    });
                }
            }
        },
        _destroy: function () {
            $wrap.empty();
        }
    };


    var methods = { //PUBLIC METHODS
        init: function (options) {
            defaults = $.extend({ data: undefined, hasEvents: 0 }, options)
            $wrap = $(this);
            $tree = $wrap.find("ul:first");
            jqtreexssFunctions._init();
        },
        debug: function ($wrap, $tree) {
            if (window.console && window.console.log) {
                window.console.log("Elements: Wrap (" + $wrap.length + ") - Tree (" + $tree.length + ")");
            }
        },
        destroy: function () {
            jqtreexssFunctions._destroy();
        }
    };


    $.fn.jqtreexss = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.jqtreexss');
        }
    };
})(jQuery, window, document);