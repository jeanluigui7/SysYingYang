﻿$(function ($) {
    $.fn.aspxPage({
        products: JSON.parse($("input[id$=hfData]").val()),
        selectedProducts: JSON.parse($("input[id$=productsSelected]").val()),
        categories: JSON.parse($("input[id$=hfDataCategories]").val()),
        startDateDay: $("input[type=hidden][id$=hfStartDateDay]").val(),
        SkuMg: $("input[type=hidden][id$=hfSkuManagment]").val(),
        txtSelected: undefined
        //aspxPage_G: aspxPage_G
    });
});

; (function ($, window, document, undefined) {

    var defaults = undefined;

    var aspxPageFunctions = { //PRIVATE FUNCTIONS

        _init: function () {
            this._setTitles();
            this._loadCategories();
            this._loadProducts();
            this._setSelectedProducts();
            this._bind();
            this._countTotal();
            this._bindOnChange();
            this._plugin();
            this._content();
            ajaxManager.run();

            if ($("select[id$=ddlYear]").val() != "0" && $("select[id$=ddlMonth]").val() != "0") 
            {
                var y = parseInt($("select[id$=ddlYear]").val());
                var m = parseInt($("select[id$=ddlMonth]").val());
                var a = 25;//new Date(y, m, 0).getDate();
                var html = "";
                html += "<option value='0'>-- " + aspxPage_G.String.SELECT + " --";

                for (var i = 1; i <= a; i++) {
                    html += "<option value='" + i + "'>" + i;
                }

                $("select[id$=ddlDay]").append(html);
                $("select[id$=ddlDayRun]").append(html);

                $("select[id$=ddlDay]").val($("input[type=hidden][id$=hfStartDateDay]").val());

            }
        },
        _bind: function () {
            var $this = this;
            $("select[id$=ddlMarket]").change(function () { //LIST CATEGORIES TOO

                $("#wrapper_contentproducts, #div_cat .panel-body:first").append("<div class='loader-snake-jax'></div>")

                var success = function (asw) {

                    var o = asw.d;

                    if (o.Result == "NoOk") { fn_message('i', o.Message); return; }

                    $this._setShoppingCartValues(undefined);

                    defaults.products = o.products;
                    defaults.categories = o.categories                    
                    $this._loadProducts()
                    $this._loadCategories();
                    $this._bindOnChange();

                    if ($("#div_cat ul li:first").length > 0 && $("#div_cat ul li:first").attr("id") != undefined && $("#div_cat ul li:first").attr("id").length > 3)
                        $this._showByCategory($("#div_cat ul li:first").attr("id").substring(3));

                };

                var error = function (xhr, ajaxOptions, thrownError) {
                    fn_message('i', aspxPage_G.Messages.ERROR_SENDING);
                };
                var complete = function () {
                    if ($("#wrapper_contentproducts, #div_cat .panel-body:first").find(".loader-snake-jax").length > 0)
                        $("#wrapper_contentproducts, #div_cat .panel-body:first").find(".loader-snake-jax").remove();
                }

                fn_callmethod(aspxPage_G.RWMDomain.replace("{0}", 'GetCategoriesAndProductsByMarketAndAccountType'), '{ code: "' + $(this).val() + '", o: "' + aspxPage_G.Properties.ssak + '", lng:"' + aspxPage_G.Properties.sslk + '"}', success, error, complete);
            });

            $(".btn-mobile").click(function () {
                $(".formSep").toggle("fast");
                if ($(".title_step").data('show') == "1") {
                    $(".title_step").hide();
                    $(".title_step").data('show', "0");
                }
                else {
                    $(".title_step").show();
                    $(".title_step").data('show', "1");
                }
            });

            $("a[id$=btnContinue]").click(function () {

                if ($("select[id$=ddlMonth]").val() != "0" && $("select[id$=ddlDay]").val() != "0" && $("select[id$=ddlYear]").val() != "0" /*&& $("select[id$=ddlDayRun]").val() != "0"*/) {

                    $("input[type=hidden][id$=hfStartDateDay]").val($("select[id$=ddlDay]").val());
                    $("input[type=hidden][id$=hfDateRun]").val($("select[id$=ddlDay]").val());

                    return true;
                }

                fn_message("i", aspxPage_G.Messages.INVALID_STARTDATE);

                return false;
            });

            $("select[id$=ddlDay]").change(function () {
                $("input[type=hidden][id$=hfStartDateDay]").val($(this).val());
                if ($(this).val() != undefined && $(this).val() != "0") {
                    $("select[id$=ddlDayRun]").val($(this).val());
                }
            });

            if ($("a[id$=btnSearchDist]").length > 0)
                $("a[id$=btnSearchDist]").click(function (e) {
                    if ($.trim($("input[type=text][id$=txtDistributor]").val()) == "") {
                        fn_message('i', aspxPage_G.Messages.WARNING_PLEASEETERAVALIDVALUE);
                        $("input[type=text][id$=txtDistributor]").focus();
                        e.preventDefault();
                    }
                });

            if ($(".searchDist").length > 0) {
                $(".searchDist").click(function () {

                    var target = $($(this).data("target"));
                    if (target.val() == "") { fn_message('i', aspxPage_G.Messages.WARNING_PLEASEETERAVALIDVALUE); target.focus(); return; }

                    var call = $(this).data("call");
                    var wrapperLoader = $($(this).data("loader"));

                    wrapperLoader.append("<div class='loader-snake-jax-transp'></div>");
                    target.attr("readonly", "readonly");

                    var success = function (asw) {
                        var o = asw.d;

                        if (o.Result == "NoOK") { fn_message('i', o.Message); return; }

                        if (o.sr.length <= 0) { return; }

                        $('#tbSearchDistGrid').DataTable().fnClearTable();

                        $this._loadDistributors(o.sr);
                    };
                    var error = function (xhr, ajaxOptions, thrownError) {
                        fn_message('i', aspxPage_G.Messages.ERROR_SENDING);
                    };
                    var complete = function () {

                        if (wrapperLoader.find(".loader-snake-jax-transp").length > 0)
                            wrapperLoader.find(".loader-snake-jax-transp").remove();

                        target.removeAttr("readonly", "readonly");
                    }

                    fn_callmethod(aspxPage_G.RWMDomain.replace("{0}", call), '{ value: "' + target.val() + '", o: "' + aspxPage_G.Properties.ssak + '", lng:"' + aspxPage_G.Properties.sslk + '"}', success, error, complete);
                });

            }

            $("select[id$=ddlMonth]").change(function (){
                if ($("select[id$=ddlYear]").val() != "0") {
                    var y = parseInt($("select[id$=ddlYear]").val());
                    var m = parseInt($("select[id$=ddlMonth]").val());
                    //var a = new Date(y, m, 0).getDate();
                    var a = 25;
                    var html = "";
                    //$("select[id$=ddlDay]").empty();
                    //$("select[id$=ddlDayRun]").empty();
                    html += "<option value='0'>-- " + aspxPage_G.String.SELECT + " --</option>";
                    for (var i = 1; i <= a; i++) {
                        html += "<option value='" + i + "'>" + i + "</option>";
                    }
                    $("select[id$=ddlDay]").html(html);
                    $("select[id$=ddlDayRun]").html(html);
                }
                
            });

            //$("[name$=Frecuency]").on("change", this, function () {
            //    var frecuency = 0;
            //    var radio = this.value;
            //    if(this.value == "rbMonthlyFrecuency")
            //    {
            //        frecuency = 0;
            //    }else if(this.value == "rbAnualFrecuency")
            //    {
            //        frecuency = 1;
            //    }
            //    var params = { o: aspxPage_G.Properties.ssak, Frecuency: frecuency };
            //    var success = function (response) {
            //        if (response.d.Result == "Ok") {
            //            $(".tblProducts tbody tr").hide();
            //            if (radio == 'rbMonthlyFrecuency') {
            //                $(".tblProducts tbody tr.cat" + $("[id$=hfcatcategories]").val() + ".fre0").show();
            //            } else if (radio == 'rbAnualFrecuency') {
            //                $(".tblProducts tbody tr.cat" + $("[id$=hfcatcategories]").val() + ".fre1").show();
            //            }
            //            $("[id$=lblItems]").text("0");
            //            $("[id$=lblTotal]").text("0.00");
            //        }
            //        $(".sp_basictxt").val(0);
            //    };
            //    var error = function (xhr, ajaxOptions, thrownError) {
            //        fn_message('i', "An error occurred sending data.");
            //    };
            //    fn_callmethod("Products.aspx/SessionFrecuency", JSON.stringify(params), success, error);                
            //})

            $("input[type=text][id$=txtDistributor]").focus(function () { defaults.txtSelected = 1; });
            $("input[type=text][id$=txtNameDistributor]").focus(function () { defaults.txtSelected = 2; });
            $("input[type=text][id$=txtLastNameDistributor]").focus(function () { defaults.txtSelected = 3; });

            $(document).keypress(function (e) {
                e.stopPropagation();
                if (e.which == 13) {
                    if (defaults.txtSelected == 1) {
                        __doPostBack('ctl00$ctl00$ContentPlaceHolder1$ContentPlaceHolder3$btnSearchDist', '');
                    }
                    else {
                        if (defaults.txtSelected == 2) {
                            $("a[id=btnSearchNameDist]").click();
                        }
                        else if (defaults.txtSelected == 3) {
                            $("a[id=btnSearchLastName]").click();
                        }
                    }
                }
            });

            $('.cart').hover(function () {
                $("#liTotalCart").show();
                $("#liTotalCartAutoship").show();

                $("#itemsOrderTitle").show();
                $("#itemsAutoshipTitle").show();
            }, function () {
                $("#liTotalCart").hide();
                $("#liTotalCartAutoship").hide();

                $("#itemsOrderTitle").hide();
                $("#itemsAutoshipTitle").hide();
            });
        },
        _content: function () {
            $("select[id$=ddlYear]").change();
        },
        _bindOnChange: function () {
            //$('.details').click(function () {
            //    var id = $(this).data('id');
            //    if ($('#pro_details' + id).data("view") == 0) {
            //        $('.pro_details').fadeOut(100);
            //        $('.pro_details').data("view", 0);
            //        $('#pro_details' + id).fadeIn(100);
            //        $('#pro_details' + id).data("view", 1)
            //    }
            //    else {
            //        $('.pro_details').fadeOut(100);
            //        $('.pro_details').data("view", 0);
            //        $('#pro_details' + id).fadeOut(100);
            //        $('#pro_details' + id).data("view", 0)
            //    }
            //});
            $("#Div_Products").on("click", ".details", function () {
                if ($(this).next().data("view") == 0) {
                    $(this).next().fadeOut(500);
                    $(this).next().data("view", 0);
                    $(this).next().fadeIn(500);
                    $(this).next().data("view", 1);
                }
                else {
                    $(this).next().fadeOut(500);
                    $(this).next().data("view", 0);
                    $(this).next().fadeOut(500);
                    $(this).next().data("view", 0);
                }
            });
            this._enableSpinner();
        },
        _setTitles: function () {
            var $this = this;

            var number = $("li[class$=current-step]").data("number");
            var steptitle = $("li[class$=current-step]").find("span[class=title-name]").text();
            $("#spanStepNumber").text(number);
            $("#lblTitleStep").text(steptitle);
        },
        _setShoppingCartValues: function (shoppingCart) {

            //var currencySimbol = $("[id$=hfCurrencySymbol]").val();

            if (shoppingCart == undefined) {
                if ($("span[id$=lblItems]").length > 0)
                    $("span[id$=lblItems]").text("0");
                if ($("span[id$=lblTotal]").length > 0)
                    $("span[id$=lblTotal]").text("0.00");
                if ($("span[id$=lblRetailSubTotal]").length > 0)
                    $("span[id$=lblRetailSubTotal]").text("0.00");
                if ($("span[id$=lblPV]").length > 0)
                    $("span[id$=lblPV]").text("0");
                if ($("span[id$=lblSubtotal]").length > 0)
                    $("span[id$=lblSubtotal]").text("0.00");
                if ($("span[id$=lblCvTotal]").length > 0)
                    $("span[id$=lblCvTotal]").text("0");
                
            }
            else {
                
                if ($("span[id$=lblItems]").length > 0)
                    $("span[id$=lblItems]").text(shoppingCart.ItemsOrder == undefined ? "0" : shoppingCart.ItemsOrder);

                if ($("span[id$=lblTotal]").length > 0)
                    $("span[id$=lblTotal]").text(shoppingCart.TotalOrder == undefined ? "0.00" :shoppingCart.TotalOrder);

                if ($("span[id$=lblRetailSubTotal]").length > 0)
                    $("span[id$=lblRetailSubTotal]").text(shoppingCart.TotalOrderRetail == undefined ? "0.00" : shoppingCart.TotalOrderRetail);

                if ($("span[id$=lblPV]").length > 0)
                    $("span[id$=lblPV]").text(shoppingCart.TotalOrderPV == undefined ? "0" : shoppingCart.TotalOrderPV);

                if ($("span[id$=lblSubtotal]").length > 0)
                    $("span[id$=lblSubtotal]").text(shoppingCart.TotalOrder == undefined ? "0.00" : shoppingCart.TotalOrder);

                if ($("span[id$=lblCvTotal]").length > 0)
                    $("span[id$=lblCvTotal]").text(shoppingCart.TotalOrderPV == undefined ? "0" : shoppingCart.TotalOrderPV);
            }
        },
        _plugin: function () {
            //this._enableSpinner();
        },
        _enableSpinner: function () {
            $(".sp_basictxt ").spinner();
            var $this = this;
            $(".txtquantity2").on('spinstop', function () {

                var val = $(this).val();
                if (isNaN(val) || $this._isFloat(val) || parseInt(val) < 0) { fn_message("i", aspxPage_G.Messages.WARNING_SELECTVALIDQTY); return; }

                setTimeout($.proxy(function () {
                    //if (defaults.SkuMg == "1") {
                    //    var listChild = [];
                    //    $(this).closest("td").find("#div" + $(this).data("id") + "_" + $(this).data("attribute")).find(".quantityvalue").each(function () {
                    //        var tmp = $(this).val() != "" ? $(this).val() : $(this).text() != "" ? $(this).text() : 0;
                    //        listChild.push({ Id: parseInt($(this).next().attr("id")), Quantity: tmp });
                    //    });
                    //    $this._addProductSku($(this).parent(), $(this).data("skuid"), $(this).data("id"), $(this).val(), aspxPage_G.Properties.ssak, aspxPage_G.Properties.sslk, listChild, $(this).data("attribute"), $(this).data("warehouseid"), $(this).data("frecuency"), $(this).data("issubscription"));
                    //} else {
                    //    $this._addProduct($(this).parent(), $(this).data("skuid"), $(this).data("id"), $(this).val(), aspxPage_G.Properties.ssak, aspxPage_G.Properties.sslk);
                    //}
                    var listChild = [];
                    $(this).closest("td").find("#div" + $(this).data("id") + "_" + $(this).data("attribute")).find(".quantityvalue").each(function () {
                        var tmp = $(this).val() != "" ? $(this).val() : $(this).text() != "" ? $(this).text() : 0;
                        listChild.push({ Id: parseInt($(this).next().attr("id")), Quantity: tmp });
                    });
                    $this._addProductSku($(this).parent(), $(this).data("skuid"), $(this).data("id"), $(this).val(), aspxPage_G.Properties.ssak, aspxPage_G.Properties.sslk, listChild, $(this).data("attribute"), $(this).data("warehouseid"), $(this).data("frecuency"), $(this).data("issubscription"));

                }, this), 800);
            });
        },
        _showByCategory:  function (n) {

            if (n == undefined || $.trim(n) == "") return;             
            
            //$(".tblProducts tbody tr").hide();
            //if ($("[id$=rbMonthlyFrecuency]").is(':checked') == true) {
            //    $(".tblProducts tbody tr.cat" + n + ".fre0").show();
            //} else if ($("[id$=rbAnualFrecuency]").is(':checked') == true) {
            //    $(".tblProducts tbody tr.cat" + n + ".fre1").show();
            //}
            $(".tblProducts tbody tr.cat" + n).show();
            $("#div_cat li.itemcat").removeClass("selected");
            $("#div_cat li#cat" + n).addClass("selected");
            //$("[id$=hfcatcategories]").val(n)
            
        },

        _loadProducts: function () {

            $("#Div_Products").empty();

            if (defaults.products == undefined || defaults.products.length <= 0) { $("a[id$=btnContinue]").hide(); $(".notdata").show(); return; }

            $(".notdata").hide();
            $("a[id$=btnContinue]").show();
           
            var append = "";
            for (var i = 0; i < defaults.products.length; i++) {
                var isset = defaults.products[i].isSet == 1 ? '<div class="col-lg-4 col-md-4 col-sm-4"><font class="customizeFont" onclick="fn_showProductsChildsCustomize(\'' + defaults.products[i].Id + '\', this)">Customize</font></div>' : '';

                /*append += '<tr class="cat' + defaults.products[i].CategoryId + ' fre' + defaults.products[i].frecuency + '" id="kit' + defaults.products[i].Id + '" data-unitprice="' + defaults.products[i].UnitPrice + '" data-dollarsprice="' + defaults.products[i].DollarsPrice + '" data-retailprice="' + defaults.products[i].RetailPrice + '">' +
                                '<td data-title=" "class="cant tdTam" style="vertical-align: middle;"  data-attribute="' + defaults.products[i].AttributeId + '" data-id="' + defaults.products[i].Id + '">' +
                                    '<div class="pro-qty" style="text-align: initial; overflow: hidden;">' +
                                        //'<label>Case Quantity</label>' +
                                        '<div class="col-lg-4 col-md-4 col-sm-4">' + // class="col-sm-4 col-md-4" style="float: initial;max-width: 85px;"
                                            '<input data-warehouseid="' + (defaults.products[i].warehouseId == "" ? "0" : defaults.products[i].warehouseId) + '"  data-attribute="' + (defaults.products[i].AttributeId == "" ? "0" : defaults.products[i].AttributeId) + '"  data-skuid="' + defaults.products[i].SkuId + '" data-id="' + defaults.products[i].Id + '" min="0" max="' + defaults.products[i].QuantityOnHand + '" data-issubscription="' + defaults.products[i].issubscription + '" data-frecuency="' + defaults.products[i].frecuency + '" value="0" name="sp_basic" class="sp_basictxt form-control txtquantity2 ui-spinner-input prime' + defaults.products[i].issubscription + ' fre' + defaults.products[i].frecuency + '" type="text" aria-valuenow="41" autocomplete="off" role="spinbutton" id="txt' + defaults.products[i].Id + '_' + (defaults.products[i].AttributeId == "" ? "0" : defaults.products[i].AttributeId) + '" ">' +
                                        '</div>' + isset +
                                    '</div>' +
                                    '<div id="div' + defaults.products[i].Id + '_' + (defaults.products[i].AttributeId == "" ? "0" : defaults.products[i].AttributeId) + '"></div>' +
                                '</td>' +
                                '<td colspan="2" data-title="Product" class="img" style="vertical-align: middle;">' +
                                    '<img src="' + defaults.products[i].ImageByDistributor + '" onerror="this.src=\'/src/images/default.jpg\';" style="width: 35px;">' +
                                    '&nbsp;&nbsp;' + defaults.products[i].Name +
                                    '<font class="details" data-id="' + defaults.products[i].Id + '">(' + aspxPage_G.String.DETAILS + ')</font>' +
                                    '<p id="pro_details' + defaults.products[i].Id + '" class="pro_details" style="display:none;" data-view="0">' + $('<div />').html($('<div />').html($('<div />').html(defaults.products[i].FullDescription).text()).text()).text() + '</p>' +
                                '</td>' +
                                '<span style="display:none;" id="name' + defaults.products[i].Id + '_' + (defaults.products[i].AttributeId == "" ? "0" : defaults.products[i].AttributeId) + '">' + defaults.products[i].Name + '</span>' + //Attribute Full Name
                                '<td data-title="Wholesale" style="vertical-align: middle;">' +
                                    '<div class="pro-price">' +
                                        '<p><span class="font-w-700">' + defaults.products[i].MarketPrice + '</span></p>' +
                                    '</div>' +
                                '</td>' +
                                '<td data-title="CV" style="vertical-align: middle;">' +
                                    '<div class="pro-price">' +
                                        '<p><span class="font-w-700">' + parseInt(defaults.products[i].CommisionValue) + '</span></p>' +
                                    '</div>' +
                                '</td>' +
                                '<td data-title="QV" style="vertical-align: middle;">' +
                                    '<div class="pro-price">' +
                                        '<p><span class="font-w-700">' + parseInt(defaults.products[i].qualifyvolumen) + '</span></p>' +
                                    '</div>' +
                                '</td>' +
                            '</tr>';*/
                append += '' +
                    '<tbody class="cellProd cat' + defaults.products[i].CategoryId + ' fre' + defaults.products[i].frecuency + '" id="kit' + defaults.products[i].Id + '" data-unitprice="' + defaults.products[i].UnitPrice + '" data-retailprice="' + defaults.products[i].RetailPrice + '">' +
                        '<tr class="addProduct" data-skuid="' + defaults.products[i].SkuId + '" data-id="' + defaults.products[i].Id + '" data-issub="' + defaults.products[i].issubscription + '">' +
                            '<td class="" style="vertical-align: middle;"></td>' +
                            '<td class="img" style="vertical-align: middle;">' +
                                '<img src="' + defaults.products[i].ImageByDistributor + '" onerror="this.src=\'/src/images/default.jpg\';" style="width: 84px;">' +
                                '<div>' +
                                    '<h5>' + defaults.products[i].SkuId + '</h5>' +
                                '</div>' +
                            '</td>' +
                            '<td class="name" style="vertical-align: middle; font-size: 25px; line-height: 30px;" data-name="' + defaults.products[i].Name + '">' +
                                '<p class="details font-lato-h4" data-id="' + defaults.products[i].Id + '">' + defaults.products[i].Name +
                                    '<font class="font-lato-h4" style="color:#9cdbd9 !important;cursor: pointer;">(' + aspxPage_G.String.DETAILS + ')</font>' +
                                '</p>' +
                                '<p id="pro_details' + defaults.products[i].Id + '" class="pro_details" style="display:none;" data-view="0">' + $('<div />').html($('<div />').html($('<div />').html(defaults.products[i].FullDescription).text()).text()).text() + '</p>' +
                                
                                '<h5><span class="font-lato-h4">' + defaults.products[i].UnitPrice + '</span></h5>' +
                                '<h5><span class="font-lato-h4">CV ' + parseInt(defaults.products[i].CommisionValue) + '</span></h5>' +

                            '</td>' +
                            '<td class="" style="vertical-align: middle;"></td>' +
                            '<td class="" style="vertical-align: middle;"></td>' +
                            '<td data-title="" class="cant tdTam" style="vertical-align: middle;" data-attribute="' + defaults.products[i].AttributeId + '" data-id="' + defaults.products[i].Id + '">' +
                                '<div class="pro-qty" style="overflow: hidden;">' +
                                    '<div class="col-lg-4 col-md-offset-8 col-md-4 col-sm-4" style="max-width: 85px;">' +
                                        '<input data-warehouseid="' + (defaults.products[i].warehouseId == "" ? "0" : defaults.products[i].warehouseId) + '" data-attribute="' + (defaults.products[i].AttributeId == "" ? "0" : defaults.products[i].AttributeId) + '"  data-skuid="' + defaults.products[i].SkuId + '" data-id="' + defaults.products[i].Id + '" min="0" max="' + defaults.products[i].QuantityOnHand + '" data-issubscription="' + defaults.products[i].issubscription + '" data-frecuency="' + defaults.products[i].frecuency + '" value="0" name="sp_basic" class="sp_basictxt form-control txtquantity2 ui-spinner-input prime' + defaults.products[i].issubscription + ' fre' + defaults.products[i].frecuency + '" type="text" aria-valuenow="41" autocomplete="off" role="spinbutton" id="txt' + defaults.products[i].Id + '_' + (defaults.products[i].AttributeId == "" ? "0" : defaults.products[i].AttributeId) + '" ">' +
                                    '</div>' + isset +
                                '</div>' +
                                '<div id="div' + defaults.products[i].Id + '_' + (defaults.products[i].AttributeId == "" ? "0" : defaults.products[i].AttributeId) + '"></div>' +
                            '</td>' +
                        '</tr>' +
                    '</tbody>';
            }

            var wholesale = $("[id$=hfWholesale]").val();
            var prefered = $("[id$=hfPrefered]").val();
            var retail = $("[id$=hfRetail]").val();


            var html = '' +
                            '<div id="no-more-tables" class="col-sm-12 col-md-12 CenterPanel">' +
                                '<table class="tblProducts table" style="border: 1px solid #dddddd;">' +
                                    /*'<thead>' +
                                        '<th></th>' +
                                        '<th colspan="2">' + aspxPage_G.String.PRODUCTTITLE + '</th>' +
                                        //'<th style="text-align:center;">Retail</th>' +
                                        //'<th style="text-align:center;">' + aspxPage_G.String.PRICETITLE + '</th>' +
                                        (wholesale == "1" ? '<th style="text-align:center;">' + aspxPage_G.String.WHOLESALETITLE + '</th>' : "") +
                                        (prefered == "1" ? '<th style="text-align:center;">' + aspxPage_G.String.PREFERREDTITLE + '</th>' : "") +
                                        (retail == "1" ? '<th style="text-align:center;">' + aspxPage_G.String.RETAILTITLE + '</th>' : "") +
                                        '<th style="text-align:center;">' + aspxPage_G.String.PVTITLE + '</th>' +
                                        '<th style="text-align:center;">' + aspxPage_G.String.QVTITLE + '</th>' +
                                    '</thead>' +*/
                                    //'<tbody class="cellProd">' +
                                        //'{0}' +
                                        append +
                                    //'</tbody>' +
                                '</table>' +
                            '</div>' +
                        '</div>';
            
            $("#Div_Products").append(html);//.replace('{0}', append));
            $(".view-total").show();

            if ($("#div_cat ul li:first").length > 0 && $("#div_cat ul li:first").attr("id") != undefined && $("#div_cat ul li:first").attr("id").length > 3)
                this._showByCategory($("#div_cat ul li:first").attr("id").substring(3));
        },

        _loadCategories: function () {

            if (defaults.categories == undefined || defaults.categories.length == 0) { return; }

            $("#div_cat ul").empty();

            var append = "";
            for (var i = 0; i < defaults.categories.length; i++) {
                append += '<li class="itemcat" id="cat' + defaults.categories[i].Id + '" >' + //class="itemcat' + defaults.categories[i].Id + '"
                                '<a href="javascript:void(0)" onclick="return $.fn.aspxPage(\'showByCategory\', \'' + defaults.categories[i].Id + '\')" >' +
                                    '<i class="icon-caret-right"></i> ' + defaults.categories[i].Name +
                                '</a>' +
                          '</li>';
            }

            $("#div_cat ul").append(append);
        },
        _addProduct: function (wrapper, sku, code, q, ssak, sslk) {
            var $this = this;
            var params = {
                code: code,
                sku: sku,
                q: q,
                o: ssak,
                lng: sslk,
                ListChild: [],
                Attribute: "0",
                Description: ""
            }
            var x =
            {
                code: code,
                wrapper: wrapper,
                ajaxloaderClass: 'loader-snake-jax',
                ajax: {
                    type: 'POST',
                    url: aspxPage_G.RWMDomain.replace("{0}", 'AddProduct'),
                    data: JSON.stringify(params),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (asw) {
                        var o = asw.d;

                        if (o.Result == "NoOk") { fn_message("i", o.Message); return; }

                        fn_message("s", o.Message);
                        $this._setShoppingCartValues(o.Totals);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        fn_message('i', aspxPage_G.Messages.ERROR_SENDING);
                    },
                    complete: function () {
                        if (wrapper.find(".loader-snake-jax").length > 0)
                            wrapper.find(".loader-snake-jax").remove();
                    }
                }
            }

            ajaxManager.addReq(x);
        },
        _addProductSku: function (wrapper, sku, code, q, ssak, sslk, listChild, attribute, warehouseId, frecuency, issubscription) {
            var $this = this;
            var d = $('#name' + code + '_' + attribute).text();
            var params = {
                code: code,
                warehouseId: warehouseId,
                sku: sku,
                q: q,
                o: ssak,
                lng: sslk,
                ListChild: listChild,
                Frecuency: frecuency,
                IsSubscription: issubscription,
                Attribute: attribute,
                Description: d
            }
            var x =
            {
                code: code,
                wrapper: wrapper,
                ajaxloaderClass: 'loader-snake-jax',
                ajax: {
                    type: 'POST',
                    url: aspxPage_G.RWMDomain.replace("{0}", 'AddProduct'),
                    data: JSON.stringify(params),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (asw) {
                        var o = asw.d;

                        if (o.Result == "NoOk") {
                            fn_message("i", o.Message);
                            if (o.ProPrime == "1") {
                                $(".fre0").val(0);
                            } else if (o.ProPrime == "0") {
                                $(".fre1").val(0);
                            }
                            return;
                        }

                        fn_message("s", o.Message);
                        $this._countTotal();
                        $this._setShoppingCartValues(o.Totals);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        fn_message('i', aspxPage_G.Messages.ERROR_SENDING);
                    },
                    complete: function () {
                        if (wrapper.find(".loader-snake-jax").length > 0)
                            wrapper.find(".loader-snake-jax").remove();
                    }
                }
            }

            ajaxManager.addReq(x);
        },
        _isFloat: function (val) {
            return val % 1 != 0;
        },
        _setSelectedProducts: function () {
            if (defaults.selectedProducts == undefined || defaults.selectedProducts.length <= 0) { return; }

            if (defaults.SkuMg == "1") {

                for (var i in defaults.selectedProducts) {

                    $("#txt" + defaults.selectedProducts[i].Id + "_" + defaults.selectedProducts[i].AttributeId).val(defaults.selectedProducts[i].Quantity);

                    /***  SET CHILITEMS SELECTED  ***/
                    if (defaults.selectedProducts[i].ChildItems.length > 0) {
                        var tam = defaults.selectedProducts[i].ChildItems.length;
                        var tabla = "<div class='col-lg-12 col-md-12 col-sm-12'><div style='text-align:center;'>" + aspxPage_G.String.PRODUCTSPERPACK + "</div>";
                        var objChildItems = defaults.selectedProducts[i].ChildItems;
                        for (var j = 0; j < tam; j++) {
                            var inputColor = objChildItems[j].GroupName == "" ? "" : " background-color: " + objChildItems[j].GroupName + "; ";
                            var divinput = objChildItems[j].CustomSettings == "1" ? "<input  data-qty='" + objChildItems[j].QuantityChild + "'  data-group='" + objChildItems[j].GroupName + "'  type='text' style='width: 36px;display: inline;" + inputColor + "' class='form-control input-sm quantityvalue' value='" + objChildItems[j].QuantitySelected + "' maxlength='2' onkeyup='calculateTotalChilds(event, this)' onkeydown='validateNumbers(event, this)' />" : "<span  data-qty='" + objChildItems[j].QuantityChild + "'  data-group='" + objChildItems[j].GroupName + "'  style='padding-left: 11px;padding-right: 14px;' class='quantityvalue'>" + objChildItems[j].QuantitySelected + "</span>";
                            //var divinput = objChildItems[j].CustomSettings == "1" ? "<input type='text' style='width: 36px;display: inline;' class='form-control input-sm quantityvalue' value='" + objChildItems[j].QuantitySelected + "' maxlength='2' onkeyup='calculateTotalChilds(event, this)' onkeydown='validateNumbers(event, this)' />" : "<span style='padding-left: 11px;padding-right: 14px;' class='quantityvalue'>" + objChildItems[j].QuantitySelected + "</span>";
                            tabla += '<div class="col-lg-12" align="left"  style="padding-left: 0; padding-right: 0;" >' +
                                        divinput + '<span style="padding-left: 10px; font-size: 11px;" id="' + objChildItems[j].IdChild + '" >' + objChildItems[j].ProductNameChild + '</span>' +
                                    '</div>';
                        }
                        tabla += "</div>"
                        var div = $("#div" + defaults.selectedProducts[i].Id + "_" + defaults.selectedProducts[i].AttributeId);
                        $(div).attr("ti", defaults.selectedProducts[i].TotalItems);
                        $(div).html(tabla);
                        $(div).css("padding", "5px 15px");
                        $(div).children().append("<div style='text-align:center;' class='div_error'></div>");
                        $(div).children().css({ "border": "1px solid #ddd", "padding": "15px 12px" });
                    }
                }

            } else {

                for (var i in defaults.selectedProducts) {
                    $("#txt" + defaults.selectedProducts[i].Id).val(defaults.selectedProducts[i].Quantity);
                }
            }
        },
        _loadDistributors: function (lst) {

            var items = "";
            var len = lst.length;

            for (var i = 0; i < len; i++) {
                var accounttype = "";
                if (lst[i].AccountType == "10") accounttype = aspxPage_G.String.PROMOTER;
                else if (lst[i].AccountType == "20") accounttype = aspxPage_G.String.PREFERREDCUSTOMER;
                else if (lst[i].AccountType == "30") accounttype = aspxPage_G.String.RETAIL;
                else if (lst[i].AccountType == "40") accounttype = aspxPage_G.String.RETAILCUSTOMER;

                items += '<tr>'
                          + '<td>' + (i + 1) + '</td>'
                          + '<td>' + lst[i].FullName + '</td>'
                          + '<td>' + lst[i].Name + '</td>'
                          + '<td>' + lst[i].LastName + '</td>'
                          + '<td>' + lst[i].LegacyNumber + '</td>'
                          + '<td>' + accounttype + '</td>'
                          + '<td>' + "<a type='button' class='mb-xs mt-xs mr-xs btn btn-xs btn-default' title='Select' onclick='$.fn.aspxPage(\"promoterRowSelected\", \"" + lst[i].Url + "\");' ><span class='glyphicon glyphicon-arrow-right' aria-hidden='true'></span></a>" + '</td>'
                    + '</tr>';
            }
            $("#tbSearchDistGrid tbody").html(items);
            $("#tbSearchDistGrid").DataTable({
                "dom": '<"top"i>rt<"bottom"flp><"clear">',
                "bDestroy": true,
                "oLanguage": {
                    "sProcessing": aspxPage_G.String.PROCESSING,
                    "sLengthMenu": aspxPage_G.String.SHOWMENUENTRIES,
                    "sZeroRecords": aspxPage_G.String.NOMATCHINGRECORDSFOUND,
                    "sEmptyTable": aspxPage_G.String.NODATAAVAILABLEINTABLE,
                    "sInfo": aspxPage_G.String.SHOWSTARTTOENDOFTOTALENTRIES,
                    "sInfoEmpty": aspxPage_G.String.SHOWING0TO0OF0ENTRIES,
                    "sInfoFiltered": aspxPage_G.String.FILTEREDFROMMAXTOTALENTRIES,
                    "sInfoPostFix": "",
                    "sSearch": aspxPage_G.String.SEARCH,
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "pageLength": 5,
                    "sLoadingRecords": aspxPage_G.String.LOADING,
                    "oPaginate": {
                        "sFfirst": aspxPage_G.String.FIRST,
                        "sLast": aspxPage_G.String.LAST,
                        "sNext": aspxPage_G.String.NEXT,
                        "sPrevious": aspxPage_G.String.PREVIOUS
                    }
                }
            });

            $("#modalSearch").modal('show');
        },
        _promoterRowSelected: function (url) {

            if (url == undefined || url == null || url == "") { return; } //show message
            window.location = url;
        },

        _countTotal: function () {
            var success = function (response) {

                if (response.d != "") {

                    var cart = response.d;
                    var orderDetail = "";
                    var autoshipDetail = "";

                    if (cart.ItemsAutoship != null && cart.TotalAutoship != null && cart.TotalItems != null) {
                        $("span[id$=lblItems]").text(cart.TotalItems);
                    }

                    if (cart.AutoshipDetail != null) {
                        for (var i = 0; i < cart.AutoshipDetail.length; i++) {
                            autoshipDetail = autoshipDetail + cart.AutoshipDetail[i].description + ' (' + cart.AutoshipDetail[i].quantity + ')  -  ' +  cart.AutoshipDetail[i].total + '<br/>';
                        }
                        $('#totalCartAutoship').html(autoshipDetail);
                    }
                }
            }

            var error = function (xhr, ajaxOptions, thrownError) {
                fn_message('i', aspxPage_G.Messages.ERRORSENDING);
            };
            fn_callmethod(aspxPage_G.RWMDomain.replace("{0}", "GetTotalAndQuantity"), "", success, error);
        },
    };

    var aspxPageMethods = { //PUBLIC METHODS
        init: function (options) {
            defaults = $.extend({}, options)
            aspxPageFunctions._init();
        },
        debug: function (msg) {
            if (window.console && window.console.log) {
                window.console.log(msg);
            }
        },
        showByCategory: function (code) {
            aspxPageFunctions._showByCategory(code);
        },
        promoterRowSelected: function (url) {
            aspxPageFunctions._promoterRowSelected(url);
        }
    };

    $.fn.aspxPage = function (method) {
        if (aspxPageMethods[method]) { return aspxPageMethods[method].apply(this, Array.prototype.slice.call(arguments, 1)); }
        else if (typeof method === 'object' || !method) { return aspxPageMethods.init.apply(this, arguments); }
        else { $.error('Method ' + method + ' does not exist'); }
    };

})(jQuery, window, document);

function fn_showProductsChildsCustomize(id, rowC) {
    var success = function (response) {
        var obj = $.parseJSON(response.d);
        var rowChild = obj.ListChildren;
        var tam = rowChild.length;
        var tabla = "<div class='col-lg-12 col-md-12 col-sm-12'><div style='text-align:center;'>" + aspxPage_G.String.PRODUCTSPERPACK + "</div>";

        for (var i = 0; i < tam; i++) {
            var inputColor = rowChild[i].GroupName == "" ? "" : " background-color: " + rowChild[i].GroupName + "; ";
            var divinput = rowChild[i].CustomSettings == "1" ? "<input  data-qty='" + rowChild[i].QuantityChild + "'  data-group='" + rowChild[i].GroupName + "'  type='text' style='width: 36px;display: inline;" + inputColor + "' class='form-control input-sm quantityvalue' value='" + rowChild[i].QuantityChild + "' maxlength='2' onkeyup='calculateTotalChilds(event, this)' onkeydown='validateNumbers(event, this)' />" : "<span  data-qty='" + rowChild[i].QuantityChild + "'  data-group='" + rowChild[i].GroupName + "'  style='padding-left: 11px;padding-right: 14px;' class='quantityvalue'>" + rowChild[i].QuantityChild + "</span>";
            //var divinput = rowChild[i].CustomSettings == "1" ? "<input type='text' style='width: 36px;display: inline;' class='form-control input-sm quantityvalue' value='" + rowChild[i].QuantityChild + "' maxlength='2' onkeyup='calculateTotalChilds(event, this)' onkeydown='validateNumbers(event, this)' />" : "<span style='padding-left: 11px;padding-right: 14px;' class='quantityvalue'>" + rowChild[i].QuantityChild + "</span>";
            tabla += '<div class="col-lg-12" align="left" style="padding-left: 0;padding-right: 0;">' +
                        divinput + '<span style="padding-left: 10px; font-size: 11px;" id="' + rowChild[i].IdChild + '" >' + rowChild[i].ProductNameChild + '</span>' +
                    '</div>';
        }
        tabla += "</div>";
        $(rowC).parent().parent().next().attr("ti", obj.TotalItems);
        $(rowC).parent().parent().next().html(tabla);
        $(rowC).parent().parent().next().css("padding", "5px 15px");
        $(rowC).parent().parent().next().children().append("<div align='center' class='div_error'></div>");
        $(rowC).parent().parent().next().children().css({ "border": "1px solid #ddd", "padding": "15px 12px", "width": "80%" });
    };
    var error = function (xhr, ajaxOptions, thrownError) {
        fn_message('i', aspxPage_G.Messages.ERROR_SENDING);
    };
    fn_callmethod(aspxPage_G.RWMDomain.replace("{0}", "GetProductBySkuidAndMarketId"), '{ code:"' + id + '", o:"' + aspxPage_G.Properties.ssak + '", lng:"' + aspxPage_G.Properties.sslk + '"}', success, error);
}

function validateNumbers(e, row) {
    if (!validateInput(e)) {
        e.preventDefault();
    }
}

function calculateTotalChilds(e, row) {
    var tempselect = 0;
    listChildItems = [];
    tempChildItems = []; //new group child items 
    tempGroup = []; //new group child items 

    var inputs = $(row).parent().parent().children().children(".quantityvalue");
    inputs.each(function (index) {
        var tmp = $(this).val() != "" ? $(this).val() : $(this).text() != "" ? $(this).text() : 0;
        tempselect = tempselect + parseInt(tmp);
        listChildItems.push({ Id: parseInt($(this).next().attr("id")), Quantity: tmp });

        //new group child items                 
        var index = tempGroup.indexOf($(this).data("group"));
        if (index != -1) {
            tempChildItems[index].QtySel += parseInt(tmp);
            tempChildItems[index].Qty += parseInt($(this).data("qty"));
        } else {
            tempGroup.push($(this).data("group"));
            tempChildItems.push({ Group: $(this).data("group"), Qty: parseInt($(this).data("qty")), QtySel: parseInt(tmp) });
        }
    });
    //console.log(tempGroup); //new group child items 
    //console.log(tempChildItems); //new group child items  
    var tempLen = tempChildItems.length;
    var tempmsj = "";
    for (var i = 0; i < tempLen; i++) {
        if (tempChildItems[i].QtySel != tempChildItems[i].Qty && tempChildItems[i].Group != "") {
            tempmsj += '<div><i class="icon-stop" style="color: ' + tempChildItems[i].Group + ';"></i><span style="color: red; font-size: 11px;">' + aspxPage_G.String.PRODUCTSMUSTUPTO + ': ' + tempChildItems[i].Qty + '. </span></div>';
        }
    }
    //if (tempmsj != "") console.log("Error, incorrect quantity in child group"); //new group child items          
    //else console.log("Correct group quantities"); //new group child items   

    var totalitems = $(row).parent().parent().parent().attr("ti");
    if (totalitems == tempselect) {
        $(row).parent().parent().children("div:last").html("");
        $(row).parent().parent().children("div:last").append(tempmsj); //new group child items  
        var attributeId = $(row).closest("td").data("attribute") == undefined ? "0" : $(row).closest("td").data("attribute");
        var id = $(row).closest("td").data("id");
        if ($(".div_error").children().length == 0) {
            $("a[id$=btnContinue]").removeClass("hidden");
        }
        if ($("#txt" + id + "_" + attributeId).val() <= 0 || tempmsj != "") { return; } //new group child items  
        //if ($("#txt" + id + "_" + attributeId).val() <= 0 ) { return; }
        fn_childItemsUpdate(listChildItems, id, attributeId);
    }
    else {
        $(row).parent().parent().children("div:last").html("<div><span style='color: red; font-size: 11px;'>" + aspxPage_G.String.QUANTITIESMUSTUPTO + ": " + totalitems + "</span></div>" +
                                "<div><span style='color: red; font-size: 11px;'>" + aspxPage_G.String.QUANTITIESMUSTUPTO + ": " + tempselect + "</span></div>");
        $(row).parent().parent().children("div:last").append(tempmsj); //new group child items  
        if ($(".div_error").children().length != 0) {
            $("a[id$=btnContinue]").addClass("hidden");
        }
    }
}

function validateInput(event) {
    var code = (event.keyCode ? event.keyCode : event.which);
    if (code === 46 || code === 8 || code === 9 || code === 27 || code === 13 || (code === 65 && event.ctrlKey === true) || (code >= 35 && code <= 39)) {
        return true;
    } else {
        if (event.shiftKey || (code < 48 || code > 57) && (code < 96 || code > 105)) {
            return false;
        }
    }
    return true;
}

function fn_childItemsUpdate(listChildItems, id, attributeId) {
    var params = { o: aspxPage_G.Properties.ssak, List: listChildItems, Id: id, AttributeId: attributeId };
    var success = function (response) {
        if (response.d == true) {
            fn_message('s', aspxPage_G.Messages.SUCCESS_CHILDITEMSUPDATED);
        } else {
            fn_message('i', aspxPage_G.Messages.ERROR_WRRORUPDATING_CHILDITEMS);
        }
    };
    var error = function (xhr, ajaxOptions, thrownError) {
        fn_message('e', aspxPage_G.Messages.ERROR_SENDING);
    };
    fn_callmethod("Products.aspx/UpdateChildQuantities", JSON.stringify(params), success, error);
}