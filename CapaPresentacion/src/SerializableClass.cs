﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CapaPresentacion.src
{
    [Serializable]
    public class srCliente
    {
       
        public int CliId { get; set; }
        public int DirId { get; set; }
        public int PaisId { get; set; }
        public int DistId { get; set; }
        public int ProvId { get; set; }
        public int DepId { get; set; }
        public string CliNombre { get; set; }
        public string CliApellidoP { get; set; }
        public string CliApellidoM { get; set; }
        public string CliTelefono { get; set; }
        public string CliCelular { get; set; }
        public string CliEmail { get; set; }
        public string CliPassword { get; set; }
        public string CliFoto { get; set; }
        public DateTime CliFechaRegistro { get; set; }
        public string CliUsuarioRegistro { get; set; }
        public DateTime CliFechaModificacion { get; set; }
        public string CliUsuarioModificacion { get; set; }
        public int CliEstado { get; set; }

    }

    [Serializable]
    public class srDireccion
    {
        public int DirId { get; set; }
        public string DirDescripcion1 { get; set; }
        public string DirDescripcion2 { get; set; }
        public string DirCodPostal { get; set; }
        public int DirEstado { get; set; }
    }

    [Serializable]
    public class srRCliente
    {
        public int CliId { get; set; }
        public int DirId { get; set; }
        public int PaisId { get; set; }
        public int DistId { get; set; }
        public int ProvId { get; set; }
        public int DepId { get; set; }
        public string CliNombre { get; set; }
        public string CliApellidoP { get; set; }
        public string CliApellidoM { get; set; }
        public string CliTelefono { get; set; }
        public string CliCelular { get; set; }
        public string CliEmail { get; set; }
        public string CliPassword { get; set; }
        public string CliFoto { get; set; }
        public DateTime CliFechaRegistro { get; set; }
        public string CliUsuarioRegistro { get; set; }
        public DateTime CliFechaModificacion { get; set; }
        public string CliUsuarioModificacion { get; set; }
        public int CliEstado { get; set; }

        public string DirDescripcion1 { get; set; }
        public string DirDescripcion2 { get; set; }
        public string DirCodPostal { get; set; }
        public int DirEstado { get; set; }
    }
    [Serializable]
    public class srPersona
    {
        public String PerId { get; set; }
        public int CliId { get; set; }
        public int DirId { get; set; }
        public int PaisId { get; set; }
        public int DistId { get; set; }
        public int ProvId { get; set; }
        public int DepId { get; set; }
        public int TipoDocId { get; set; }
        public string Nombre { get; set; }
        public string NumDoc { get; set; }
        public string ApellidoP { get; set; }
        public string ApellidoM { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string Sexo { get; set; }
        public string Edad { get; set; }
        public string Email { get; set; }
        public string EstadoP { get; set; }
        public string Foto { get; set; }

        public string DirDescripcion1 { get; set; }
        public string DirDescripcion2 { get; set; }
        public string DirCodPostal { get; set; }
        public int DirEstado { get; set; }
    }
}