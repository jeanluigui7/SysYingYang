﻿
function fn_filetype(ex) {
    $('input[type="file"]').MultiFile({
        accept: ex,
        max: 1,
        STRING: {
            remove: 'Remover',
            selected: 'Selecionado: $file',
            denied: 'Invalid file type $ext!',
            duplicate: 'The file is selected:\n$file!'
        }
    });
}
$(function () {
    //fn_filetype('"doc|docx|xls|xlsx|ppt|pdf|pptx"');
    if ($(".Select_ResourceType option:selected")) {
        var x = $(".Select_ResourceType option:selected").text();
        switch (x) {
            case "Document": fn_filetype('"doc|docx|xls|xlsx|ppt|pdf|pptx"');
        }
    }

    $('.Select_ResourceType').change(function () {
        $(".Select_ResourceType option:selected").each(function () {
            var x = $(this).text();
            var ex ="";
            
            switch (x) {
                case "Video":
                    ex = '"avi|mp4|wmv|m4v"';
                    break;
                case "Document":
                    ex = '"doc|docx|xls|xlsx|ppt|pdf|pptx"';
                    break;
                case "Audio":
                    ex = '"mp3|wav|3gp|wmv"';
                    break;
                case "Image":
                    ex = "'jpeg|jpg|gif|tif|png'";
                    break;
                default:
                    ex = '"doc|docx|xls|xlsx|ppt|pdf|pptx|mp3|wav|3gp|wmv|avi|mp4|wmv|m4v|gif|jpeg|jpg|tif|png|i"';
            }
          
            fn_filetype(ex);
        });
    });
});


