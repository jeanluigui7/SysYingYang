
(function ($) {
    $.fn.validationEngineLanguage = function () { };
    $.validationEngineLanguage = {
        newLang: function () {
            $.validationEngineLanguage.allRules = {
                "required": {    			// Add your regex rules here, you can take telephone as an example
                    "regex": "none",
                    "alertText": "* This field is required",
                    "alertTextCheckboxMultiple": "* Please select an option",
                    "alertTextCheckboxe": "* This checkbox is required"
                },
                "chkrequired": {    			// Add your regex rules here, you can take telephone as an example
                    "regex": "none",
                    "alertText": "* This field is required",
                    "alertTextCheckboxMultiple": "* Please select an option",
                    "alertTextCheckboxe": "* This checkbox is required"
                },
                "length": {
                    "regex": "none",
                    "alertText": "*Between ",
                    "alertText2": " and ",
                    "alertText3": " characters allowed"
                },
                "maxCheckbox": {
                    "regex": "none",
                    "alertText": "* Checks allowed Exceeded"
                },
                "minCheckbox": {
                    "regex": "none",
                    "alertText": "* Please select ",
                    "alertText2": " options"
                },
                "equals": {
                    "regex": "none",
                    "alertText": "* Fields do not match"
                },
                "dontmatch": {
                    "regex": "none",
                    "alertText": "* Please select a market"
                },
                "samevalues": {
                    "regex": "none",
                    "alertText": "* Password must be equals"
                },
                "between": {
                    "regex": "none",
                    "alertText": "* Please select a valid value"
                },
                "phone": {
                    // credit: jquery.h5validate.js / orefalo
                    "regex": /^([\+][0-9]{1,3}[ \.\-])?([\(]{1}[0-9]{2,6}[\)])?([0-9 \.\-\/]{3,20})((x|ext|extension)[ ]?[0-9]{1,4})?$/,
                    "alertText": "* Invalid phone number"
                },
                "fax": {
                    // credit: jquery.h5validate.js / orefalo
                    "regex": /[\+? *[1-9]+]?[0-9 ]+/,
                    "alertText": "* Invalid fax number"
                },
                "email": {
                    // Shamelessly lifted from Scott Gonzalez via the Bassistance Validation plugin http://projects.scottsplayground.com/email_address_validation/
                    "regex": /^((([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/,
                    "alertText": "* Invalid email address"
                },
                "integer": {
                    "regex": /^[\-\+]?\d+$/,
                    "alertText": "* Not a valid integer"
                },
                "number": {
                    // Number, including positive, negative, and floating decimal. Credit: bassistance
                    "regex": /^[\-\+]?(?:\d+|\d{1,3})(?:\.\d+)$/,
                    //^[\-\+]?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)$
                    "alertText": "* Invalid floating decimal number"
                },
                "date": {
                    // Date in ISO format. Credit: bassistance
                    "regex": /^\d{1,2}[\/\-]\d{1,2}[\/\-]\d{4}$/,
                    "alertText": "* Invalid date, must be in MM/DD/YYYY format"
                },

                "ipv4": {
                    "regex": /^([1-9][0-9]{0,2})+\.([1-9][0-9]{0,2})+\.([1-9][0-9]{0,2})+\.([1-9][0-9]{0,2})+$/,
                    "alertText": "* Invalid IP address"
                },
                "creditnumber": {
                    "regex": /^([0-9][0-9]{0,4})+([0-9][0-9]{0,4})+([0-9][0-9]{0,4})+([0-9][0-9]{0,4})+$/,
                    "alertText": "* Invalid credit card number.Numbers only"
                },
                "url": {
                    "regex": /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/,
                    "alertText": "* Invalid URL"
                },
                "datehour": {
                    // Date in ISO format. Credit: bassistance $ =final
                    "regex": /^\d{1,2}[\/\-]\d{1,2}[\/\-]\d{4}\s([0-9][0-9]{0,2}):([0-9][0-9]{0,2})$/,
                    "alertText": "*Must be in MM-DD-YYYY HH:mm format"
                },
                "max100": {
                    // Date in ISO format. Credit: bassistance $ =final
                    "regex": /^([0-9][0-9]{0,100})$/,
                    "alertText": "* Maximum 100 characters allowed"
                },
                /*"maxsize": {
                    "regex": /^([a-zA-Z]{})$/,
                },*/
                /*
                12/19/2012 00:00
                */
                "onlyNumber": {
                    "regex": /^[0-9\ ]+$/,
                    "alertText": "* Numbers only"
                },
                "noSpecialCaracters": {
                    "regex": /^[0-9a-zA-Z']+$/,
                    "alertText": "* No special caracters allowed"
                },
                "noSpecialCaractersSpace": {
                    "regex": /^[0-9a-zA-Z' ]+$/,
                    "alertText": "* No special caracters allowed"
                },
                "ajaxUser": {
                    "file": "validateUser.php",
                    "extraData": "name=eric",
                    "alertTextOk": "* This user is available",
                    "alertTextLoad": "* Loading, please wait",
                    "alertText": "* This user is already taken"
                },
                "ajaxName": {
                    "file": "validateUser.php",
                    "alertText": "* This name is already taken",
                    "alertTextOk": "* This name is available",
                    "alertTextLoad": "* Loading, please wait"
                },
                "onlyLetter": {
                    "regex": /^[a-zA-Z\ \']+$/,
                    "alertText": "* Letters only"
                },
                "onlyLetterNumberandothers": {
                    "regex": /^[0-9a-zA-Z\ \'\-\U+00D1\U+00F1\*\u00C0-\u017F\ ']+$/,
                    "alertText": "* No special characters allowed"
                },
                "validate2fields": {
                    "nname": "validate2fields",
                    "alertText": "* You must have a firstname and a lastname"
                },
                "min": {
                    "regex": "none",
                    "alertText": "* Minimum value is "
                },
                "max": {
                    "regex": "none",
                    "alertText": "* Maximum value is "
                },
                "expirationdate": {
                    "regex": "none",
                    "alertText": "* Invalid date,"
                }
            };

        }
    };
})(jQuery);

$(document).ready(function () {
    $.validationEngineLanguage.newLang();
});