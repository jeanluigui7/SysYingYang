﻿


(function (e) {

    e.fn.AddBackup = function (IdLevel, dig) {
        switch (IdLevel) {
            case 1: $('#sti_nodes_lvl_1_backup span').html(dig); break;
            case 2: $('#sti_slide_lvl_2_backup span').html(dig); break;
            case 3: $('#sti_slide_lvl_3_backup span').html(dig); break;
        }
    }

    e.fn.AddCancelled = function (IdLevel, dig) {
        switch (IdLevel) {
            case 1: $('#sti_nodes_lvl_1_Cancelled span').html(dig); break;
            case 2: $('#sti_nodes_lvl_2_Cancelled span').html(dig); break;
            case 3: $('#sti_nodes_lvl_3_Cancelled span').html(dig); break;
        }
    }

    e.fn.ClearItems = function (IdLevel) {
        switch (IdLevel) {
            case 1: $('#sti_slide_lvl1 li').remove(); $('#sti_slide_lvl1').css('left', '0px'); $('#sti_nodes_lvl_1_backup span').html('0'); break;
            case 2: $('#sti_slide_lvl2 li').remove(); $('#sti_slide_lvl2').css('left', '0px'); $('#sti_nodes_lvl_2_backup span').html('0'); break;
            case 3: $('#sti_slide_lvl3 li').remove(); $('#sti_slide_lvl3').css('left', '0px'); $('#sti_nodes_lvl_3_backup span').html('0'); break;
        }
    }

    e.fn.ShowBox = function (obj) {

        e('body').append('<div id="sti_rs_lightbox" class="sti_prod_lightbox ' + obj.Status + '"></div>');
        var box = e('#sti_rs_lightbox');
        box.html('');

        var Trends = "";
        for (var i = 0; i < obj.Trends.items.length; i++) {
            Trends += '<tr><td>' + obj.Trends.items[i].Month.toString() + '</td><td>' + obj.Trends.items[i].L1.toString() + '</td><td>' + obj.Trends.items[i].L2.toString() + '</td><td>' + obj.Trends.items[i].L3.toString() + '</td></tr>'
        }


        var History = "";
        for (var i = 0; i < obj.History.items.length; i++) {
            //History += '<tr><td>' + obj.History.items[i].col1.toString() + '</td><td>' + obj.History.items[i].col2.toString() + '</td><td><a href="' + obj.History.items[i].col3.toString() + '">Click</a> to View</td></tr>'
            History += '<tr><td>' + obj.History.items[i].col1.toString() + '</td><td>' + obj.History.items[i].col2.toString() + '</td><td><a href="#">Click</a> to View</td></tr>'
        }


        var Activity = "";
        for (var i = 0; i < obj.Activity.items.length; i++) {
            Activity += '<li class="' + obj.Activity.items[i].state.toString() + '">' + obj.Activity.items[i].cod.toString() + '</li>'
        }

        var lvl = "L1";
        if (obj.Level != undefined)
            lvl = obj.Level;

        var dataPage =
           '<ul class="sti_prod_lightbox_title">' +
           '   <li class="lvl">' + level + ' ' + lvl + '</li>' +
           '   <li>' + upline + ': <span class="childName">' + obj.Name + '</span> > <span class="parentName">' + obj.ParentName + '</span></li>' +
           '</ul>' +
           '<ul class="sti_prod_lightbox_profile">' +
           '    <li>' +
           '        <span class="label">' + status + ':</span>' +
           '        <span class="text">' + obj.Status + '</span>' +
           '    </li>' +
           '    <li>' +
           '        <span class="label">' + name + ':</span>' +
           '        <span class="text">' + obj.Names + '</span>' +
           '    </li>';
        if (obj.Phone != "-") {
            var dataPage = dataPage +
            '    <li>' +
            '        <span class="label">' + email + ':</span>' +
            '        <span class="text">' + obj.Email + '</span>' +
            '    </li>' +
            '    <li>' +
            '        <span class="label">' + phone + ':</span>' +
            '        <span class="text">' + obj.Phone + '</span>' +
            '    </li>';
        }
        var dataPage = dataPage +
        '    <li>' +
        '        <span class="label">' + associateid + ':</span>' +
        '       <span class="text">' + obj.AssociateID + '</span>' +
        '    </li' +
        '    <li>' +
        '        <span class="label">' + country + ':</span>' +
        '        <span class="text">' + obj.Country + '</span' +
        '    </li>' +
        '</ul>' +
        '<table class="sti_prod_lightbox_table">' +
        '      <thead>' +
        '          <tr>' +
        '             <td colspan="4">' + autoshiptrends + ':</td>' +
        '          </tr>' +
        '              <tr>' +
        '                  <td>' + month + '</td>' +
        '              <td>' + l1 + '</td>' +
        '              <td>' + l2 + '</td>' +
        '              <td>' + l3 + '</td>' +
        '          </tr>' +
        '      </thead>' +
        '      <tbody>' + Trends +

        '      </tbody>' +
        '  </table>' +
        '   <table class="sti_prod_lightbox_table2"  style="display:none;">' +
        '      <thead>' +
        '          <tr>' +
        '              <td colspan="3">AutoShip History: 3 Month View</td>' +
        '          </tr>' +
        '      </thead>' +
        '      <tbody>' + History +
        '      </tbody>' +
        '  </table>' +

        '<div class="sti_prod_lightbox_MonthActivity">' +
        '  <h5>' + monthamonth + '</h5>' +
        '  <ul class="qualification2"> ' + Activity + '</ul>' +
        '</div>' +
        '<a id="A1" class="close close_btn_lightbox" href="#">close</a>';

        e('#sti_rs_lightbox').append(dataPage);
        return box;
    };

    e.fn.addNode = function (levelID, obj) {
        $this = e(this);

        var $Q = "";
        var $data = "";

        for (var i = 0; i < obj.qualification.length; i += 1) {
            $Q += '<li class="' + obj.qualification[i].toString() + '"></li>';
        }

        var iconData = "";
        var iconState = "";
        var iconDatafull = "";
        var iconStatefull = "";

        //iconData = '<span class="nro-cardinal_44">' + obj.date + '</span>';
        //<a href="#" class="link44 content sti_lightbox" ><span class="nro-cardinal_44">11</span></a>
        //<%--<a href="#" class="link content sti_lightbox" data-type="div-content" data-size="550x450" ><span class="nro-ordinal">28<small>th</small></span></a>--%>
        switch (levelID) {
            case 0:
                if (obj.state == "completed")
                    iconState = "icon-completed_86";
                if (obj.state == "scheduled") {
                    iconState = "nro-ordinal_86";
                    iconData = obj.date;
                }
                if (obj.state == "failed")
                    iconState = "icon-failed_86";
                if (obj.state == "cancelled")
                    iconState = "icon-cancelled_86";
                if (obj.state == "nonautoship")
                    iconState = "icon-nonautoship_86";


                levelID = "#sti_slide_lvl0";
                $data = '<li style="width: 91px;"><div class="sti_prod ' + obj.state + '" data-effects="false"><span class="sti_prod-title">' + obj.title + '</span>' +
                 '<a href="javascript:void(0)"  rel="' + obj.id + '" class="link86 content sti_lightbox" ><span class="' + iconState + '">' + iconData + '</span></a>' +
                 '<span class="sti_prod-date">' + asdate + ':' + obj.date + '</span>' +
                 '<ul class="qualification">' + $Q + '</ul></div></li>';
                break;
            case 1: case 2: case 3:
                //$(".sti_lightbox").click(function (x) { rs_lightbox(x, $(this)); });
                if (obj.state == "completed")
                    iconState = "icon-completed_86";
                if (obj.state == "scheduled") {
                    iconState = "nro-ordinal_86";
                    iconData = obj.date;
                }
                if (obj.state == "failed")
                    iconState = "icon-failed_86";
                if (obj.state == "cancelled")
                    iconState = "icon-cancelled_86";
                if (obj.state == "nonautoship")
                    iconState = "icon-nonautoship_86";
                if (levelID == 1)
                    levelID = "#sti_slide_lvl1";
                if (levelID == 2)
                    levelID = "#sti_slide_lvl2";
                if (levelID == 3)
                    levelID = "#sti_slide_lvl3";

                $data = '<li style="width: 91px;"><div class="sti_prod ' + obj.state + '" data-effects="false"><span class="sti_prod-title">' + obj.title + '</span>' +
                 '<a href="javascript:void(0)"  rel="' + obj.id + '" class="link86 content sti_lightbox" ><span class="' + iconState + '">' + iconData + '</span></a>' +
                 '<span class="ribbon ribbon_circle" title="2">' + obj.nro + '</span>' +
                 '<span class="sti_prod-date">' + asdate + ':' + obj.date + '</span>' +
                 '<ul class="qualification">' + $Q + '</ul></div></li>';

                break;
        }


        e(levelID).append($data);

    };

    e.fn.addNodeEmpty = function (levelID, count) {
        $this = e(this);
        var $Q = "";
        var $data = "";
        switch (levelID) {

            case 0:
                levelID = "#sti_slide_lvl0";
                $data = '<li><div class="sti_prod empty" data-effects="false"><span class="sti_prod-title">EMPTY</span>' +
               '<a href="javascript:void(0)" class="link86 " >' +
               '<span class="icon-completed_86"></span></a><span class="sti_prod-date"></span><ul class="qualification"></ul></div></li>';
                e(levelID).append($data);
                break;

            case 1: case 2: case 3:
                if (levelID == 1) {
                    levelID = "#sti_slide_lvl1";
                    $(".sti_nodes_lvl11").find("#showbiz_right_11").removeClass("notclickable");
                }
                if (levelID == 2) {
                    levelID = "#sti_slide_lvl2";
                    $(".sti_nodes_lvl2").find("#showbiz_right_2").removeClass("notclickable");

                }
                if (levelID == 3) {
                    levelID = "#sti_slide_lvl3";
                    $(".sti_nodes_lvl3").find("#showbiz_right_3").removeClass("notclickable");
                }
                for (var i = 0; i < count; i++) {
                    $data = '<li style="width: 91px;"><div class="sti_prod empty" data-effects="false"><span class="sti_prod-title">EMPTY</span>' +
                   '<a href="javascript:void(0)" class="link86 " >' +
                   '<span class="icon-completed_86"></span></a><span class="sti_prod-date"></span><ul class="qualification"></ul></div></li>';
                    e(levelID).append($data);
                }
                break;
        }
    };


})(jQuery);


(function ($) {

    /*
    * $ lightbox_me
    */
    $.fn.lightbox_d = function PageLock() {
        $('.sti_lightbox').unbind('click').off("click", ".sti_lightbox");
    };

    $.fn.lightbox_me = function (options) {
        return this.each(function () {
            var
                opts = $.extend({}, $.fn.lightbox_me.defaults, options),
                $overlay = $(),
                $self = $(this),
                //$iframe = $('<iframe id="foo" style="z-index: ' + (opts.zIndex + 1) + ';border: none; margin: 0; padding: 0; position: absolute; width: 100%; height: 100%; top: 0; left: 0; filter: mask();"/>'),
                ie6 = ($.browser.msie && $.browser.version < 7);

            if (opts.showOverlay) {

                //check if there's an existing overlay, if so, make subequent ones clear
                var $currentOverlays = $(".js_lb_overlay:visible");
                if ($currentOverlays.length > 0) {
                    $overlay = $('<div class="lb_overlay_clear js_lb_overlay"/>');
                } else {
                    $overlay = $('<div class="' + opts.classPrefix + '_overlay js_lb_overlay"/>');
                }
            }

            /*----------------------------------------------------
               DOM Building
            ---------------------------------------------------- */
            if (ie6) {
                var src = /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank';
                //$iframe.attr('src', src);
                //$('body').append($iframe);
            } // iframe shim for ie6, to hide select elements
            $('body').append($self.hide()).append($overlay);


            /*----------------------------------------------------
               Overlay CSS stuffs
            ---------------------------------------------------- */

            // set css of the overlay
            if (opts.showOverlay) {
                setOverlayHeight(); // pulled this into a function because it is called on window resize.
                $overlay.css({ position: 'absolute', width: '100%', top: 0, left: 0, right: 0, bottom: 0, zIndex: (opts.zIndex + 2), display: 'none' });
                if (!$overlay.hasClass('lb_overlay_clear')) {
                    $overlay.css(opts.overlayCSS);
                }
            }

            /*----------------------------------------------------
               Animate it in.
            ---------------------------------------------------- */
            //
            if (opts.showOverlay) {
                $overlay.fadeIn(opts.overlaySpeed, function () {
                    setSelfPosition();
                    $self[opts.appearEffect](opts.lightboxSpeed, function () { setOverlayHeight(); setSelfPosition(); opts.onLoad() });
                });
            } else {
                setSelfPosition();
                $self[opts.appearEffect](opts.lightboxSpeed, function () { opts.onLoad() });
            }

            /*----------------------------------------------------
               Hide parent if parent specified (parentLightbox should be jquery reference to any parent lightbox)
            ---------------------------------------------------- */
            if (opts.parentLightbox) {
                opts.parentLightbox.fadeOut(200);
            }


            /*----------------------------------------------------
               Bind Events
            ---------------------------------------------------- */

            $(window).resize(setOverlayHeight)
                     .resize(setSelfPosition)
                     .scroll(setSelfPosition);

            $(window).bind('keyup.lightbox_me', observeKeyPress);

            if (opts.closeClick) {
                $overlay.click(function (e) { closeLightbox(); e.preventDefault; });

            }

            $self.delegate(opts.closeSelector, "click", function (e) { closeLightbox(); e.preventDefault(); });

            $self.bind('close', closeLightbox);
            $self.bind('reposition', setSelfPosition);



            /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
              -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */


            /*----------------------------------------------------
               Private Functions
            ---------------------------------------------------- */

            /* Remove or hide all elements */
            function closeLightbox() {
                var s = $self[0].style;
                if (opts.destroyOnClose) {
                    $self.add($overlay).remove();
                } else {
                    $self.add($overlay).hide();
                }

                //show the hidden parent lightbox
                if (opts.parentLightbox) {
                    opts.parentLightbox.fadeIn(200);
                }

                //$iframe.remove();

                // clean up events.
                $self.undelegate(opts.closeSelector, "click");

                $(window).unbind('reposition', setOverlayHeight);
                $(window).unbind('reposition', setSelfPosition);
                $(window).unbind('scroll', setSelfPosition);
                $(window).unbind('keyup.lightbox_me');
                if (ie6)
                    s.removeExpression('top');

                $('.sti_prod_lightbox').remove();
                $('.js_lb_overlay').remove();
                opts.onClose();
            }


            /* Function to bind to the window to observe the escape/enter key press */
            function observeKeyPress(e) {
                if ((e.keyCode == 27 || (e.DOM_VK_ESCAPE == 27 && e.which == 0)) && opts.closeEsc) closeLightbox();
            }


            /* Set the height of the overlay
                    : if the document height is taller than the window, then set the overlay height to the document height.
                    : otherwise, just set overlay height: 100%
            */
            function setOverlayHeight() {
                if ($(window).height() < $(document).height()) {
                    $overlay.css({ height: $(document).height() + 'px' });
                    //$iframe.css({ height: $(document).height() + 'px' });
                } else {
                    $overlay.css({ height: '100%' });
                    if (ie6) {
                        $('html,body').css('height', '100%');
                        //$iframe.css('height', '100%');
                    } // ie6 hack for height: 100%; TODO: handle this in IE7
                }
            }


            /* Set the position of the modal'd window ($self)
                    : if $self is taller than the window, then make it absolutely positioned
                    : otherwise fixed
            */
            function setSelfPosition() {
                var s = $self[0].style;

                // reset CSS so width is re-calculated for margin-left CSS
                $self.css({ left: '50%', marginLeft: ($self.outerWidth() / 2) * -1, zIndex: (opts.zIndex + 3) });


                /* we have to get a little fancy when dealing with height, because lightbox_me
                    is just so fancy.
                 */

                // if the height of $self is bigger than the window and self isn't already position absolute
                if (($self.height() + 80 >= $(window).height()) && ($self.css('position') != 'absolute' || ie6)) {

                    // we are going to make it positioned where the user can see it, but they can still scroll
                    // so the top offset is based on the user's scroll position.
                    var topOffset = $(document).scrollTop() + 40;
                    $self.css({ position: 'absolute', top: topOffset + 'px', marginTop: 0 })
                    if (ie6) {
                        s.removeExpression('top');
                    }
                } else if ($self.height() + 80 < $(window).height()) {
                    //if the height is less than the window height, then we're gonna make this thing position: fixed.
                    // in ie6 we're gonna fake it.
                    if (ie6) {
                        s.position = 'absolute';
                        if (opts.centered) {
                            s.setExpression('top', '(document.documentElement.clientHeight || document.body.clientHeight) / 2 - (this.offsetHeight / 2) + (blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"')
                            s.marginTop = 0;
                        } else {
                            var top = (opts.modalCSS && opts.modalCSS.top) ? parseInt(opts.modalCSS.top) : 0;
                            s.setExpression('top', '((blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + ' + top + ') + "px"')
                        }
                    } else {
                        if (opts.centered) {
                            $self.css({ position: 'fixed', top: '50%', marginTop: ($self.outerHeight() / 2) * -1 })
                        } else {
                            $self.css({ position: 'fixed' }).css(opts.modalCSS);
                        }

                    }
                }
            }

        });



    };

    $.fn.lightbox_me.defaults = {

        // animation
        appearEffect: "fadeIn",
        appearEase: "",
        overlaySpeed: 250,
        lightboxSpeed: 300,

        // close
        closeSelector: ".close",
        closeClick: true,
        closeEsc: true,

        // behavior
        destroyOnClose: false,
        showOverlay: true,
        parentLightbox: false,

        // callbacks
        onLoad: function () { },
        onClose: function () { },

        // style
        classPrefix: 'lb',
        zIndex: 999,
        centered: false,
        modalCSS: { top: '40px' },
        overlayCSS: { background: 'black', opacity: .3 }
    }
})(jQuery);

function rs_lightbox(x, me) {
    LoadShow();
    var idSearch = me.attr("rel");
    var success = function (r) {
        var obj = SetObjectDetail(idSearch, r);
        LoadHide();
        if (obj.Name != null) {
            $.fn.ShowBox(obj).lightbox_me({ centered: true });
            x.preventDefault();
        }
    };
    fn_loadDetail(idSearch, success, "");
}

function ShowNext(IdLevel) {
    $.fn.lightbox_d();
    switch (IdLevel) {
        case 'sti_nodes_lvl_1':
            node = NextParent(Nodelevel1_1, i1);
            if (node != undefined) {
                Nodelevel2_1_1 = "";
                i2_1 = 1; i2_2 = 1; i3_1 = 1; i3_2 = 1; i4_1 = 1; i4_2 = 1;
                icancelled2_1 = 0; icancelled3_1 = 0;
                $.fn.ClearItems(2);
                $.fn.ClearItems(3);
                Nodelevel1_1 = node.id;
                SetChildren(Nodelevel1_1, "2", 2);
                $.fn.addNodeEmpty(2, calculeEmpty_l2(i2_1));
                $.fn.AddBackup(2, calculeBackup(i2_1));

                $.fn.addNodeEmpty(3, calculeEmpty_l3(i3_1));
                $.fn.AddBackup(3, calculeBackup(i3_1));
                //$.fn.AddCancelled(2, icancelled2_1);
                //$.fn.AddCancelled(3, icancelled3_1);
                $('#level_2').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
                $('#level_3').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
            }
            break;
        case 'sti_nodes_lvl_2':
            node = NextParent(Nodelevel2_1_1, i2_1);
            if (node != undefined) {
                i3_1 = 1; i4_1 = 1;
                icancelled3_1 = 0;
                $.fn.ClearItems(3);

                Nodelevel2_1_1 = node.id;
                SetChildren(Nodelevel2_1_1, "3", 3);
                //$.fn.AddCancelled(3, icancelled3_1);
                $.fn.addNodeEmpty(3, calculeEmpty_l3(i3_1));
                $.fn.AddBackup(3, calculeBackup(i3_1));
                $('#level_3').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
            }
            break;

    }
    $(".sti_lightbox").click(function (x) { rs_lightbox(x, $(this)); });
    fn_validatesize();
}

function ShowPrev(IdLevel) {
    switch (IdLevel) {

        case 'sti_nodes_lvl_1':
            node = PrevParent(Nodelevel1_1, i1);
            if (node != undefined) {
                Nodelevel2_1_1 = "";
                i2_1 = 1; i2_2 = 1; i3_1 = 1; i3_2 = 1; i4_1 = 1; i4_2 = 1;
                icancelled2_1 = 0; icancelled3_1 = 0;
                $.fn.ClearItems(2);
                $.fn.ClearItems(3);
                Nodelevel1_1 = node.id;
                SetChildren(Nodelevel1_1, "2", 2);
                $.fn.addNodeEmpty(2, calculeEmpty_l2(i2_1));
                $.fn.AddBackup(2, calculeBackup(i2_1));

                $.fn.addNodeEmpty(3, calculeEmpty_l3(i3_1));
                $.fn.AddBackup(3, calculeBackup(i3_1));
                //$.fn.AddCancelled(2, icancelled2_1);
                //$.fn.AddCancelled(3, icancelled3_1);
                $('#level_2').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
                $('#level_3').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
            }
            break;
        case 'sti_nodes_lvl_2':
            node = PrevParent(Nodelevel2_1_1, i2_1);
            if (node != undefined) {
                i3_1 = 1; i4_1 = 1;
                icancelled3_1 = 0;
                $.fn.ClearItems(3);
                Nodelevel2_1_1 = node.id;
                SetChildren(Nodelevel2_1_1, "3", 3);
                $.fn.addNodeEmpty(3, calculeEmpty_l3(i3_1));
                $.fn.AddBackup(3, calculeBackup(i3_1));
                //$.fn.AddCancelled(3, icancelled3_1);
                $('#level_3').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
            }
            break;
    }
    fn_validatesize();
}

function loadPlug() {

    $('#level_1').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
    $('#level_2').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
    $('#level_3').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });

}

