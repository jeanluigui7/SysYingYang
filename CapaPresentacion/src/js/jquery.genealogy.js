

(function (e) {
    
    e.fn.AddBackup = function (IdLevel, dig) {
        switch (IdLevel) {
            case 1: $('#sti_slide_lvl1_backup span').html(dig); break;
            case 21: $('#sti_slide_lvl2-1_backup span').html(dig); break;
            case 22: $('#sti_slide_lvl2-2_backup span').html(dig); break;
            case 31: $('#sti_slide_lvl3-1_backup span').html(dig); break;
            case 32: $('#sti_slide_lvl3-2_backup span').html(dig); break;
            case 41: $('#sti_slide_lvl4-1_backup span').html(dig); break;
            case 42: $('#sti_slide_lvl4-2_backup span').html(dig); break;
        }
    }

    e.fn.AddCancelled = function (IdLevel, dig) {
        switch (IdLevel) {
            case 1: $('#sti_slide_lvl1_Cancelled span').html(dig); break;
            case 21: $('#sti_slide_lvl2-1_Cancelled span').html(dig); break;
            case 22: $('#sti_slide_lvl2-2_Cancelled span').html(dig); break;
            case 31: $('#sti_slide_lvl3-1_Cancelled span').html(dig); break;
            case 32: $('#sti_slide_lvl3-2_Cancelled span').html(dig); break;
            case 41: $('#sti_slide_lvl4-1_Cancelled span').html(dig); break;
            case 42: $('#sti_slide_lvl4-2_Cancelled span').html(dig); break;
        }
    }

    e.fn.ClearItems = function (IdLevel) {
        switch (IdLevel) {
            case 1: $('#sti_slide_lvl1 li').remove(); $('#sti_slide_lvl1_backup span').html('0'); break;
            case 21: $('#sti_slide_lvl2_1 li').remove(); $('#sti_slide_lvl2-1_backup span').html('0'); break;
            case 22: $('#sti_slide_lvl2_2 li').remove(); $('#sti_slide_lvl2-2_backup span').html('0'); break;
            case 31: $('#sti_slide_lvl3_1 li').remove(); $('#sti_slide_lvl3-1_backup span').html('0'); break;
            case 32: $('#sti_slide_lvl3_2 li').remove(); $('#sti_slide_lvl3-2_backup span').html('0'); break;
            case 41: $('#sti_slide_lvl4_1 li').remove(); $('#sti_slide_lvl4-1_backup span').html('0'); break;
            case 42: $('#sti_slide_lvl4_2 li').remove(); $('#sti_slide_lvl4-2_backup span').html('0'); break;
        }
    }

    e.fn.ShowBox = function (obj) {
         var idgens = fn_getURLParameter("idsearch");
         var idgenMe = fn_getURLParameter("id");
         e('body').append('<div id="sti_rs_lightbox" class="sti_prod_lightbox2 "></div>');
         var box = e('#sti_rs_lightbox');
         box.html('');

         var Trends = "";
         for (var i = 0; i < obj.Trends.items.length; i++) {
             Trends += '<tr><td>' + obj.Trends.items[i].Month.toString() + '</td><td>' + obj.Trends.items[i].L1.toString() + '</td><td>' + obj.Trends.items[i].L2.toString() + '</td><td>' + obj.Trends.items[i].L3.toString() + '</td></tr>'
         }

         var History = "";
         for (var i = 0; i < obj.History.items.length; i++) {
             //History += '<tr><td>' + obj.History.items[i].col1.toString() + '</td><td>' + obj.History.items[i].col2.toString() + '</td><td><a href="' + obj.History.items[i].col3.toString() + '">Click</a> to View</td></tr>'
             History += '<tr><td>' + obj.History.items[i].col1.toString() + '</td><td>' + obj.History.items[i].col2.toString() + '</td><td><a href="#">Click</a> to View</td></tr>'
         }


         var Activity = "";
         for (var i = 0; i < obj.Activity.items.length; i++) {
             Activity += '<li class="' + obj.Activity.items[i].state.toString() + '">' + obj.Activity.items[i].cod.toString() + '</li>'
         }

         var lvl = "L1";
         if (obj.Level != undefined)
             lvl = obj.Level;
         var dataPage =
         '<ul class="sti_prod_lightbox_title">' +
         '   <li class="lvl">' + level + ' ' + lvl + '</li>' +
         //'   <li>' + upline + ': <span class="childName">' + obj.Name + '</span> > <span class="parentName">' + obj.ParentName + '</span></li>' +
         '</ul>' +
         '<ul class="sti_prod_lightbox_profile">' +
         '    <li>' +
         '        <span class="label">' + status + ':</span>' +
         '        <span class="text">' + obj.Status + '</span>' +
         '    </li>' +
         '    <li>' +
         '        <span class="label">' + name + ':</span>' +
         '        <span class="text">' + obj.Names + '</span>' +
         '    </li>';

         if (obj.Phone != "-" && idgens == "null") {
             var dataPage = dataPage +
             '    <li>' +
             '        <span class="label">' + email + ':</span>' +
             '        <span class="text">' + obj.Email + '</span>' +
             '    </li>' +
             '    <li>' +
             '        <span class="label">' + phone + ':</span>' +
             '        <span class="text">' + obj.Phone + '</span>' +
             '    </li>'; 
         }
        // idselectsearch = obj.AssociateID;
         var dataPage = dataPage +    
         '    <li>' +
         '        <span class="label">' + associateid + ':</span>' +
         '       <span class="text">' + obj.AssociateID + '</span>' +
         '    </li' +
         '    <li>' +
         '        <span class="label">' + country + ':</span>' +
         '        <span class="text">' + obj.Country + '</span' +
         '    </li>' +
         '</ul>' +
         '<table class="sti_prod_lightbox_table">' +
         '      <thead>' +
         '          <tr>' +
         '             <td colspan="4">' + autoshiptrends + ':</td>' +
         '          </tr>' +
         '              <tr>' +
         '                  <td>' + month + '</td>' +
         '              <td>' + l1 + '</td>' +
         '              <td>' + l2 + '</td>' +
         '              <td>' + l3 + '</td>' +
         '          </tr>' +
         '      </thead>' +
         '      <tbody>' + Trends +

         '      </tbody>' +
         '  </table>' +
         '   <table class="sti_prod_lightbox_table2"  style="display:none;">' +
         '      <thead>' +
         '          <tr>' +
         '              <td colspan="3">AutoShip History: 3 Month View</td>' +
         '          </tr>' +
         '      </thead>' +
         '      <tbody>' + History +
         '      </tbody>' +
         '  </table>' +

         '<div class="sti_prod_lightbox_MonthActivity">' +
         '  <h5>' + monthamonth + '</h5>' +
         '  <ul class="qualification2"> ' + Activity + '</ul>' +
         '</div>' +
         '<a id="A1" class="close close_btn_lightbox" href="#">close</a>';


         var buttom1 = '<div class="buttom1" onclick="fnShowtopid(' + obj.AssociateID + ')"><div class="btmimg"><img src="src/img/showtop-white.png" /></div><div  class="btmtxt">' + textliteral2 + '</div> </div>';
         var buttom2 = '<div class="buttom2"><div class="btmimg"><img src="src/img/ico-messpopup.png" /></div><div  class="btmtxt">ASSOCIATE</div> </div>';
         var buttom3 = '<div class="buttom3"><div class="btmimg"><img src="src/img/ico-messpopup.png" /></div><div  class="btmtxt">SPONSOR</div> </div>';
         var buttoms = '<div id="buttom">' + buttom1 + ' </div>';
         //obj.SponsorName

         var listchain = '<div class="contenttitle"><span>' + textliteral3 + '</span></div> <div id="contentchain" class="contentchain" ><ul class="">';
         if (idgenMe == obj.AssociateID) {
             listchain = listchain + '<li><div class="numberchain">1.</div><div class="textchain"> ' + obj.SponsorName + '</div></li>';
         } else {

             if (obj.Chain.items != null) {
                 for (var i = 0; i < obj.Chain.items.length; i++) {
                     listchain = listchain + '<li><div class="numberchain">' + obj.Chain.items[i].Index.toString() + '.</div><div class="textchain"><a href="javascript:fnShowtopid(' + obj.Chain.items[i].Id.toString() + ')" > ' + obj.Chain.items[i].Name.toString() + '</a></div></li>';
                 }
             }
         }

         //listchain = listchain+ 
         //listchain = listchain+ '<li><div class="numberchain">2.</div><div class="textchain">Camelot International LLC</div></li>';
         //listchain = listchain+ '<li><div class="numberchain">3.</div><div class="textchain">Carter, Jacque Jennings</div></li>';
         //listchain = listchain+ '<li><div class="numberchain">4.</div><div class="textchain">Key2</div></li>';
         //listchain = listchain+ '<li><div class="numberchain">5.</div><div class="textchain">Gardner, Cristie</div></li>';
         //listchain = listchain+ '<li><div class="numberchain">6.</div><div class="textchain">Freedom Coach</div></li>';
         //listchain = listchain+ '<li><div class="numberchain">7.</div><div class="textchain">Terrell, Maggie & Steve</div></li>';
         //listchain = listchain+ '<li><div class="numberchain">8.</div><div class="textchain">Jones, James Earl</div></li>';
         //listchain = listchain+ '<li><div class="numberchain">9</div><div class="textchain">Candy, John Sweet</div></li>';
         //listchain = listchain+ '<li><div class="numberchain">10.</div><div class="textchain">Momentum International</div></li>';
         //listchain = listchain+ '<li><div class="numberchain">11.</div><div class="textchain">Live</div></li>';
         listchain = listchain + '</ul></div>';



         var PageMain = '<div id="mainpopup" class=" sti_borderpopup ' + obj.Status + '"><div id="sti1">' + dataPage + '</div><div id="sti2">' + listchain + '</div></div>' + buttoms;
         e('#sti_rs_lightbox').append(PageMain);
       
        return box;
    };

    e.fn.addNode = function (levelID, obj) {
        $this = e(this);
       
        var $Q = "";
        var $data = "";

        for (var i = 0; i < obj.qualification.length; i += 1) {
            $Q += '<li class="' + obj.qualification[i].toString() + '"></li>';
        }
        
        var iconData = "";
        var iconState = "";
        var iconDatafull = "";
        var iconStatefull = "";
       
        switch (levelID) {
            case 0:
                if (obj.state == "completed")
                    iconState = "icon-completed_86";
                if (obj.state == "scheduled") {
                    iconState = "nro-ordinal_86";
                    iconData = obj.date;
                }
                if (obj.state == "failed")
                    iconState = "icon-failed_86";
                if (obj.state == "cancelled")
                    iconState = "icon-cancelled_86";
                if (obj.state == "nonautoship")
                    iconState = "icon-nonautoship_86";

                
                levelID = "#sti_slide_lvl0";
                $data = '<li><div class="sti_prod ' + obj.state + '" data-effects="false"><span class="sti_prod-title">' + obj.title + '</span>' +
                 '<a href="javascript:void(0)"  rel="' + obj.id + '" class="link86 content sti_lightbox" ><span class="' + iconState + '">' + iconData + '</span></a>' +
                 '<span class="sti_prod-date">' + asdate + ':' + obj.date + '</span>' +
                 '<ul class="qualification">' + $Q + '</ul></div></li>';
                break;
            case 1: 

                if (obj.state == "completed")
                    iconState = "icon-completed_86";
                if (obj.state == "scheduled") {
                    iconState = "nro-ordinal_86";
                    iconData = obj.date;
                }
                if (obj.state == "failed")
                    iconState = "icon-failed_86";
                if (obj.state == "cancelled")
                    iconState = "icon-cancelled_86";
                if (obj.state == "nonautoship")
                    iconState = "icon-nonautoship_86";
                if (levelID == 1)
                    levelID = "#sti_slide_lvl1";

                $data = '<li><div class="sti_prod ' + obj.state + '" data-effects="false"><span class="sti_prod-title">' + obj.title + '</span>' +
                 '<a href="javascript:void(0)"  rel="' + obj.id + '" class="link86 content sti_lightbox" ><span class="' + iconState + '">' + iconData + '</span></a>' +
                 '<span class="ribbon ribbon_circle" >' + obj.nro + '</span>' +
                 '<span class="sti_prod-date">' + asdate + ':' + obj.date + '</span>' +
                 '<ul class="qualification">' + $Q + '</ul></div></li>';
                break;
            case 21: case 22:
                
                if (obj.state == "completed")
                {
                    iconStatefull = "icon-completed_86";
                    iconState = "icon-completed_60";
                }
                if (obj.state == "scheduled") {
                    iconState = "nro-ordinal_60";
                    iconData = obj.date;
                    iconStatefull = "nro-ordinal_86";
                    iconDatafull = obj.date;
                }
                if (obj.state == "failed") {
                    iconStatefull = "icon-failed_86";
                    iconState = "icon-failed_60";
                }
                if (obj.state == "cancelled") {
                    iconStatefull = "icon-cancelled_86";
                    iconState = "icon-cancelled_60";
                }
                if (obj.state == "nonautoship") {
                    iconStatefull = "icon-nonautoship_86";
                    iconState = "icon-nonautoship_60";
                }

                if (levelID == 21)
                    levelID = "#sti_slide_lvl2_1";
                if (levelID == 22)
                    levelID = "#sti_slide_lvl2_2";
                $data = 
                       '<li style="width: 60px;">' +
                       '    <div class="sti_prod ' + obj.state + ' sti_prodlvl3" data-effects="false">' +
                       '        <span class="sdt_active"></span>' +
                       '        <span class="sdt_wrap" style="display:none">' +
                       '            <div class="sti_prod ' + obj.state + '" data-effects="false">' +
                       '                <span class="sti_prod-title">' + obj.title + '</span>' +
                       '                <a  href="javascript:void(0)"  rel="' + obj.id + '" class="link86 sti_lightbox" ><span class="' + iconStatefull + '">' + iconDatafull + '</span></a>' +
                       '                <span class="ribbon ribbon_circle" >' + obj.nro + '</span>' +
                       '                <span class="sti_prod-date">' + asdate + ':' + obj.date + '</span>' +
                       '                <ul class="qualification">' + $Q + '</ul>' +
                       '            </div>' +
                       '        </span>' +
                       '        <span class="ribbon ribbon_circle" >' + obj.nro + '</span>' +
                       '        <a  href="javascript:void(0)"  class="link60 content" ><span class="' + iconState + '">' + iconData + '</span></a>' +
                       '    </div>' +
                       '</li>';
                break;

            case 31: case 32:
                if (obj.state == "completed") {
                    iconStatefull = "icon-completed_86";
                    iconState = "icon-completed_44";
                }
                if (obj.state == "scheduled") {
                    iconState = "nro-ordinal_44";
                    iconData = obj.date;
                    iconStatefull = "nro-ordinal_86";
                    iconDatafull = obj.date;
                }
                if (obj.state == "failed") {
                    iconStatefull = "icon-failed_86";
                    iconState = "icon-failed_44";
                }
                if (obj.state == "cancelled") {
                    iconStatefull = "icon-cancelled_86";
                    iconState = "icon-cancelled_44";
                }
                if (obj.state == "nonautoship") {
                    iconStatefull = "icon-nonautoship_86";
                    iconState = "icon-nonautoship_44";
                }
                if (levelID == 31)
                    levelID = "#sti_slide_lvl3_1";
                if (levelID == 32)
                    levelID = "#sti_slide_lvl3_2";
                $data =
                       '<li style="width:42px;">' +
                       '    <div class="sti_prod ' + obj.state + ' sti_prodlvl3" data-effects="false">' +
                       '        <span class="sdt_active"></span>' +
                       '        <span class="sdt_wrap" style="display:none">' +
                       '            <div class="sti_prod ' + obj.state + '" data-effects="false">' +
                       '                <span class="sti_prod-title">' + obj.title + '</span>' +
                       '                <a  href="javascript:void(0)"  rel="' + obj.id + '" class="link86 sti_lightbox" ><span class="' + iconStatefull + '">' + iconDatafull + '</span></a>' +
                       '                <span class="ribbon ribbon_circle" >' + obj.nro + '</span>' +
                       '                <span class="sti_prod-date">' + asdate + ':' + obj.date + '</span>' +
                       '                <ul class="qualification">' + $Q + '</ul>' +
                       '            </div>' +
                       '        </span>' +
                       '        <span class="ribbon ribbon_circle" >' + obj.nro + '</span>' +
                       '        <a  href="javascript:void(0)"  class="link44 content" ><span class="' + iconState + '">' + iconData + '</span></a>' +
                       '    </div>' +
                       '</li>';
                break;

            case 41: case 42:
                if (obj.state == "completed") {
                    iconStatefull = "icon-completed_86";
                    iconState = "icon-completed_44";
                }
                if (obj.state == "scheduled") {
                    iconState = "nro-ordinal_44";
                    iconData = obj.date;
                    iconStatefull = "nro-ordinal_86";
                    iconDatafull = obj.date;
                }
                if (obj.state == "failed") {
                    iconStatefull = "icon-failed_86";
                    iconState = "icon-failed_44";
                }
                if (obj.state == "cancelled") {
                    iconStatefull = "icon-cancelled_86";
                    iconState = "icon-cancelled_44";
                }
                if (obj.state == "nonautoship") {
                    iconStatefull = "icon-nonautoship_86";
                    iconState = "icon-nonautoship_44";
                }
                if (levelID == 41)
                    levelID = "#sti_slide_lvl4_1";
                if (levelID == 42)
                    levelID = "#sti_slide_lvl4_2";
                $data =
                       '<li style="width:41px;">' +
                       '    <div class="sti_prod ' + obj.state + ' sti_prodlvl3" data-effects="false">' +
                       '        <span class="sdt_active"></span>' +
                       '        <span class="sdt_wrap" style="display:none">' +
                       '            <div class="sti_prod ' + obj.state + '" data-effects="false">' +
                       '                <span class="sti_prod-title">' + obj.title + '</span>' +
                       '                <a  href="javascript:void(0)"  rel="' + obj.id + '" class="link86 sti_lightbox" ><span class="' + iconStatefull + '">' + iconDatafull + '</span></a>' +
                       '                <span class="ribbon ribbon_circle" >' + obj.nro + '</span>' +
                       '                <span class="sti_prod-date">' + asdate + ':' + obj.date + '</span>' +
                       '                <ul class="qualification">' + $Q + '</ul>'+
                       '            </div>' +
                       '        </span>' +
                       '        <span class="ribbon ribbon_circle" >' + obj.nro + '</span>' +
                       '        <a  href="javascript:void(0)"  class="link44 content" ><span class="' + iconState + '">' + iconData + '</span></a>' +
                       '    </div>' +
                       '</li>';
                break;
        }
        e(levelID).append($data);
       
    };

    e.fn.addNodeEmpty = function (levelID, count) {
        $this = e(this);
        var $Q = "";
        var $data = "";
        switch (levelID) {
            case 0:
                levelID = "#sti_slide_lvl0";
                $data = '<li><div class="sti_prod empty" data-effects="false"><span class="sti_prod-title">EMPTY</span>' +
               '<a href="javascript:void(0)" class="link86 " >' +
               '<span class="icon-completed_86"></span></a><span class="sti_prod-date"></span><ul class="qualification"></ul></div></li>';
                e(levelID).append($data);
                break;

            case 1:
                for (var i = 0; i < count; i++) {
                    levelID = "#sti_slide_lvl1";
                    $data = '<li><div class="sti_prod empty" data-effects="false"><span class="sti_prod-title">EMPTY</span>' +
                   '<a href="javascript:void(0)" class="link86 " >' +
                   '<span class="icon-completed_86"></span></a><span class="sti_prod-date"></span><ul class="qualification"></ul></div></li>';
                    e(levelID).append($data);
                }
                break;
           
            case 21: case 22:
               
                if (levelID == 21) {
                   
                    levelID = "#sti_slide_lvl2_1";
                }
                if (levelID == 22) {
                   
                    levelID = "#sti_slide_lvl2_2";
                }
                for (var i = 0; i < count; i++) {
                    $data = '<li style="width: 60px;"><div class="sti_prod empty" data-effects="false">' +
                   '<a href="javascript:void(0)" class="link60 " >' +
                   '<span class="icon-completed_60"></span></a></div></li>';
                    e(levelID).append($data);
                }
                break;

            case 31: case 32:
                if (levelID == 31) {
                    levelID = "#sti_slide_lvl3_1";
                }
                if (levelID == 32) {
                    levelID = "#sti_slide_lvl3_2";
                }
                for (var i = 0; i < count; i++) {
                    $data = '<li style="width:42px;"><div class="sti_prod empty" data-effects="false">' +
                   '<a href="javascript:void(0)" class="link44 " >' +
                   '<span class="icon-completed_44"></span></a></div></li>';
                    e(levelID).append($data);
                }
                break;

            case 41: case 42:

                if (levelID == 41) {
                    levelID = "#sti_slide_lvl4_1";
                }
                if (levelID == 42) {
                    levelID = "#sti_slide_lvl4_2";
                }
                for (var i = 0; i < count; i++) {
                    $data = '<li style="width:41px;"><div class="sti_prod empty" data-effects="false">' +
                   '<a href="javascript:void(0)" class="link44 " >' +
                   '<span class="icon-completed_44"></span></a></div></li>';
                    e(levelID).append($data);
                }
                break;
        }
    };

    e.fn.PositionLayer = function PositionLayer(xy) {
        var X = (xy.pageX);
        var Y = (xy.pageY);
        return new Array(X, Y);
    }
})(jQuery);

(function ($) {

    /*
    * $ lightbox
    */
    $.fn.lightbox_d = function PageLock() {
        $('.sti_lightbox').unbind('click').off("click", ".sti_lightbox");
    };

    $.fn.lightbox_me = function (options) {
        return this.each(function () {
            var
                opts = $.extend({}, $.fn.lightbox_me.defaults, options),
                $overlay = $(),
                $self = $(this),
                //$iframe = $('<iframe id="foo" style="z-index: ' + (opts.zIndex + 1) + ';border: none; margin: 0; padding: 0; position: absolute; width: 100%; height: 100%; top: 0; left: 0; filter: mask();"/>'),
                ie6 = ($.browser.msie && $.browser.version < 7);

            if (opts.showOverlay) {

                //check if there's an existing overlay, if so, make subequent ones clear
                var $currentOverlays = $(".js_lb_overlay:visible");
                if ($currentOverlays.length > 0) {
                    $overlay = $('<div class="lb_overlay_clear js_lb_overlay"/>');
                } else {
                    $overlay = $('<div class="' + opts.classPrefix + '_overlay js_lb_overlay"/>');
                }
            }

            /*----------------------------------------------------
               DOM Building
            ---------------------------------------------------- */
            if (ie6) {
                var src = /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank';
                //$iframe.attr('src', src);
                //$('body').append($iframe);
            } // iframe shim for ie6, to hide select elements
            $('body').append($self.hide()).append($overlay);


            /*----------------------------------------------------
               Overlay CSS stuffs
            ---------------------------------------------------- */

            // set css of the overlay
            if (opts.showOverlay) {
                setOverlayHeight(); // pulled this into a function because it is called on window resize.
                $overlay.css({ position: 'absolute', width: '100%', top: 0, left: 0, right: 0, bottom: 0, zIndex: (opts.zIndex + 2), display: 'none' });
                if (!$overlay.hasClass('lb_overlay_clear')) {
                    $overlay.css(opts.overlayCSS);
                }
            }

            /*----------------------------------------------------
               Animate it in.
            ---------------------------------------------------- */
            //
            if (opts.showOverlay) {
                $overlay.fadeIn(opts.overlaySpeed, function () {
                    setSelfPosition();
                    $self[opts.appearEffect](opts.lightboxSpeed, function () { setOverlayHeight(); setSelfPosition(); opts.onLoad() });
                });
            } else {
                setSelfPosition();
                $self[opts.appearEffect](opts.lightboxSpeed, function () { opts.onLoad() });
            }

            /*----------------------------------------------------
               Hide parent if parent specified (parentLightbox should be jquery reference to any parent lightbox)
            ---------------------------------------------------- */
            if (opts.parentLightbox) {
                opts.parentLightbox.fadeOut(200);
            }


            /*----------------------------------------------------
               Bind Events
            ---------------------------------------------------- */

            $(window).resize(setOverlayHeight)
                     .resize(setSelfPosition)
                     .scroll(setSelfPosition);

            $(window).bind('keyup.lightbox_me', observeKeyPress);

            if (opts.closeClick) {
                $overlay.click(function (e) { closeLightbox(); e.preventDefault; });

            }

            $self.delegate(opts.closeSelector, "click", function (e) { closeLightbox(); e.preventDefault(); });

            $self.bind('close', closeLightbox);
            $self.bind('reposition', setSelfPosition);



            /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
              -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */


            /*----------------------------------------------------
               Private Functions
            ---------------------------------------------------- */

            /* Remove or hide all elements */
            function closeLightbox() {
                var s = $self[0].style;
                if (opts.destroyOnClose) {
                    $self.add($overlay).remove();
                } else {
                    $self.add($overlay).hide();
                }

                //show the hidden parent lightbox
                if (opts.parentLightbox) {
                    opts.parentLightbox.fadeIn(200);
                }

                //$iframe.remove();

                // clean up events.
                $self.undelegate(opts.closeSelector, "click");

                $(window).unbind('reposition', setOverlayHeight);
                $(window).unbind('reposition', setSelfPosition);
                $(window).unbind('scroll', setSelfPosition);
                $(window).unbind('keyup.lightbox_me');
                if (ie6)
                    s.removeExpression('top');

                $('.sti_prod_lightbox').remove();
                $('.js_lb_overlay').remove();
                opts.onClose();
            }


            /* Function to bind to the window to observe the escape/enter key press */
            function observeKeyPress(e) {
                if ((e.keyCode == 27 || (e.DOM_VK_ESCAPE == 27 && e.which == 0)) && opts.closeEsc) closeLightbox();
            }


            /* Set the height of the overlay
                    : if the document height is taller than the window, then set the overlay height to the document height.
                    : otherwise, just set overlay height: 100%
            */
            function setOverlayHeight() {
                if ($(window).height() < $(document).height()) {
                    $overlay.css({ height: $(document).height() + 'px' });
                    //$iframe.css({ height: $(document).height() + 'px' });
                } else {
                    $overlay.css({ height: '100%' });
                    if (ie6) {
                        $('html,body').css('height', '100%');
                        //$iframe.css('height', '100%');
                    } // ie6 hack for height: 100%; TODO: handle this in IE7
                }
            }


            /* Set the position of the modal'd window ($self)
                    : if $self is taller than the window, then make it absolutely positioned
                    : otherwise fixed
            */
            function setSelfPosition() {
                var s = $self[0].style;

                // reset CSS so width is re-calculated for margin-left CSS
                $self.css({ left: '50%', marginLeft: ($self.outerWidth() / 2) * -1, zIndex: (opts.zIndex + 3) });


                /* we have to get a little fancy when dealing with height, because lightbox_me
                    is just so fancy.
                 */

                // if the height of $self is bigger than the window and self isn't already position absolute
                if (($self.height() + 80 >= $(window).height()) && ($self.css('position') != 'absolute' || ie6)) {

                    // we are going to make it positioned where the user can see it, but they can still scroll
                    // so the top offset is based on the user's scroll position.
                    var topOffset = $(document).scrollTop() + 40;
                    $self.css({ position: 'absolute', top: topOffset + 'px', marginTop: 0 })
                    if (ie6) {
                        s.removeExpression('top');
                    }
                } else if ($self.height() + 80 < $(window).height()) {
                    //if the height is less than the window height, then we're gonna make this thing position: fixed.
                    // in ie6 we're gonna fake it.
                    if (ie6) {
                        s.position = 'absolute';
                        if (opts.centered) {
                            s.setExpression('top', '(document.documentElement.clientHeight || document.body.clientHeight) / 2 - (this.offsetHeight / 2) + (blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"')
                            s.marginTop = 0;
                        } else {
                            var top = (opts.modalCSS && opts.modalCSS.top) ? parseInt(opts.modalCSS.top) : 0;
                            s.setExpression('top', '((blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + ' + top + ') + "px"')
                        }
                    } else {
                        if (opts.centered) {
                            $self.css({ position: 'fixed', top: '50%', marginTop: ($self.outerHeight() / 2) * -1 })
                        } else {
                            $self.css({ position: 'fixed' }).css(opts.modalCSS);
                        }

                    }
                }
            }

        });

        

    };

    $.fn.lightbox_me.defaults = {

        // animation
        appearEffect: "fadeIn",
        appearEase: "",
        overlaySpeed: 100,
        lightboxSpeed: 100,

        // close
        closeSelector: ".close",
        closeClick: true,
        closeEsc: true,

        // behavior
        destroyOnClose: true,
        showOverlay: true,
        parentLightbox: false,

        // callbacks
        onLoad: function () {},
        onClose: function () {},

        // style
        classPrefix: 'lb',
        zIndex: 999,
        centered: true,
        modalCSS: { top: '40px' },
        overlayCSS: { background: 'black', opacity: .3 }
    }
})(jQuery);

function rs_lightbox(x, me) {
     LoadShow();
      var idSearch = me.attr("rel");
      var success = function (r) {
          var obj = SetObjectDetail(idSearch, r);
          LoadHide();
          if (obj.Name != null) {
              $.fn.ShowBox(obj).lightbox_me({ centered: true });
              x.preventDefault();
              LoadScrollchain();
              setTimeout(function () { LoadScrollchain(); }, 1000);
          }
      };
      var idgen = fn_getURLParameter("id");
      if (idgen!="null")
          fn_loadDetail(idSearch, success, "yes", idgen);
}

function PositionChild(i, x, e, success) {

    switch (i) {
        case 'level_2_1':
            if (x > 46 && x < 85) {
                var child1 = $('> li:nth-child(1)', $(e).parents('ul')).position();
                $leftPosition = child1.left; success = true;
            }
            else if (x >= 121 && x <= 158) {
                var child2 = $('> li:nth-child(2)', $(e).parents('ul')).position();
                $leftPosition = child2.left; success = true;
            }
            else if (x >= 203 && x <= 235) {
                var child3 = $('> li:nth-child(3)', $(e).parents('ul')).position();
                $leftPosition = child3.left; success = true;
            }
            else if (x >= 271 && x <= 309) {
                var child4 = $('> li:nth-child(4)', $(e).parents('ul')).position();
                $leftPosition = child4.left; success = true;
            }
            break;
        case 'level_2_2':
            if (x > 544 && x < 584) {
                var child1 = $('> li:nth-child(1)', $(e).parents('ul')).position();
                $leftPosition = child1.left; success = true;
            }
            else if (x >= 620 && x <= 659) {
                var child2 = $('> li:nth-child(2)', $(e).parents('ul')).position();
                $leftPosition = child2.left; success = true;
            }
            else if (x >= 695 && x <= 736) {
                var child3 = $('> li:nth-child(3)', $(e).parents('ul')).position();
                $leftPosition = child3.left; success = true;
            }
            else if (x >= 772 && x <= 811) {
                var child4 = $('> li:nth-child(4)', $(e).parents('ul')).position();
                $leftPosition = child4.left; success = true;
            }
            break;
        case 'level_3_1':
            if (x > 48 && x < 76) {
                var child1 = $('> li:nth-child(1)', $(e).parents('ul')).position();
                $leftPosition = child1.left; success = true;
            }
            else if (x >= 105 && x <= 130) {
                var child2 = $('> li:nth-child(2)', $(e).parents('ul')).position();
                $leftPosition = child2.left; success = true;
            }
            else if (x >= 164 && x <= 188) {
                var child3 = $('> li:nth-child(3)', $(e).parents('ul')).position();
                $leftPosition = child3.left; success = true;
            }
            else if (x >= 219 && x <= 249) {
                var child4 = $('> li:nth-child(4)', $(e).parents('ul')).position();
                $leftPosition = child4.left; success = true;
            }
            break;
        case 'level_3_2':
            if (x > 534 && x < 560) {
                var child1 = $('> li:nth-child(1)', $(e).parents('ul')).position();
                $leftPosition = child1.left; success = true;
            }
            else if (x >= 591 && x <= 619) {
                var child2 = $('> li:nth-child(2)', $(e).parents('ul')).position();
                $leftPosition = child2.left; success = true;
            }
            else if (x >= 648 && x <= 674) {
                var child3 = $('> li:nth-child(3)', $(e).parents('ul')).position();
                $leftPosition = child3.left; success = true;
            }
            else if (x >= 705 && x <= 734) {
                var child4 = $('> li:nth-child(4)', $(e).parents('ul')).position();
                $leftPosition = child4.left; success = true;
            }
            break;
        case 'level_4_1':
            if (x > 147 && x < 170) {
                var child1 = $('> li:nth-child(1)', $(e).parents('ul')).position();
                $leftPosition = child1.left; success = true;
            }
            else if (x >= 203 && x <= 232) {
                var child2 = $('> li:nth-child(2)', $(e).parents('ul')).position();
                $leftPosition = child2.left; success = true;
            }
            else if (x >= 259 && x <= 286) {
                var child3 = $('> li:nth-child(3)', $(e).parents('ul')).position();
                $leftPosition = child3.left; success = true;
            }
            else if (x >= 315 && x <= 343) {
                var child4 = $('> li:nth-child(4)', $(e).parents('ul')).position();
                $leftPosition = child4.left; success = true;
            }
            break;
        case 'level_4_2':
            if (x > 647 && x <= 674) {
                var child1 = $('> li:nth-child(1)', $(e).parents('ul')).position();
                $leftPosition = child1.left; success = true;
            }
            else if (x >= 704 && x <= 733) {
                var child2 = $('> li:nth-child(2)', $(e).parents('ul')).position();
                $leftPosition = child2.left; success = true;
            }
            else if (x >= 761 && x <= 789) {
                var child3 = $('> li:nth-child(3)', $(e).parents('ul')).position();
                $leftPosition = child3.left; success = true;
            }
            else if (x >= 819 && x <= 846) {
                var child4 = $('> li:nth-child(4)', $(e).parents('ul')).position();
                $leftPosition = child4.left; success = true;
            }
            break;
    }

    return success;
}

function rs_mouseHover() {
    $('.showbiz-container').mouseleave(function () {
        $('.Clone').remove();
        $('.sti_lightbox').unbind('click').off("click", ".sti_lightbox");
        $(".sti_lightbox").click(function (x) { rs_lightbox(x, $(this)); });
    });
    $('.sdt_menu > li .content').mousemove(function (xy) {
        $('.Clone').remove();
        var success = false;
        var Layer = $.fn.PositionLayer(xy);
        var levelID = $(this).parents('div').parents('div').parents('div').parents('div').attr('id');
        success = PositionChild(levelID, Layer[0], this, success);
        var id = $(this).parents('div').parents('div').parents('div').attr('id');
        if (success == true) {
            var $elem = $(this).parents('li');
            var varClone = $elem.find('.sdt_wrap').html();
            $('<div class="Clone"></div>').appendTo('#' + id);
            $(".Clone").html('');
            var $cloned = $(varClone).clone(true, true);
            $(".Clone").append($cloned);
            $(".Clone")
                .stop(true)
                .animate({ 'top': '-34px' }, 0, 'easeOutBack')
                .andSelf()
                .css({
                    top: '-34px',
                    left: $leftPosition - 10,
                    display: 'block',
                    height: '135px',
                    position: 'absolute',
                    'z-index': '666'
                })
                .mouseleave(function () {
                    $('.Clone').remove();
                    $('.sti_lightbox').unbind('click').off("click", ".sti_lightbox");
                    $(".sti_lightbox").click(function (x) { rs_lightbox(x, $(this)); });
                })
                .find(".sti_lightbox").click(function (x) { rs_lightbox(x, $(this)); });
        }
    });
};


function ShowPrev(IdLevel) {
    $.fn.lightbox_d();
    var node;
    switch (IdLevel) {
        case 'sti_nodes_lvl1':
            node = PrevParent(Nodelevel1_1, i1);
            if (node != undefined) {
                Nodelevel2_1_1 = ""; Nodelevel2_2_1 = "";
                i2_1 = 1; i2_2 = 1; i3_1 = 1; i3_2 = 1; i4_1 = 1; i4_2 = 1;
                icancelled2_1 = 0; icancelled2_2 = 0; icancelled3_1 = 0; icancelled3_2 = 0; icancelled4_1 = 0; icancelled4_2 = 0;
                $.fn.ClearItems(21);
                $.fn.ClearItems(22);
                $.fn.ClearItems(31);
                $.fn.ClearItems(32);
                $.fn.ClearItems(41);
                $.fn.ClearItems(42);
                Nodelevel1_1 = node.id;
                SetChildren(Nodelevel1_1, "2", 21);
                $.fn.addNodeEmpty(21, calculeEmpty(i2_1));
                $.fn.addNodeEmpty(22, calculeEmpty(i2_2));
                $.fn.addNodeEmpty(31, calculeEmpty_l3(i3_1));
                $.fn.addNodeEmpty(32, calculeEmpty_l3(i3_2));
                $.fn.addNodeEmpty(41, calculeEmpty_l3(i4_1));
                $.fn.addNodeEmpty(42, calculeEmpty_l3(i4_2));
                $.fn.AddBackup(21, calculeBackup(i2_1));
                $.fn.AddBackup(22, calculeBackup(i2_2));
                $.fn.AddBackup(31, calculeBackup(i3_1));
                $.fn.AddBackup(32, calculeBackup(i3_2));
                $.fn.AddBackup(41, calculeBackup(i4_1));
                $.fn.AddBackup(42, calculeBackup(i4_2));

                //$.fn.AddCancelled(21, icancelled2_1);
                //$.fn.AddCancelled(22, icancelled2_2);
                //$.fn.AddCancelled(31, icancelled3_1);
                //$.fn.AddCancelled(32, icancelled3_2);
                //$.fn.AddCancelled(41, icancelled4_1);
                //$.fn.AddCancelled(42, icancelled4_2);

                $('#level_2_1').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
                $('#level_2_2').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
                $('#level_3_1').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
                $('#level_3_2').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
                $('#level_4_1').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
                $('#level_4_2').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });

            }
            break;
        //case 'sti_nodes_lvl11':
        //    node = PrevParent(Nodelevel1_1, i1);
        //    if (node != undefined) {
        //        Nodelevel2_1_1 = "";
        //        i2_1 = 1; i2_2 = 1; i3_1 = 1; i3_2 = 1; i4_1 = 1; i4_2 = 1;
        //        $.fn.ClearItems(2);
        //        $.fn.ClearItems(3);
        //        Nodelevel1_1 = node.id;
        //        SetChildren(Nodelevel1_1, "2", 2);
        //        $.fn.addNodeEmpty(2, calculeEmpty(i2_1));
        //        $.fn.addNodeEmpty(3, calculeEmpty(i3_1));
        //        $.fn.AddBackup(2, calculeBackup(i2_1));
        //        $.fn.AddBackup(3, calculeBackup(i3_1));
                
        //    }
        //    break;
        //case 'sti_nodes_lvl2':
        //    node = PrevParent(Nodelevel2_1_1, i2_1);
        //    if (node != undefined) {
        //        i3_1 = 1; i4_1 = 1;
        //        $.fn.ClearItems(3);
                
        //        Nodelevel2_1_1 = node.id;
        //        SetChildren(Nodelevel2_1_1, "3", 3);
        //        $.fn.addNodeEmpty(3, calculeEmpty_l3(i3_1));
        //        $.fn.AddBackup(3, calculeBackup(i3_1));
                
        //    }
        //    break;
        //case 'sti_nodes_lvl3':
        //    break;
        case 'sti_nodes_lvl2-1':
            node = PrevParent(Nodelevel2_1_1, i2_1);
            if (node != undefined) {
                i3_1 = 1; i4_1 = 1;
                icancelled3_1 = 0; icancelled4_1 = 0;
                $.fn.ClearItems(31);
                $.fn.ClearItems(41);
                Nodelevel2_1_1 = node.id;
                SetChildren(Nodelevel2_1_1, "3", 31, true);


                //$.fn.AddCancelled(31, icancelled3_1);
                //$.fn.AddCancelled(41, icancelled4_1);


                $.fn.addNodeEmpty(31, calculeEmpty_l3(i3_1));
                $.fn.AddBackup(31, calculeBackup(i3_1));
                $('#level_3_1').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
                $.fn.addNodeEmpty(41, calculeEmpty_l3(i4_1));
                $.fn.AddBackup(41, calculeBackup(i4_1));
                $('#level_4_1').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });

            }
            break;
        case 'sti_nodes_lvl2-2':
            node = PrevParent(Nodelevel2_2_1, i2_2);
            if (node != undefined) {
                i3_2 = 1; i4_2 = 1;
                icancelled3_2 = 0; icancelled4_2 = 0;
                $.fn.ClearItems(32);
                $.fn.ClearItems(42);
                Nodelevel2_2_1 = node.id;
                SetChildren(Nodelevel2_2_1, "3", 32, true);

                //$.fn.AddCancelled(32, icancelled3_2);
                //$.fn.AddCancelled(42, icancelled4_2);


                $.fn.addNodeEmpty(32, calculeEmpty_l3(i3_2));
                $('#level_3_2').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
                $.fn.AddBackup(32, calculeBackup(i3_2));
                $.fn.addNodeEmpty(42, calculeEmpty_l3(i4_2));
                $.fn.AddBackup(42, calculeBackup(i4_2));
                $('#level_4_2').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });

            }

            break;
        case 'sti_nodes_lvl3-1':
          //  alert("Level 3 - 1");
            break;
        case 'sti_nodes_lvl3-2':
           // alert("Level 3 - 2");
            break;
        case 'sti_nodes_lvl4-1':
          //  alert("Level 4 - 1");
            break;
        case 'sti_nodes_lvl4-2':
          //  alert("Level 4 - 2");
            break;
    }

    $(".sti_lightbox").click(function (x) { rs_lightbox(x, $(this)); });
    rs_mouseHover();
    fn_validatesize();
}

function ShowNext(IdLevel) {
    $.fn.lightbox_d();

    var node;
    switch (IdLevel) {
        case 'sti_nodes_lvl1':
            node = NextParent(Nodelevel1_1, i1);
            if (node != undefined) {
                Nodelevel2_1_1 = ""; Nodelevel2_2_1 = "";
                i2_1 = 1; i2_2 = 1; i3_1 = 1; i3_2 = 1; i4_1 = 1; i4_2 = 1;
                icancelled2_1 = 0; icancelled2_2 = 0; icancelled3_1 = 0; icancelled3_2 = 0; icancelled4_1 = 0; icancelled4_2 = 0;
                $.fn.ClearItems(21);
                $.fn.ClearItems(22);
                $.fn.ClearItems(31);
                $.fn.ClearItems(32);
                $.fn.ClearItems(41);
                $.fn.ClearItems(42);
                Nodelevel1_1 = node.id;
                SetChildren(Nodelevel1_1, "2", 21);
                $.fn.addNodeEmpty(21, calculeEmpty(i2_1));
                $.fn.addNodeEmpty(22, calculeEmpty(i2_2));

                $.fn.addNodeEmpty(31, calculeEmpty_l3(i3_1));
                $.fn.addNodeEmpty(32, calculeEmpty_l3(i3_2));
                $.fn.addNodeEmpty(41, calculeEmpty_l3(i4_1));
                $.fn.addNodeEmpty(42, calculeEmpty_l3(i4_2));
                $.fn.AddBackup(21, calculeBackup(i2_1));
                $.fn.AddBackup(22, calculeBackup(i2_2));
                $.fn.AddBackup(31, calculeBackup(i3_1));
                $.fn.AddBackup(32, calculeBackup(i3_2));
                $.fn.AddBackup(41, calculeBackup(i4_1));
                $.fn.AddBackup(42, calculeBackup(i4_2));

                //$.fn.AddCancelled(21, icancelled2_1);
                //$.fn.AddCancelled(22, icancelled2_2);
                //$.fn.AddCancelled(31, icancelled3_1);
                //$.fn.AddCancelled(32, icancelled3_2);
                //$.fn.AddCancelled(41, icancelled4_1);
                //$.fn.AddCancelled(42, icancelled4_2);

                $('#level_2_1').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
                $('#level_2_2').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
                $('#level_3_1').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
                $('#level_3_2').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
                $('#level_4_1').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
                $('#level_4_2').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });

            }
            
            break;
        case 'sti_nodes_lvl2-1':
            node = NextParent(Nodelevel2_1_1, i2_1);
            if (node != undefined) {
               icancelled3_1 = 0; icancelled4_1 = 0;
                i3_1 = 1; i4_1 = 1; 
                $.fn.ClearItems(31);
                $.fn.ClearItems(41);
                Nodelevel2_1_1 = node.id;
                SetChildren(Nodelevel2_1_1, "3", 31, true);
                $.fn.addNodeEmpty(31, calculeEmpty_l3(i3_1));

               
                //$.fn.AddCancelled(31, icancelled3_1);              
                //$.fn.AddCancelled(41, icancelled4_1);
               

                $.fn.AddBackup(31, calculeBackup(i3_1));
                $('#level_3_1').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
                $.fn.addNodeEmpty(41, calculeEmpty_l3(i4_1));
                $.fn.AddBackup(41, calculeBackup(i4_1));
                $('#level_4_1').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
            }
            break;
        case 'sti_nodes_lvl2-2':
            node = NextParent(Nodelevel2_2_1, i2_2);
            if (node != undefined) {
                icancelled3_2 = 0; icancelled4_2 = 0;
                i3_2 = 1; i4_2 = 1;
                $.fn.ClearItems(32);
                $.fn.ClearItems(42);
                Nodelevel2_2_1 = node.id;
                SetChildren(Nodelevel2_2_1, "3", 32, true);

               
                //$.fn.AddCancelled(32, icancelled3_2);
                //$.fn.AddCancelled(42, icancelled4_2);

                $.fn.addNodeEmpty(32, calculeEmpty_l3(i3_2));
                $.fn.AddBackup(32, calculeBackup(i3_2));
                $('#level_3_2').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
                $.fn.addNodeEmpty(42, calculeEmpty_l3(i4_2));
                $.fn.AddBackup(42, calculeBackup(i4_2));
                $('#level_4_2').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
            }
            break;
        case 'sti_nodes_lvl3-1':
             break;
        case 'sti_nodes_lvl3-2':
            break;
        case 'sti_nodes_lvl4-1':
            break;
        case 'sti_nodes_lvl4-2':
            break;
    }
    
    $(".sti_lightbox").click(function (x) { rs_lightbox(x, $(this)); });
    rs_mouseHover();
    fn_validatesize();
}

function loadPlug() {
    $('#level_1').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
    $('#level_2_1').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
    $('#level_2_2').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
    $('#level_3_1').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
    $('#level_3_2').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
    $('#level_4_1').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
    $('#level_4_2').showbizpro({ dragAndScroll: "off", visibleElementsArray: [1, 1, 1, 4], carousel: "off", entrySizeOffset: 1, allEntryAtOnce: "off", mediaMaxHeight: [119, 119, 119, 119] });
}
