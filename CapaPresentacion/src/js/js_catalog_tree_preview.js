﻿
function fn_SelectCheck()
{
    var array_of_checked_values = $(".dllcategory").multiselect("getChecked").map(function () {
        return this.value;
    }).get();

   
    success = function (response) {
        $("#loading").hide();
        $("#message").show();
        $("#message").html("<span class='icon_acceptcheckconfirmedgogreenokpositiveyes'></span><p class='style_alert'>successfully!</p>");
        $("#message").fadeOut(3000);
    };
    error = function (xhr, ajaxOptions, thrownError) {
        fn_message("e", "Error Search.");
    };

    var datacall = '{ jsondata:"' + array_of_checked_values + '"}';
    fn_callmethod('/Private/products/CatalogsSave.aspx/GetProductsByCategories', datacall, success, error);
}

function fn_GetProductByCategoryID(CategoryID) {

    success = function (response) {
        //var pic1 = document.getElementById("HeaderRightPlaceHolder_ucLogin1_imgProfile");
        //if (pic1 == typeof ('undefined')) return;
        //pic1.src = "http://" + response.d;

        $("#loading").hide();
        $("#message").show();
        $("#message").html("<span class='icon_acceptcheckconfirmedgogreenokpositiveyes'></span><p class='style_alert'>successfully!</p>");
        $("#message").fadeOut(3000);
    };
    error = function (xhr, ajaxOptions, thrownError) {
        fn_message("e", "Error Search.");
    };

    var datacall = '{ CategoryID:"' + CategoryID + '"}';
    fn_callmethod('/Private/products/CatalogsSave.aspx', datacall, success, error);
}





function windowclose_TreeGenealogy() {
    $(".jOrgChart").fadeOut(500, function () { $(this).remove(); });
    $("#TreeGenealogy_CatalogPreview").hide(1000);

}

(function ($) {
    $.fn.extend({
        center: function () {
            return this.each(function () {
                var top = ($(window).height() - $(this).outerHeight()) / 2;
                var left = ($(window).width() - $(this).outerWidth()) / 2;
                $(this).css({ position: 'absolute', margin: 0, top: (top > 0 ? top : 0) + 'px', left: (left > 0 ? left : 0) + 'px' });
            });
        }
    });
})(jQuery);