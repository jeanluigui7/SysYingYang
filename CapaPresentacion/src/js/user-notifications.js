﻿$(function () {

    fn_init();
    $(".ui-jqgrid-view").children().removeClass("ui-widget-header ui-state-default");
    $(".ui-jqgrid-pager").removeClass("ui-state-default");
    jQuery(".ui-jqgrid-labels, .ui-search-toolbar").addClass('.jqgrid-header');
});
function fn_init() {
    fn_plugin();
    fn_content();
    fn_TotalMsgsInbox();
    fn_TotalMsgsSent();
    fn_TotalMsgsTrash();
}
function fn_plugin() {
    //fn_buildtable();
    
}
function fn_content() {
    fn_loadInbox('To');
    fn_SeeMessage();
}
function fn_SeeMessage()
{
   
    if (notificationQuery)
    {
        fn_GetMessageById(notificationQuery);
        //fn_callmethod("Notifications.aspx/GetInbox", senddata, success, error);
    }
    
}
function fn_loadInbox(x) {

    switch (x) {
        case 'To':
            $("#titleNotification").text("Inbox");
            //$("#jqgh_tbGrid_from").text("From");
            $("#menuemail li a").each(function () {
                if ($(this).hasClass('active'))
                    $(this).removeClass('active');
                $("#inboxs").addClass('active');

            });
            //$("#tbGrid").jqGrid('setLabel', 3, x); 
            break;
        case 'From':
            $("#titleNotification").text("Sent");
            //$("#tbGrid").jqGrid('setLabel', 3, x); break;
            //$("#jqgh_tbGrid_from").text("To");
            //$("ul li a").each(function () {
            //    if ($(this).hasClass('active_li'))
            //        $(this).removeClass('active_li');
            //    $(".i_sent a").addClass('active_li');

            //});
            $("#menuemail li a").each(function () {
                if ($(this).hasClass('active'))
                    $(this).removeClass('active');
                $("#sents").addClass('active');

            });
            //$("#tbGrid").jqGrid('setLabel', 3, x); 
            break;
        case 'Trash':
            $("#titleNotification").text("Trash");
            //$("#jqgh_tbGrid_from").text("Trash");
            //$("ul li a").each(function () {
            //    if ($(this).hasClass('active_li'))
            //        $(this).removeClass('active_li');
            //    $(".i_trash a").addClass('active_li');

            //});
            $("#menuemail li a").each(function () {
                if ($(this).hasClass('active'))
                    $(this).removeClass('active');
                $("#trashs").addClass('active');

            });
            break;
    }
    $('.email_loading').css({ 'display': 'block' });
    //$("#tbGrid").jqGrid("clearGridData", true).trigger("reloadGrid");
    var senddata = '{ tp:"' + x + '" }';
    var success = function (asw) {
        $("#tbGrid").empty();
        if (asw.d != "") {
            //var values = [];
            //var obj = JSON.stringify(asw);
            var obj = $.parseJSON(asw.d);
            var len = obj.length;
            console.log(len);
            
            
            //for (var i = 0; i < len; i++) {
            //    values.push(obj[i].Email);
            //}
            //$('.tags').tagbox({
            //    url: values
            //});
            try {
                for (var i = 0; i < len; i++) {
                    var index = i + 1;

                    switch (x) {
                        case 'To':
                           // if (obj[i].SenderName != "KIS Kapital (5000)" && obj[i].Xapp== 2) {
                                straction = "<button type='button' class='btn btn-primary'  title='response' onclick='fn_rowedit(" + index + ")' ><i class='fa fa-reply'></i></button> <button type='button'class='btn btn-primary' title='view' onclick='fn_rowview(" + index + ")' ><i class='fa fa-search'></i></button>";
                            //}
                            //else {
                            //    straction = "<button type='button' class='btn btn-primary'  title='view' onclick='fn_rowview(" + index + ")' ><i class='fa fa-search'></i></button>";
                            //}
                            var row = {
                                ID: obj[i].ID,
                                hidden: obj[i].SenderID,
                                ID: obj[i].ID,
                                row: index,
                                from: obj[i].SenderName,
                                subject: obj[i].Subject,
                                email: obj[i].Mail,
                                date: obj[i].CreatedDate,
                                action: straction
                            };
                            //if ($("ul li a").hasClass('active_li'))
                            //  alert("true");
                            break;
                        case 'From':
                            straction = "<button type='button' class='btn btn-primary' title='response' onclick='fn_rowedit(" + index + ")' ><i class='fa fa-reply'></i></button> <button type='button' class='btn btn-primary' title='view' onclick='fn_rowview(" + index + ")' ><i class='fa fa-search'></i></button>";
                            var row = {
                                ID: obj[i].ID,
                                hidden: obj[i].ReceiverID,
                                ID: obj[i].ID,
                                row: index,
                                from: obj[i].ReceiverName,
                                subject: obj[i].Subject,
                                email: obj[i].Mail,
                                date: obj[i].CreatedDate,
                                action: straction
                            };

                            break;

                        case 'Trash':
                            straction = "<button type='button' class='btn btn-primary' title='view' onclick='fn_rowview(" + index + ")' ><i class='fa fa-search'></i></button>";
                            //straction = "<input type='button' class='return-button' title='response' onclick='fn_rowedit(" + index + ")' /> <input type='button' class='view-button' title='view' onclick='fn_rowview(" + index + ")' />";
                            var row = {
                                ID: obj[i].ID,
                                hidden: obj[i].SenderID,
                                ID: obj[i].ID,
                                row: index,
                                from: obj[i].SenderName,
                                subject: obj[i].Subject,
                                email: obj[i].Mail,
                                date: obj[i].CreatedDate,
                                action: straction
                            };

                            break;
                    }

                    fn_buildhtmlNotification(row);
                    //jQuery("#tbGrid").jqGrid('addRowData', i + 1, row);
                   // load_Dialog_EmailView();
                }
               // $("#tbGrid").trigger("reloadGrid");
                $('.email_loading').css({ 'display': 'none' });
            }
            catch (e) {
                fn_message('e', 'An error occurred while loading data...');
            }
        }
        else
            $('.email_loading').css({ 'display': 'none' });
    };
    var error = function (xhr, ajaxOptions, thrownError) {
        $('.email_loading').css({ 'display': 'none' });
        fn_message('e', 'An error occurred while sending data');
    };

    fn_callmethod("Notifications.aspx/GetInbox", senddata, success, error);
}
function fn_buildhtmlNotification(obj) {
    var html = "";
    html = '<li class="unread" data-id="row_' + obj.ID + '">' +
                '<div class="checkbox-custom checkbox-text-primary ib">' +
							        '<input type="checkbox" id="' + obj.ID + '" data-id="' + obj.ID + '" data-sender="' + obj.hidden + '">' +
							        '<label for="' + obj.ID + '"></label>' +
						        '</div>' +
				'<a href="javascript:void(0);" onclick="fn_rowview(' + obj.ID + ');">' +
					//'<i class="mail-label" style="border-color: #EA4C89"></i>'+
							
					'<div class="col-sender">'+
						
						'<p class="m-none ib">' + obj.from + '</p>' +
					'</div>'+
					'<div class="col-mail">'+
						'<p class="m-none mail-content">'+
							'<span class="subject">' + obj.subject + '&nbsp;–&nbsp;</span>' +
							'<span class="mail-partial">' + obj.email + '</span>' +
						'</p>'+
						//'<i class="mail-attachment fa fa-paperclip"></i>'+
						'<p class="m-none mail-date">' + obj.date + '</p>' +
					'</div>'+
				'</a>'+
			'</li>';
    $("#tbGrid").append(html);
}
function fn_TotalMsgsInbox() {  
    var success = function (response) {      
            $("#MsgsInbox").text(response.d);
    };
        var error = function (xhr, ajaxOptions, thrownError) {
            fn_message('e', 'An error occurred while sending data');
        };
        var senddata = '{}';
        fn_callmethod("Notifications.aspx/TotalMsgsInbox", senddata, success, error);
}
function fn_TotalMsgsSent() {
    var success = function (response) {
        $("#MsgsSent").text(response.d);
    };
    var error = function (xhr, ajaxOptions, thrownError) {
        fn_message('e', 'An error occurred while sending data');
    };
    var senddata = '{}';
    fn_callmethod("Notifications.aspx/TotalMsgsSent", senddata, success, error);
}
function fn_TotalMsgsTrash() {

    var success = function (response) {

        $("#MsgsTrash").text(response.d);

    };
    var error = function (xhr, ajaxOptions, thrownError) {
        fn_message('e', 'An error occurred while sending data');
    };
    var senddata = '{}';
    fn_callmethod("Notifications.aspx/TotalMsgsTrash", senddata, success, error);

}
function htmlDecode(value) {
    if (value) {
        return $('<div />').html(value).text();
    } else {
        return '';
    }
}

function fn_buildtable() {
    try {
        $("#tbGrid").jqGrid({
            datatype: 'local',
            colNames: ['ID', '', '#', 'From', 'Subject', '', 'Date', ''],
            colModel: [
                { name: 'ID', index: 'ID', hidden: true },
                { name: 'hidden', index: 'hidden', hidden: true },
                { name: 'row', index: 'row', width: 20, align: 'right', sorttype: 'int' },
                { name: 'from', index: 'from', width: 100, align: 'left', sorttype: 'text'},
                { name: 'subject', index: 'subject', width: 100, align: 'left', sorttype: 'text' },
                { name: 'email', index: 'email', width: 50, align: 'left', sorttype: 'text', hidden: true },
                { name: 'date', index: 'date', width: 100, align: 'center', sorttype: 'date' },
                { name: 'action', index: 'action', width: 70, align: 'center' }
            ],

            width: 650,
            hoverrows: true,
            pager: '#tbGridPager',
            viewrecords: true,
            height: 'auto',
            sortorder: 'desc',
            rowNum: 20,
            rowList: [20, 40, 60],
            //caption: "View: All",

            multiselect: true
        });
        //jQuery("#tbGrid").jqGrid('filterToolbar', { searchOperators: true });
    } catch (e) {
        fn_message('e', 'An error occurred while loading grid...');
    }
}
//function fn_deleteitems() {
//    var Case = $("#jqgh_tbGrid_from").text();
//    var obj = { ID: "" };
//    var lista = [];

//    var selIndex = $("#tbGrid").getGridParam('selarrrow');
//    var len = selIndex.length;
//    var selIndex2 = $.extend(true, {}, selIndex);
//    if (selIndex.length > 0) {
//        for (var i = 0; i < selIndex.length; i++) {
//            var data = $('#tbGrid').getRowData(selIndex[i]);
//            obj.ID = data.ID;
//            lista[i] = $.extend(true, {}, obj);
//        }
//        var senddata;
//        var json = JSON.stringify(lista);
//        if (Case == "Trash") {
//            senddata = '{jsondata:"' + fn_jsonreplace(json) + '" }';
//        } else
//            senddata = '{  Category:"' + "1" + '",  jsondata:"' + fn_jsonreplace(json) + '" }';
//        var success = function (asw) {
//            if (asw.d == "Deleted successfully") {
//                fn_message('s', 'Deleted successfully');
//                for (var y = 0; y < len; y++) {
//                    $("#tbGrid").delRowData(selIndex2[y]);
//                }
//            }
//            else
//                fn_message('e', 'Unable to delete the record(s)');
//        };
//        var error = function (xhr, ajaxOptions, thrownError) {
//            fn_message('e', 'An error occurred while sending data');
//        };
//        if (Case == "Trash") {
//            fn_callmethod("Notifications.aspx/DeleteEmail", senddata, success, error);
//        } else
//            fn_callmethod("Notifications.aspx/DeleteInternalEmail", senddata, success, error);
//    }
//    else
//        fn_message('i', "Please select at least one row to delete");
//}
function fn_updateitems() {
    $("#menuemail li a").each(function () {
        if ($(this).hasClass('active'))
            $(this).click();          
    });
    //var mydata = $("#tbGrid").jqGrid('getGridParam', 'data');
    //$("#tbGrid").jqGrid("clearGridData", true).trigger("reloadGrid");
    //$('.email_loading').css({ 'display': 'block' });
    //setTimeout(function () {
    //    $("#tbGrid").jqGrid('setGridParam', { datatype: 'local', data: mydata }).trigger("reloadGrid");
    //    $('.email_loading').hide();
    //}, 1000);

    // $('.email_loading').css({ 'display': 'none' });
}