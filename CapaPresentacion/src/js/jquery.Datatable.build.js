﻿(function ($) {
	$.fn.buildDatatable = function (settings) {		
		settings = jQuery.extend({			
			colNames: [],
			colModel: [],
			sortorder: "asc",
			multiselect: false			
		}, settings);
		$.buildDatatable.settings = settings;		
		$.buildDatatable.buildTable(settings.colModel, settings.multiselect);
		$.buildDatatable.fillTable(settings.colNames, settings.sortorder);
	};
	$.buildDatatable = {
		buildTable: function (colModel, multiselect) {
			if ($.buildTable.colModel.length == 0 && !$.buildTable.multiselect)
				$.buildTable.debug('e',"An error occurred while loading data...");

		},
		debug: function (type, error) {
			fn_message(type, error);
		},
		
	};
})(jQuery);