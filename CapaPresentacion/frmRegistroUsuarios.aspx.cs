﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaNegocio;
using CapaEntidades;
using System.Data;
using Library;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CapaPresentacion
{
    public partial class frmRegistroUsuarios1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }
        private void LoadData()
        {
            object objPersona = new object();
            List<entUsuario> list = new List<entUsuario>();
            DataTable dt = negUsuarios.Instancia.ListaUsuarios(1);
            if (dt != null && dt.Rows.Count > 0)
            {
                Int32 count = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    count++;
                    String sId = Server.UrlEncode(clsEncryption.Encrypt(Utility.ValidateDataRowKeyDefault(dr, "ID", "")));
                    list.Add(new entUsuario()
                    {
                        isCheckbox = "1",
                        UsuId = sId,
                        UsuCodigo = Utility.ValidateDataRowKeyDefault(dr, "UsuCodigo", ""),
                        PerId = Utility.ValidateDataRowKeyDefault(dr, "PerId", ""),
                        CargoId = Utility.ValidateDataRowKeyDefault(dr, "CargoDescripcion", ""),
                        PerNombre = Utility.ValidateDataRowKeyDefault(dr, "PerNombre", ""),
                        Usuario = Utility.ValidateDataRowKeyDefault(dr, "Usuario", ""),
                        Password = Utility.ValidateDataRowKeyDefault(dr, "Password", ""),
                        UsuEstado = Utility.ValidateDataRowKeyDefault(dr, "UsuEstado", ""),
                        Index = count.ToString(),

                    });
                }
                if (list != null)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string json = serializer.Serialize(list);
                    hfData.Value = json.ToString();
                }
            }
        }

       

    }
}