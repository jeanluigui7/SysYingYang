﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrincipalShop.Master" AutoEventWireup="true" CodeBehind="ProccessAddress.aspx.cs" Inherits="CapaPresentacion.ProccessAddress" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="Estilos/js/jquery-1.11.1.min.js"></script>
    <script src="Estilos/js/Base.js"></script>
    <script type="text/javascript">
        var objCliente = new Object();
        var objDirecciones = new Object();
        $(function () {

            fn_Init();
            fn_Bind();
        });

        function fn_Init() {
            $("#DivLateralIzq").hide();
            $("#RowContentProducts").removeClass();
            $("#DivBox").removeClass();
        }

        function fn_Bind() {

            $("#<%=cboDepartamento.ClientID%>").on("change", function () {

                var success = function (asw) {

                    if (asw != null) {

                        if (asw.d.Msg == "Saved Successfully") {
                            LoadProvincias(asw.d.consultaPro);
                            LoadDistritos(asw.d.consultaDist);
                        }
                    }
                };

                var error = function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                };

                var DepId = $("#<%=cboDepartamento.ClientID%>").val();
                fn_callmethod("frmRegistrarClientes.aspx/SeleccionarDepartamento", '{ DepId: "' + DepId + '"}', success, error);
            });


            $("#<%=cboProvincia.ClientID%>").on("change", function () {
                var success = function (asw) {
                    if (asw != null) {
                        if (asw.d.Msg == "Saved Successfully") {
                            DistritoXProvincia(asw.d.consultaDistXPro);
                        }
                    }
                };


                var error = function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                }

                var ProvId = $("#<%=cboProvincia.ClientID%>").val();
                fn_callmethod("frmRegistrarClientes.aspx/SeleccionarProvincia", '{ ProvId: "' + ProvId + '"}', success, error);
            });
        }

        
        function DistritoXProvincia(dataDistxPro) {
            $("#<%=cboDistrito.ClientID%>").empty();
            $.each(dataDistxPro, function () {
                $("#<%=cboDistrito.ClientID%>").append($("<option></option>").attr("value", this.DistId).text(this.DistNombre))
            });
        }

        function LoadProvincias(dataProv) {
            $("#<%=cboProvincia.ClientID%>").empty();
            $.each(dataProv, function () {
                $("#<%=cboProvincia.ClientID%>").append($("<option></option>").attr("value", this.ProvId).text(this.ProvNombre))
            });
        }
        function LoadDistritos(dataDist) {

            $("#<%=cboDistrito.ClientID%>").empty();
            $.each(dataDist, function () {
                $("#<%=cboDistrito.ClientID%>").append($("<option></option>").attr("value", this.DistId).text(this.DistNombre))
            });
        }



        function fn_Redirect() {
            window.location.href = "DeliveryMethod.aspx";
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div id="content">
            <div class="container">

<%--           <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="#">Home</a>
                        </li>
                        <li>Checkout - Address</li>
                    </ul>
                </div>--%>

                <div class="col-md-9" id="checkout">

                    <div class="box">
                        <form method="post" action="checkout2.html">
                            <h1>Checkout</h1>
                            <ul class="nav nav-pills nav-justified">
                                <li class="active"><a href="#"><i class="fa fa-map-marker"></i><br>Address</a>
                                </li>
                                <li class="disabled"><a href="#"><i class="fa fa-truck"></i><br>Delivery Method</a>
                                </li>
                                <li class="disabled"><a href="#"><i class="fa fa-money"></i><br>Payment Method</a>
                                </li>
                                <li class="disabled"><a href="#"><i class="fa fa-eye"></i><br>Order Review</a>
                                </li>
                            </ul>

                            <div class="content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="firstname">Nombre</label>
                                            <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="lastname">Apellido Materno</label>
                                              <asp:TextBox ID="txtApellidoM" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="company">Apellido Paterno</label>
                                             <asp:TextBox ID="txtApellidoP" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="street">Celular</label>
                                             <asp:TextBox ID="txtCelular" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->

                                 <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="company">Direccion Principal</label>
                                              <asp:TextBox ID="txtDireccionPrincipal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                   <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="company">Direccion Referencial</label>
                                             <asp:TextBox ID="txtDireccionReferencial" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                </div>

                                <!-- /.row -->

                                <div class="row">
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="city">Codigo Postal</label>
                                            <asp:TextBox ID="txtCodigoPos" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="zip">Departamento</label>
                                            <asp:DropDownList runat="server" CssClass="form-control" ID="cboDepartamento"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="state">Provincia</label>
                                            <asp:DropDownList runat="server" CssClass="form-control" ID="cboProvincia"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="country">Distrito</label>
                                          <asp:DropDownList runat="server" CssClass="form-control" ID="cboDistrito"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                             <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.row -->
                            </div>

                            <div class="box-footer">
                                <div class="pull-left">
                                    <a href="frmShopCard.aspx" class="btn btn-default"><i class="fa fa-chevron-left"></i>Back to basket</a>
                                </div>
                                <div class="pull-right">
                                    <button type="button" class="btn btn-primary" onclick="fn_Redirect();">Continue to Delivery Method<i class="fa fa-chevron-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->


                </div>
                <!-- /.col-md-9 -->

                <div class="col-md-3">

                    <div class="box" id="order-summary">
                        <div class="box-header">
                            <h3>Order summary</h3>
                        </div>
                        <p class="text-muted">Shipping and additional costs are calculated based on the values you have entered.</p>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>Order subtotal</td>
                                        <th id="lblSubtotal" runat="server">S/.0.00</th>
                                    </tr>
                                    <tr>
                                        <td>Shipping and handling</td>
                                        <th>S/.0.00</th>
                                    </tr>
                                    <tr>
                                        <td>Tax</td>
                                        <th>S/.0.00</th>
                                    </tr>
                                    <tr class="total">
                                        <td>Total</td>
                                        <th id="lblTotalResumen" runat="server">S/.0.00</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
                <!-- /.col-md-3 -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
</asp:Content>
