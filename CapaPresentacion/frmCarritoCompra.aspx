﻿<%@ Page ValidateRequest="false" EnableEventValidation="false"  Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="frmCarritoCompra.aspx.cs" Inherits="CapaPresentacion.frmCarritoCompra" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:GridView ID="gvCarrito" runat="server" OnRowDeleting="gvCarrito_RowDeleting" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical" HorizontalAlign="Center" Width="100%" OnRowEditing="gvCarrito_RowEditing">
        <AlternatingRowStyle BackColor="Gainsboro" />
        <Columns>
            <asp:BoundField DataField="Numero" HeaderText="N°">
            <ItemStyle Font-Size="10pt" />
            </asp:BoundField>
            <asp:BoundField DataField="idproducto" HeaderText="idproducto">
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Font-Size="10pt" />
            </asp:BoundField>
            <asp:BoundField DataField="codigo" HeaderText="Codigo">
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Font-Size="10pt" />
            </asp:BoundField>
            <asp:BoundField DataField="dproducto" HeaderText="Producto">
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Font-Size="10pt" />
            </asp:BoundField>
        
            <asp:TemplateField HeaderText="Cant." ValidateRequestMode="Disabled">
                <ItemTemplate>
                    <asp:TextBox ID="txtCant" runat="server" Text='<%# Eval("cantidad") %>'></asp:TextBox>
                </ItemTemplate>
                <HeaderStyle Font-Size="10pt" />
            </asp:TemplateField>
            <asp:BoundField DataField="precio" HeaderText="Precio">
            </asp:BoundField>
            <asp:BoundField DataField="subtotal" HeaderText="Importe">
            </asp:BoundField>
            <asp:ImageField DataImageUrlField="imagen" HeaderText="Imagen">
                <ControlStyle Height="35px" Width="35px" />
                <ItemStyle Height="20px" Width="20px" />
            </asp:ImageField>
            <asp:CommandField ButtonType="Image" DeleteImageUrl="~/Fotos/descarga (1).jpg" HeaderText="Imagen" ShowDeleteButton="True" >
            <ControlStyle Height="20px" Width="20px" />
            <ItemStyle HorizontalAlign="Center" />
            </asp:CommandField>
            <asp:CommandField ShowEditButton="True" ButtonType="Button" />
        </Columns>
        <EmptyDataRowStyle Font-Italic="False" />
        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
        <HeaderStyle BackColor="#000084" Font-Bold="True" Font-Size="10pt" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Top" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#0000A9" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#000065" />
    </asp:GridView>

    <br />
    <%
    if (Session["Pedido"] != null)
    {
    %>
    <asp:Button ID="btnGuardar" runat="server" Text="Guardar Pedido" OnClick="btnGuardar_Click" />
    <%
    }
    %>
  <%
    else
    {
    %>
    <h5>Si desea realizar comprar algun Producto de <a href="frmListaProductos.aspx" style="font-family:Arial; font-size:medium; color:blue">Clic aqui</a>.</h5>
    <%
    }
    %>
    <asp:Label ID="lblMensaje" runat ="server" Text="" ForeColor="Red"></asp:Label>

</asp:Content>
