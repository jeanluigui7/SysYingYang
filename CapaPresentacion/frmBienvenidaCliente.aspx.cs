﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaEntidades;
using CapaNegocio;
namespace CapaPresentacion
{
    public partial class frmBienvenidaCliente : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                entCliente c = (entCliente)Session["Cliente"];
                if (c != null)
                {
                    lblNombre.Text = c.CliNombre;
                    lblApellidoP.Text = c.CliApellidoP;
                    lblApellidoM.Text = c.CliApellidoM;
                    lblEmail.Text = c.CliEmail;
                    lblCelular.Text = c.CliCelular;
                    imgFoto.ImageUrl = "~/Fotos/" + c.CliFoto;
                }
                else
                {
                    Response.Redirect("Index.aspx");
                }
            
            }
        }
    }
}