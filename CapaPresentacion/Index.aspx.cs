﻿using CapaEntidades;
using CapaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaEntidades;
using CapaNegocio;
using System.Web.Services;
using Library;

namespace CapaPresentacion
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ListarProductos();
            }
        }

        private void ListarProductos()
        {
            try
            {
                JavaScriptSerializer sr = new JavaScriptSerializer();
                List<entProducto> lstPro = new List<entProducto>();
                
                lstPro = negProducto.Instancia.ListarProductos();

                if (lstPro != null && lstPro.Count > 0)
                {
                    hfProductos.Value = sr.Serialize(lstPro);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [WebMethod(EnableSession = true)]
        public static object GetProductId(string id)
        {
            entProducto item = new entProducto();
            List<entDetallePedido> DetalleP = new List<entDetallePedido>();
            entPedido Pedido = new entPedido();
            object lista = new object();
            try
            {
                JavaScriptSerializer sr = new JavaScriptSerializer();

                item = negProducto.Instancia.ListarProductosId(Convert.ToInt32(id));

                if (HttpContext.Current.Session["Pedido"] == null || HttpContext.Current.Session["SessionDetalle"] == null)
                {
                   HttpContext.Current.Session["Pedido"] = Pedido;
                   HttpContext.Current.Session["SessionDetalle"] = DetalleP;

                    Utility.AgregarProductoDetalle(item, DetalleP);
                    Utility.AgregarPedido(Pedido, DetalleP);
                   
                }
                else {

                    DetalleP = (List<entDetallePedido>)HttpContext.Current.Session["SessionDetalle"];
                    var proExist = (from p in DetalleP
                              where item.ProId == p.ProId
                              select p).Any();

                    if (proExist == true)
                    {
                        return new { rpta = "ProdExist" };
                    }

                    Utility.AgregarProductoDetalle(item, DetalleP);
                    Utility.AgregarPedido(Pedido, DetalleP);

                }

                if (item != null)
                {
                    lista = new {
                        //itemProducto = sr.Serialize(item),
                        rpta = "Exist"
                    };
                }
                else
                {
                    lista = new
                    {
                        //itemProducto = "",
                        rpta = "NoExist"
                    };
                }
            }
            catch (Exception ex)
            {
                lista = new
                {
                    //itemProducto = "",
                    rpta = "NoExist"
                };
            }
            return lista;
        }

       

       
    }
}