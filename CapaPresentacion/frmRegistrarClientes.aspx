﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrincipalShop.Master" AutoEventWireup="true" CodeBehind="frmRegistrarClientes.aspx.cs" Inherits="CapaPresentacion.frmRegistrarClientes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script src="src/js/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="Estilos/js/Base.js"></script>
    <script type="text/javascript">

        var objCliente = new Object();
        var objDirecciones = new Object();

        $(function () {
            fn_init();
        });

        function fn_init() {
            fn_bind();
            fn_Plugin();
        }
        function fn_Plugin() {
            $("#FormCliente").validationEngine();
            //$("#FormPassword").validationEngine();
        }
        function fn_bind() {

            $("#<%=cboDepartamento.ClientID%>").on("change", function () {

                var success = function (asw) {

                    if (asw != null) {

                        if (asw.d.Msg == "Saved Successfully") {
                            LoadProvincias(asw.d.consultaPro);
                            LoadDistritos(asw.d.consultaDist);
                        }
                    }
                };

                var error = function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                };

                var DepId = $("#<%=cboDepartamento.ClientID%>").val();
                fn_callmethod("frmRegistrarClientes.aspx/SeleccionarDepartamento", '{ DepId: "' + DepId + '"}', success, error);
            });


            $("#<%=cboProvincia.ClientID%>").on("change", function () {
                var success = function (asw) {
                    if (asw != null) {
                        if (asw.d.Msg == "Saved Successfully") {
                            DistritoXProvincia(asw.d.consultaDistXPro);
                        }
                    }
                };


                var error = function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                }

                var ProvId = $("#<%=cboProvincia.ClientID%>").val();
                fn_callmethod("frmRegistrarClientes.aspx/SeleccionarProvincia", '{ ProvId: "' + ProvId + '"}', success, error);
            });
        }

        function DistritoXProvincia(dataDistxPro) {
            $("#<%=cboDistrito.ClientID%>").empty();
            $.each(dataDistxPro, function () {
                $("#<%=cboDistrito.ClientID%>").append($("<option></option>").attr("value", this.DistId).text(this.DistNombre))
            });
        }

        function LoadProvincias(dataProv) {
            $("#<%=cboProvincia.ClientID%>").empty();
            $.each(dataProv, function () {
                $("#<%=cboProvincia.ClientID%>").append($("<option></option>").attr("value", this.ProvId).text(this.ProvNombre))
            });
        }
        function LoadDistritos(dataDist) {

            $("#<%=cboDistrito.ClientID%>").empty();
            $.each(dataDist, function () {
                $("#<%=cboDistrito.ClientID%>").append($("<option></option>").attr("value", this.DistId).text(this.DistNombre))
            });
        }

        function fn_SavePassword() {
            //if (fn_validateform('FormPassword') == false){
            //    return;
            //}
            if ($("input[id$=txtPasswordAntiguo]").val().replace(/^\s+|\s+$/g, "").length == 0) {
            fn_message('i', 'Debe Contraseña Antiguo', 'message_row');
            return;
          }
            if ($("input[id$=txtNuevoPassword]").val().replace(/^\s+|\s+$/g, "").length == 0) {
            fn_message('i', 'Debe ingresar Nueva contraseña', 'message_row');
            return;
          }
            if ($("input[id$=txtConfirmaPassword]").val().replace(/^\s+|\s+$/g, "").length == 0) {
            fn_message('i', 'Debe confirmar Nueva contraseña', 'message_row');
            return;
          }
        }

        function fn_GuardarCliente() {

            if ($("#<%=hfNameFoto.ClientID%>").val() == "") {
                fn_message('i', 'Debe seleccionar y cargar una Foto', 'message_row');
                return;
            }
            if ($("input[id$=txtDireccion1]").val().replace(/^\s+|\s+$/g, "").length == 0) {
                fn_message('i', 'Debe ingresar la Direccion 1', 'message_row');
                return;
            }
            if ($("input[id$=txtDireccion2]").val().replace(/^\s+|\s+$/g, "").length == 0) {
                fn_message('i', 'Debe ingresar la Direccion 2', 'message_row');
                return;
            }
            if ($("input[id$=txtCodPostal]").val().replace(/^\s+|\s+$/g, "").length == 0) {
                fn_message('i', 'Debe ingresar Codigo Postal', 'message_row');
                return;
            }
            if ($("input[id$=txtCelular]").val().replace(/^\s+|\s+$/g, "").length == 0) {
                fn_message('i', 'Debe Ingresar Celular', 'message_row');
                return;
            }
            if ($("input[id$=txtTelefono]").val().replace(/^\s+|\s+$/g, "").length == 0) {
                fn_message('i', 'Debe Ingresar Telefono', 'message_row');
                return;
            }
            if ($("input[id$=txtEmail]").val().replace(/^\s+|\s+$/g, "").length == 0) {
                fn_message('i', 'Debe Ingresar Email', 'message_row');
                return;
            }
            if ($("#ContentPlaceHolder1_txtPassword").val().replace(/^\s+|\s+$/g, "").length == 0) {
                fn_message('i', 'Debe ingresar Password', 'message_row');
                return;
            }
            if (fn_validateform('FormCliente') == false) {
                return;
            }

            objCliente = {

                PaisId: $("#<%=cboPais.ClientID%>").val(),
                DepId: $("#<%=cboDepartamento.ClientID%>").val(),
                ProvId: $("#<%=cboProvincia.ClientID%>").val(),
                DistId: $("#<%=cboDistrito.ClientID%>").val(),
                CliNombre: $("#<%=txtNombre.ClientID%>").val(),
                CliApellidoP: $("#<%=txtApellidoP.ClientID%>").val(),
                CliApellidoM: $("#<%=txtApellidoM.ClientID%>").val(),
                CliTelefono: $("#<%=txtTelefono.ClientID%>").val(),
                CliCelular: $("#<%=txtCelular.ClientID%>").val(),
                CliEmail: $("#<%=txtEmail.ClientID%>").val(),
                CliPassword: $("#<%=txtPassword.ClientID%>").val(),
                CliFoto: $("#<%=hfNameFoto.ClientID%>").val(),
                CliUsuarioRegistro: $("#<%=txtNombre.ClientID%>").val(),
                CliUsuarioModificacion: "",
                CliEstado: 1,

                DirDescripcion1: $("#<%=txtDireccion1.ClientID%>").val(),
                DirDescripcion2: $("#<%=txtDireccion2.ClientID%>").val(),
                DirCodPostal: $("#<%=txtCodPostal.ClientID%>").val(),
                DirEstado: 1

            }
            var success = function (asw) {
                if (asw != null) {
                    if (asw.d.Msg == "Saved Successfully") {
                        fn_message("s", "Saved Successfully");
                    }
                }
            }

            var error = function (xhr, ajaxOptions, thrownError) {
                fn_message('e', 'An error occurred while sending data');
            };

            var dataCliente = { c: objCliente };

            fn_callmethod("frmRegistrarClientes.aspx/GuardarCliente", JSON.stringify(dataCliente), success, error);
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Mi Cuenta</h1>
    <h6 class="lead">Cambia tus datos personales o tu contraseña Aqui.</h6>

    <h3>Cambias Contraseña</h3>
    <div id="FormPassword">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Contraseña Antigua:</label>
                  <asp:TextBox ID="txtPasswordAntiguo" runat="server" TextMode="Password"  class="form-control"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Nueva Contraseña:</label>
                <asp:TextBox ID="txtNuevoPassword" runat="server" TextMode="Password"  class="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Confirme nueva Contraseña:</label>
                <asp:TextBox ID="txtConfirmaPassword" TextMode="Password" runat="server" class="form-control"></asp:TextBox>
            </div>
        </div>
    </div>
    </div>
    <!-- /.row -->
    <div class="row">
         <div class="col-sm-12 text-center">
             <button type="button" runat="server" class="btn btn-primary" onclick="fn_SavePassword();"><i class="fa fa-save"></i>Guardar Contraseña</button>
         </div>
    </div>
    <hr>

    <h3>Datos Personales</h3>
    <div id="FormCliente">    
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Direccion 1:</label>
                <asp:TextBox ID="txtDireccion1" runat="server" class="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Direccion 2:</label>
                <asp:TextBox ID="txtDireccion2" runat="server" class="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Codigo Postal:</label>
                <asp:TextBox ID="txtCodPostal" runat="server" class="form-control"></asp:TextBox>
            </div>
        
       </div>
    </div>
    

    <div class="row">
        <div class="col-sm-6 col-md-3">
            <div class="form-group">
                <label for="">Pais:</label>
                 <asp:DropDownList runat="server" class="form-control " ID="cboPais"></asp:DropDownList>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="form-group">
                <label for="">Departamento:</label>
               <asp:DropDownList runat="server" class="form-control " ID="cboDepartamento"></asp:DropDownList>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="form-group">
                <label for="">Provincia:</label>
                <asp:DropDownList runat="server" class="form-control " ID="cboProvincia"></asp:DropDownList>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="form-group">
                <label for="">Distrito:</label>
                <asp:DropDownList runat="server" class="form-control " ID="cboDistrito"></asp:DropDownList>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Nombre:</label>
                <asp:TextBox ID="txtNombre" runat="server" class="form-control validate[custom[onlyLetterandothers],html]"></asp:TextBox>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Apellido Paterno:</label>
                <asp:TextBox ID="txtApellidoP" runat="server" class="form-control validate[custom[onlyLetterandothers],html]"></asp:TextBox>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Apeliido Materno:</label>
                 <asp:TextBox ID="txtApellidoM" runat="server" class="form-control validate[custom[onlyLetterandothers],html]"></asp:TextBox>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Telefono:</label>
                 <asp:TextBox ID="txtTelefono" runat="server" CssClass="form-control validate[custom[onlyNumberSp]]" TextMode="Number"></asp:TextBox>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Celular:</label>
                <asp:TextBox ID="txtCelular" runat="server" CssClass="form-control validate[custom[onlyNumberSp]]" TextMode="Number"></asp:TextBox>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Email:</label>
                 <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
            </div>
        </div>      
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Password:</label>
                  <asp:TextBox ID="txtPassword" runat="server" class="form-control" TextMode="Password"></asp:TextBox>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Foto:</label>
                <asp:Image ID="ImgFoto" ClientIDMode="Static" runat="server" Width="130px" Height="130px"></asp:Image>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
               <asp:FileUpload ID="FU1" runat="server" />
              <asp:Button ID="btnUpload" runat="server" Text="Cargar Imagen" OnClick="btnUpload_Click" />
            </div>
        </div>

        <div class="col-sm-12 text-center">
             <button type="button" runat="server" class="btn btn-primary" onclick="fn_GuardarCliente()"><i class="fa fa-save"></i>Guardar Datos</button>
        </div>
     </div>
   </div>
    <!-- /.row -->

  <asp:HiddenField runat="server" ID="hfNameFoto" />
</asp:Content>
