﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="frmBienvenidaCliente.aspx.cs" Inherits="CapaPresentacion.frmBienvenidaCliente" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h2>Datos de Cliente</h2>
    <br />
    <table>
        <tr>
            <td>Nombre :</td>
            <td><asp:Label ID="lblNombre" Text="----------------" runat="server" /></td>
        </tr>
        <tr>
            <td>Apellido Paterno :</td>
            <td><asp:Label ID="lblApellidoP" Text="----------------"  runat="server" /></td>
        </tr>
        <tr>
            <td>Apellido Materno :</td>
            <td><asp:Label ID="lblApellidoM" Text="----------------"  runat="server" /></td>
        </tr>
        <tr>
            <td>Celular :</td>
            <td><asp:Label ID="lblCelular" Text="----------------"  runat="server" /></td>
        </tr>
        <tr>
            <td>Email :</td>
            <td><asp:Label ID="lblEmail" Text="----------------"  runat="server" /></td>
        </tr>
        <tr>
            <td>Foto :</td>
            <td><asp:Image ID="imgFoto" runat="server" Width="90px" Height="90px" /></td>
        </tr>
    </table>

    <br />
    <h5>Si desea modificar sus datos, haga clic en <a href="#">Editar</a>.</h5>
    <br />
    <asp:LinkButton ID="lnkCerrarSession" Text="text" runat="server">Cerrar Sesion</asp:LinkButton>

</asp:Content>
