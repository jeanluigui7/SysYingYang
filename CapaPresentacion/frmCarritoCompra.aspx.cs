﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaEntidades;
using CapaNegocio;
namespace CapaPresentacion
{
    public partial class frmCarritoCompra : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["Pedido"] != null)
                {
                    DataTable dt = (DataTable)Session["Pedido"];
                    gvCarrito.DataSource = dt;
                    gvCarrito.DataBind();

                    //for (int i = 0; i < dt.Rows.Count; i++)
                    //{
                    //    DataRow dr = dt.Rows[i];
                    //    System.Convert.ToInt16(((TextBox)this.gvCarrito.Rows[i].Cells[6].FindControl("txtCant")).Text = dr["cantidad"].ToString());

                    //}
                }
                else
                {
                    gvCarrito.DataSource = null;
                    gvCarrito.DataBind();
                    lblMensaje.Text = ".::.USTED NO TIENE PRODUCTOS EN EL CARRITO.::.";
                }
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                entPedido p = new entPedido();
                DataTable dt = (DataTable)Session["PedidoSQL"];
                entCliente c = (entCliente)Session["Cliente"];
                p.CliId = c.CliId;
                Decimal Total = Convert.ToDecimal(Session["Total"]);
                p.Total = Total;

                if (dt.Rows.Count > 0 && p != null)
                {
                    int i = negPedido.Instancia.Insertar_Pedido(p, dt);
                    if (i > 0)
                    {
                        Session.Remove("Pedido");
                        Session.Remove("PedidoSQL");
                        Session.Remove("Total");
                        Session.Remove("CountProductos");
                        gvCarrito.DataSource = null;
                        gvCarrito.DataBind();
                        lblMensaje.Text = ".::.USTED NO TIENE PRODUCTOS EN EL CARRITO.::.";
                        //Response.Redirect("Index.aspx");
                        ScriptMessage(".::::. Su pedido fue Registrado Satisfactoriamente .::::.");

                    }
                }
            }
            catch (Exception ex)
            {
                Session.Remove("Pedido");
                Session.Remove("PedidoSQL");
                Session.Remove("Total");
                Session.Remove("CountProductos");
                ScriptMessage(".::::.Ocurrio un error al realizar el Pedido .::::.");
            }

            
        }

        protected void gvCarrito_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            TableCell cell = gvCarrito.Rows[e.RowIndex].Cells[1];
            int fila = Convert.ToInt32(cell.Text);

            DataTable dt = (DataTable)(Session)["Pedido"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                if (fila == Convert.ToInt32(dr["idproducto"].ToString()))
                {
                    Session["Total"] = Convert.ToDouble(Session["Total"]) - Convert.ToDouble(dr["SubTotal"]);
                    dt.Rows.RemoveAt(i);
                    Session["Pedido"] = dt;

                    gvCarrito.DataSource = dt;
                    gvCarrito.DataBind();
                    break;
                }
            }

            DataTable dtsql = (DataTable)Session["PedidoSQL"];

            for (int s = 0; s < dtsql.Rows.Count; s++)
            {
                DataRow drs = dtsql.Rows[s];
                if (fila == Convert.ToInt32(drs["ProId"].ToString()))
                {
                    dtsql.Rows.RemoveAt(s);
                    Session["PedidoSQL"] = dtsql;
                    break;
                }
            }
            Session["CountProductos"] = dt.Rows.Count;

            if (((DataTable)Session["PedidoSQL"]) == null || ((DataTable)Session["PedidoSQL"]).Rows.Count <= 0
                || ((DataTable)Session["Pedido"]) == null || ((DataTable)Session["Pedido"]).Rows.Count <= 0)
            {
                Session.Remove("Pedido");
                Session.Remove("PedidoSQL");
                Session.Remove("Total");
                Response.Redirect("Index.aspx");
            }
            
        }

        protected void gvCarrito_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
              
                //int i = e.NewEditIndex;
                int val = int.Parse(((TextBox)this.gvCarrito.Rows[e.NewEditIndex].Cells[4].FindControl("txtCant")).Text);
                int cant = val;
                Double total = 0;
                DataTable dt = (DataTable)Session["Pedido"];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    if (i == e.NewEditIndex)
                    {
                        if (cant > Convert.ToInt32(dr["stock"]))
                        {
                            ((TextBox)this.gvCarrito.Rows[e.NewEditIndex].Cells[4].FindControl("txtCant")).Text = dr["cantidad"].ToString();
                            ScriptMessage("La cantidad no debe superar el Stock");
                        }
                        if (cant <= 0)
                        {
                            ((TextBox)this.gvCarrito.Rows[e.NewEditIndex].Cells[4].FindControl("txtCant")).Text = dr["cantidad"].ToString();
                            ScriptMessage("La cantidad debe ser mayor a 0");
                        }

                        else
                        {
                            dr["cantidad"] = cant;
                            dr["SubTotal"] = (cant * Convert.ToDouble(dr["Precio"]));
                            Session["Pedido"] = dt;

                            DataTable dtDetalle = (DataTable)Session["PedidoSQL"];
                            foreach (DataRow item in dtDetalle.Rows)
                            {
                                if (Convert.ToInt32(item["ProId"]) == Convert.ToInt32(dr["idproducto"]))
                                {
                                    item["Cantidad"] = cant;
                                }
                            }
                            gvCarrito.DataSource = dt;
                            gvCarrito.DataBind();
                        }
                    }
                }
                foreach (DataRow drr in dt.Rows)
                {
                    total += Convert.ToDouble(drr["SubTotal"]);
                }
                Session["Total"] = total;
            }
            catch (Exception ex)
            {
                e.NewEditIndex = -1;
                ScriptMessage(ex.Message);
            }
            e.NewEditIndex = - 1;
        }
        private void ScriptMessage(string msg)
        {
            string script = @"<script type='text/javascript'>alert('"+msg.ToString()+"');</script>";
            ScriptManager.RegisterStartupScript(this, typeof(Page),"alertmsg", script, false);
            return;
        }
    }
}