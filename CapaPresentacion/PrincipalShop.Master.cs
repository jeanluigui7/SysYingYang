﻿using CapaEntidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaNegocio;
namespace CapaPresentacion
{
    public partial class PrincipalShop : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) {
              ListarCategorias();  
            }
            ListarCategorias();

        }

        private void ListarCategorias() {

            try
            {
                JavaScriptSerializer sr = new JavaScriptSerializer();
                List<entCategoria> lst = new List<entCategoria>();
                lst = negCategoria.Instancia.ListaCategorias();

                if (lst != null && lst.Count > 0)
                {
                    hfData.Value = sr.Serialize(lst);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            Session.Remove("Cliente");
            Response.Redirect("Index.aspx");
        }
    }
}