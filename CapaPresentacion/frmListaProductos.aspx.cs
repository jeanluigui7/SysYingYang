﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaNegocio;
using CapaEntidades;
using System.Data;

namespace CapaPresentacion
{
    public partial class frmListaProductos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e) //Siempre entra aqui cuando pagina carga
        {
            if (!Page.IsPostBack) //¿La pagina esta cargando por primera vez? :D Solo entra cuando la pag. carga por primera vez
            {
                try
                {
                    int idmarca = Convert.ToInt32(Request.QueryString["idmarca"]);
                    if (idmarca > 0)
                    {
                        dlsProductos.DataSource = negProducto.Instancia.ListarProductosxCategoria(idmarca);
                        dlsProductos.DataBind();
                    }
                    else {
                        string script = @"<script type='text/javascript'>alert('" + "Seleccione una Categoria para ver Productos" + "');</script>";
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, false);
                    }
                }
                catch (ApplicationException ae)
                {
                    string script = @"<script type='text/javascript'>alert('" + ae.Message + "');</script>";
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, false);
                }
            }
        }

        private void CrearTablaPedido()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("idproducto", Type.GetType("System.Int32"));
                dt.Columns.Add("cantidad", Type.GetType("System.Int32"));
                dt.Columns.Add("precio", Type.GetType("System.Double"));
                dt.Columns.Add("dproducto", Type.GetType("System.String"));
                dt.Columns.Add("codigo", Type.GetType("System.String"));
                dt.Columns.Add("marca", Type.GetType("System.String"));
                dt.Columns.Add("SubTotal", Type.GetType("System.Double"));
                dt.Columns.Add("imagen", Type.GetType("System.String"));
                dt.Columns.Add("Numero", Type.GetType("System.Int32"));
                dt.Columns["Numero"].AutoIncrement = true;
                dt.Columns["Numero"].AutoIncrementSeed = 1;
                dt.Columns["Numero"].AutoIncrementStep = 1;
                dt.Columns.Add("stock", Type.GetType("System.Int32"));

                Session["Pedido"] = dt;

                DataTable dtsql = new DataTable();
                dtsql.Columns.Add("ProId", Type.GetType("System.Int32"));
                dtsql.Columns.Add("Precio", Type.GetType("System.Double"));
                dtsql.Columns.Add("Cantidad", Type.GetType("System.Int32"));

                Session["PedidoSQL"] = dtsql;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Boolean VerificarSiExiste(DataTable dt, Int32 idprod)
        {
            Boolean yaExiste = false;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                if (idprod == Convert.ToInt32(dr["idproducto"]))
                {
                    yaExiste = true;
                    break;
                }
            }
            return yaExiste;
        }

        protected void dlsProductos_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "btnComprar")
            {
                if (Convert.ToInt32(((TextBox)e.Item.FindControl("txtCantidad")).Text) > Convert.ToInt32(((Label)e.Item.FindControl("lblStock")).Text))
                {
                    string script = @"<script type='text/javascript'>alert('" + "La cantidad supera el stock" + "');</script>";
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alertastock", script, false);
                    return;
                }
                if (Convert.ToInt32(((TextBox)e.Item.FindControl("txtCantidad")).Text) <= 0)
                {
                    string script = @"<script type='text/javascript'>alert('" + "La cantidad debe ser mayor a 0" + "');</script>";
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alertastock0", script, false);
                    return;
                }
                if (Session["Pedido"] == null)
                {
                    CrearTablaPedido();
                }
                DataTable dt = (DataTable)Session["Pedido"];
                Int32 idprod = Convert.ToInt32(((Label)e.Item.FindControl("lblIdProducto")).Text);

                if (VerificarSiExiste(dt, idprod))
                {
                    string script = @"<script type='text/javascript'>alert('"+ "El producto ya Existe" +"');</script>";
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alertaExiste", script, false);
                    return;
                }

                /*********GRID VIEW************/
                DataRow fila = dt.NewRow();
                fila["idproducto"] =((Label)e.Item.FindControl("lblIdProducto")).Text;
                fila["cantidad"] = ((TextBox)e.Item.FindControl("txtCantidad")).Text;
                fila["precio"] = ((Label)e.Item.FindControl("lblPrecio")).Text;
                fila["dproducto"] = ((Label)e.Item.FindControl("lblNombrePro")).Text;
                fila["codigo"] = ((Label)e.Item.FindControl("lblCodigo")).Text;
                fila["marca"] = ((Label)e.Item.FindControl("lblCategoria")).Text;

                double subtotal = Convert.ToDouble(((Label)e.Item.FindControl("lblPrecio")).Text) * Convert.ToInt32(((TextBox)e.Item.FindControl("txtCantidad")).Text);
                fila["SubTotal"] = subtotal;
                fila["stock"] = ((Label)e.Item.FindControl("lblStock")).Text;
                fila["imagen"] = ((Image)e.Item.FindControl("imgProducto")).ImageUrl;

                Session["Total"] = Convert.ToDouble(Session["Total"]) + subtotal;
                dt.Rows.Add(fila);

                /*****TABLA TYPE*********/
                DataTable dty = (DataTable)Session["PedidoSQL"];
                DataRow filaty = dty.NewRow();
                filaty["ProId"] = ((Label)e.Item.FindControl("lblIdProducto")).Text;
                filaty["Precio"] = ((Label)e.Item.FindControl("lblPrecio")).Text;
                filaty["Cantidad"] = ((TextBox)e.Item.FindControl("txtCantidad")).Text;

                dty.Rows.Add(filaty);
                Session["CountProductos"] = dt.Rows.Count;
                Response.Redirect("frmCarritoCompra.aspx");
            }
        }

      
    }
}