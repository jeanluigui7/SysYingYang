﻿using CapaEntidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaNegocio;
using CapaEntidades;
using System.Web.Services;
using CapaPresentacion.src;
using System.Web.Script.Serialization;
using System.IO;
using Library;

namespace CapaPresentacion
{
    public partial class frmSavePersona : System.Web.UI.Page
    {
        public Int32 vsIDP
        {
            get { return ViewState["vsIDP"] != null ? (Int32)ViewState["vsIDP"] : default(Int32); }
            set { ViewState["vsIDP"] = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ListarUbigeo();
                ListarTipoDocumento();
                SetQuery();
                SetData();
            }
        }

        private void SetData()
        {
            entPersona objp = null;
            if (vsIDP > 0)
            {
                objp = negPersona.Instancia.ListarPersonaxId(vsIDP);
                if (objp != null)
                {
                    hfIdPersona.Value = Convert.ToString(vsIDP);
                    hfIdDireccion.Value = Convert.ToString(objp.DirId);
                    SetControls(objp);
                }
            }
            else
            {
                hfIdPersona.Value = "0";
            }
        }

        private void SetControls(entPersona obj)
        {
            txtNombre.Text = obj.PerNombre;
            txtApellidoMaterno.Text = obj.PerApellidoM;
            txtApellidoPaterno.Text = obj.PerApellidoP;
            txtEdad.Text = obj.PerEdad;
            txtEmail.Text = obj.PerEmail;
            cboSexo.SelectedValue = obj.PerSexo;
            txtCelular.Text = obj.PerCelular;
            txtTelefono.Text = obj.PerTelefono;
            ImgFoto.ImageUrl = "~/Fotos/" + obj.PerFoto;
            cboTipoDocumento.SelectedValue = Convert.ToString(obj.TipoDocId);
            txtNroDocumento.Text = obj.NroDocumento;
            cboPais.SelectedValue = Convert.ToString(obj.PaisId);
            cboDepartamento.SelectedValue = Convert.ToString(obj.DepId);
            cboProvincia.SelectedValue = Convert.ToString(obj.ProvId);
            cboDistrito.SelectedValue = Convert.ToString(obj.DistId);
            chkActivo.Checked = Convert.ToBoolean(Convert.ToInt32(obj.PerEstado));
            txtDireccion1.Text = obj.Direccion.DirDescripcion1;
            txtDireccion2.Text = obj.Direccion.DirDescripcion2;
            txtCodPostal.Text = obj.Direccion.DirCodPostal;
        }

        private void SetQuery()
        {
            if (!String.IsNullOrEmpty(Request.QueryString["q"]))
            {
                String id = clsEncryption.Decrypt(Request.QueryString["q"]);
                if (id != String.Empty)
                {
                    vsIDP = Convert.ToInt32(id);
                    this.ltTitle.Text = "Editar Persona";
                }
                else
                {
                    this.ltTitle.Text = "Agregar Persona";
                }
            }
            else {
                this.ltTitle.Text = "Agregar Persona";
            }
        }

        private void ListarTipoDocumento()
        {
            DataTable dt = negTipoDocumento.Instancia.ListarTiposDocumento();
            if (dt != null && dt.Rows.Count > 0)
            {
                cboTipoDocumento.DataSource = dt;
                cboTipoDocumento.DataTextField = "NOMBRE";
                cboTipoDocumento.DataValueField = "ID";
                cboTipoDocumento.DataBind();

                ddlMarket.DataSource = dt;
                ddlMarket.DataTextField = "NOMBRE";
                ddlMarket.DataValueField = "ID";
                ddlMarket.DataBind();
            }
        }

        private void ListarUbigeo()
        {
            try
            {
                List<DataTable> dt = negUbigeo.Instancia.ListarUbigeo();
                if (dt.Count > 0)
                {
                    for (int i = 0; i < dt.Count; i++)
                    {
                        DataTable tabla = dt[i];
                        switch (tabla.TableName)
                        {
                            case "Paises":
                                ComboPais(tabla);
                                break;

                            case "Departamentos":
                                ComboDepartamentos(tabla);
                                break;

                            case "Provincias":
                                Session["Provincias"] = tabla;
                                //var consultaProvincias = from t in tabla.AsEnumerable() where t.Field<int>("DepId") == 1 select new { ProvId = t.Field<int>("ProvId"), ProvNombre = t.Field<string>("ProvNombre") };
                                //ComboProvincias(consultaProvincias);
                                ComboProvincias(tabla);
                                break;

                            case "Distritos":
                                Session["Distritos"] = tabla;
                                //var consultaDistritos = from t in tabla.AsEnumerable() where t.Field<int>("ProvId") == 1 select new { DistId = t.Field<int>("DistId"), DistNombre = t.Field<string>("DistNombre") };
                                //ComboDistritos(consultaDistritos);
                                ComboDistritos(tabla);
                                break;
                        }
                    }
                }
            }
            catch (ApplicationException msg)
            {
                string script = @"<script type='text/javascript'>alert('" + msg.Message + "')</script>";
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, false);
            }
        }

        public void ComboPais(object dtPa)
        {
            cboPais.DataTextField = "PaisNombre";
            cboPais.DataValueField = "PaisId";
            cboPais.DataSource = dtPa;
            cboPais.DataBind();
        }
        public void ComboDepartamentos(object dtDe)
        {
            cboDepartamento.DataTextField = "DepNombre";
            cboDepartamento.DataValueField = "DepId";
            cboDepartamento.DataSource = dtDe;
            cboDepartamento.DataBind();
        }
        public void ComboProvincias(object dtPr)
        {
            cboProvincia.DataTextField = "ProvNombre";
            cboProvincia.DataValueField = "ProvId";
            cboProvincia.DataSource = dtPr;
            cboProvincia.DataBind();
        }
        public void ComboDistritos(object dtDi)
        {
            cboDistrito.DataTextField = "DistNombre";
            cboDistrito.DataValueField = "DistId";
            cboDistrito.DataSource = dtDi;
            cboDistrito.DataBind();
        }

        [WebMethod(EnableSession = true)]
        public static object SeleccionarDepartamento(String DepId)
        {
            DataTable dtPro = (DataTable)HttpContext.Current.Session["Provincias"];
            var consultaPro = from dt in dtPro.AsEnumerable() where dt.Field<int>("DepId") == Convert.ToInt32(DepId) select new { ProvId = dt.Field<int>("ProvId"), ProvNombre = dt.Field<string>("ProvNombre") };

            //ComboProvincias(consultaPro);

            int idPro = consultaPro.FirstOrDefault().ProvId;
            DataTable dtDist = (DataTable)HttpContext.Current.Session["Distritos"];
            var consultaDist = from dt in dtDist.AsEnumerable() where dt.Field<int>("ProvId") == idPro select new { DistId = dt.Field<int>("DistId"), DistNombre = dt.Field<string>("DistNombre") };
            //ComboDistritos(consultaDist);

            return new
            {
                Result = "Ok",
                Msg = "Saved Successfully",
                consultaPro = consultaPro,
                consultaDist = consultaDist,
            };
        }

        [WebMethod(EnableSession = true)]
        public static object SeleccionarProvincia(String ProvId)
        {
            DataTable dtDist = (DataTable)HttpContext.Current.Session["Distritos"];
            var consultaDistXPro = from dt in dtDist.AsEnumerable() where dt.Field<int>("ProvId") == Convert.ToInt32(ProvId) select new { DistId = dt.Field<int>("DistId"), DistNombre = dt.Field<string>("DistNombre") };
            //ComboDistritos(consultaDist);

            return new
            {
                Result = "Ok",
                Msg = "Saved Successfully",
                consultaDistXPro = consultaDistXPro,
            };
        }
        [WebMethod]
        public static object GuardarPersona(String data)
        {
            try
            {
                Boolean success = false;
                entPersona objp = new entPersona();
                entDireccion objD = new entDireccion();
                JavaScriptSerializer sr = new JavaScriptSerializer();
                srPersona objSrP = sr.Deserialize<srPersona>(data);

                objp.PaisId = objSrP.PaisId;
                objp.DistId = objSrP.DistId;
                objp.ProvId = objSrP.ProvId;
                objp.DepId = objSrP.DepId;
                objp.TipoDocId = objSrP.TipoDocId;
                objp.NroDocumento = objSrP.NumDoc;
                objp.PerNombre = objSrP.Nombre;
                objp.PerApellidoP = objSrP.ApellidoP;
                objp.PerApellidoM = objSrP.ApellidoM;
                objp.PerEdad = objSrP.Edad;
                objp.PerSexo = objSrP.Sexo;
                objp.PerEmail = objSrP.Email;
                objp.PerTelefono = objSrP.Telefono;
                objp.PerCelular = objSrP.Celular;
                objp.PerFoto = objSrP.Foto;
                objp.PerEstado = objSrP.EstadoP;

                objD.DirDescripcion1 = objSrP.DirDescripcion1;
                objD.DirDescripcion2 = objSrP.DirDescripcion2;
                objD.DirCodPostal = objSrP.DirCodPostal;
                objD.DirEstado = objSrP.DirEstado;

                objp.PerId = objSrP.PerId;
                objD.DirId= objSrP.DirId;
                if (Convert.ToInt32(objp.PerId) > 0 && objD.DirId > 0)
                {
                    success = negPersona.Instancia.ActualizarPersona(objp, objD);
                }
                else {
                    success = negPersona.Instancia.RegistrarPersonas(objp, objD);
                }
                if (success)
                {
                    return new { Result = "Ok", Msg = "Saved Successfully" };
                }
                else{
                    return new { Result = "NoOk", Msg = "Error al guardar la Data" };
                }
            }
            catch (Exception ex)
            {
                return new { Result = "NoOk", Msg = "Error al guardar la Data" };
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (FU1.HasFile)
            {
                String ext = Path.GetExtension(FU1.FileName).ToLower();
                if (ext == ".jpg" || ext == ".gif" || ext == ".jpeg" || ext == ".png")
                {
                    String Name = FU1.FileName;
                    String uploadFilePath = Server.MapPath("~/Fotos/" + Name);
                    FU1.SaveAs(uploadFilePath);
                    ImgFoto.ImageUrl = "~/Fotos/" + Name;
                    hfNameFoto.Value = Name;
                }

            }
        }
    }
}