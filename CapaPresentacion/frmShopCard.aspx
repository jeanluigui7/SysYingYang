﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrincipalShop.Master" AutoEventWireup="true" CodeBehind="frmShopCard.aspx.cs" Inherits="CapaPresentacion.frmShopCard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="Estilos/js/jquery-1.11.1.min.js"></script>
    <script src="Estilos/js/Base.js"></script>
    <script type="text/javascript">
        
        $(function () {
            $("#DivLateralIzq").hide();
            $("#RowContentProducts").removeClass();
            $("#DivBox").removeClass();
            //$("#DivBox").hide();
            
        });

        $(document).ready(function () { //Carga primero el html luego las funciones de jquery
            CreateProductoItem();
            
        });

        $(document).on('change', '#txtCantidad', function () {
        
            var ProductId = $(this).closest("tr").find(".productid").text();
            var NewQuantity = $(this).val();
            
            var data = "{\"ID\":\"" + ProductId + "\",\"NewQuantity\":\"" + NewQuantity+"\"}";

            try {
                var success = function (asw) {
                    CreateProductoItem();
                    fn_SetTotals(asw.d);
                }

                var error = function (jqXHR, textStatus, errorThrown) {
                    fn_message('e', 'An error occurred while loading data');
                }

            } catch (e) {
                fn_message('e', 'An error occurred while loading data');
            }
            fn_callmethod("frmShopCard.aspx/ChangeQuantityProduct",  data, success, error);

            //alert("Id : " + ProductId + "Cantidad:" + NewCant);

        });
       
        function CreateProductoItem() {
            try {
                var success = function (asw) {
                    if (asw != null) {
                        
                        var lstDetail = JSON.parse(asw.d.lstDetail)
                        var cadena = '';
                        for (var i = 0; i < lstDetail.length; i++) {

                            cadena += " <tr>";
                            cadena += " <td>";
                            cadena += " <a href='#'>";
                            cadena += " <img src='Imagenes/" + lstDetail[i].Products.ProImagen + "' alt='" + lstDetail[i].Products.ProNombre + "'>";
                            cadena += " </a>"
                            cadena += " </td>"
                            cadena += " <td><a href='#'>" + lstDetail[i].Products.ProNombre + "</a>"
                            cadena += " </td>"
                            cadena += " <td>"
                            cadena += " <input id='txtCantidad' type='number' min='1' value=" + lstDetail[i].Cantidad + " class='form- control' pattern='^[0-9]+'>"
                            cadena += " </td>"
                            cadena += " <td>S/. " + lstDetail[i].Products.Precio + "</td>"
                            cadena += " <td>S/.0.00</td>"
                            cadena += " <td id='TotalProduct'>S/. " + (lstDetail[i].Products.Precio * lstDetail[i].Cantidad) + "</td>"
                            cadena += " <td class='clsIdPro'><a onClick='fn_RemoveProduct(" + lstDetail[i].Products.ProId +");'><i class='fa fa-trash-o'></i></a>"
                            cadena += " </td>"
                            cadena += " <td class='productid' style='display:none'>" + lstDetail[i].Products.ProId + "</td>"
                            cadena += " </tr>"

                            $("#ProductoTbody").html(cadena);
                        }
                        fn_SetTotals(asw.d);
                        
                    }
                   
                };
                var error = function (jqXHR, textStatus, errorThrown) {
                    fn_message('e', 'An error occurred while loading data');
                };
               
            } catch (e) {
                fn_message('e', 'An error occurred while loading data');
            }
            var trim = "";
            fn_callmethod("frmShopCard.aspx/ProductAddtoList", '{ trim : "' + trim +'" }', success, error);
        }

        function fn_SetTotals(Data) {
            $("#lblTotalItems").text("You currently have ( " + JSON.parse(Data.lstDetail).length + " ) item(s) in your cart.");
            $("#lblTotal").text("S/." + Data.Total);
            $("#lblTotalResumen").text("S/." + Data.Total);
            $("#lblSubtotal").text("S/." + Data.Total);
        }

        function fn_RemoveProduct(idproduct) {
            try {

                var success = function (asw) {

                    if (asw.d == "true") {
                        $("#ProductoTbody").html("");
                        CreateProductoItem();
                        fn_SetTotals(asw.d);
                    } else {
                        fn_message('e', 'An error occurred while deleting data');
                    }

                }

                var error = function (jqXHR, textStatus, errorThrown) {
                    fn_message('e', 'An error occurred while deleting data');
                }

            } catch (e) {
                fn_message('e', 'An error occurred while deleting data');
            }

            fn_callmethod("frmShopCard.aspx/DeleteProduct", JSON.stringify({ idpro: idproduct }), success, error);
        }

        function fn_Redirect() {

            window.location.href = "ProccessAddress.aspx";
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="content">
        <div class="container">

            <div class="col-md-9" id="basket">
                <div class="box">
                    <h1>Shopping cart</h1>
                    <p class="text-muted" id="lblTotalItems">You currently have (N) item(s) in your cart.</p>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th colspan="2">Product</th>
                                    <th>Quantity</th>
                                    <th>Unit price</th>
                                    <th>Discount</th>
                                    <th colspan="2">Total</th>
                                </tr>
                            </thead>
                            <tbody id="ProductoTbody">
                             <%--   <tr>
                                    <td>
                                        <a href="#">
                                            <img src="Source/img/detailsquare.jpg" alt="White Blouse Armani">
                                        </a>
                                    </td>
                                    <td><a href="#">White Blouse Armani</a>
                                    </td>
                                    <td>
                                        <input type="number" value="2" class="form-control">
                                    </td>
                                    <td>$123.00</td>
                                    <td>$0.00</td>
                                    <td>$246.00</td>
                                    <td><a href="#"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>--%>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="5">Total</th>
                                    <th colspan="2" id="lblTotal">$0.00</th>
                                </tr>
                            </tfoot>
                        </table>

                    </div>
                    <!-- /.table-responsive -->

                    <div class="box-footer">
                        <div class="pull-left">
                            <a href="Index.aspx" class="btn btn-default"><i class="fa fa-chevron-left"></i>Continue shopping</a>
                        </div>
                        <div class="pull-right">
                            <button class="btn btn-default"><i class="fa fa-refresh"></i>Update basket</button>
                            <button type="button" class="btn btn-primary" onclick="fn_Redirect();">
                                Proceed to checkout <i class="fa fa-chevron-right"></i>
                            </button>
                        </div>
                    </div>

                </div>
                <!-- /.box -->


            </div>
            <!-- /.col-md-9 -->

            <div class="col-md-3">
                <div class="box" id="order-summary">
                    <div class="box-header">
                        <h3>Order summary</h3>
                    </div>
                    <p class="text-muted">Shipping and additional costs are calculated based on the values you have entered.</p>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>Order subtotal</td>
                                    <th id="lblSubtotal">S/.0.00</th>
                                </tr>
                                <tr>
                                    <td>Shipping and handling</td>
                                    <th>S/.0.00</th>
                                </tr>
                                <tr>
                                    <td>Tax</td>
                                    <th>S/.0.00</th>
                                </tr>
                                <tr class="total">
                                    <td>Total</td>
                                    <th id="lblTotalResumen">S/.0.00</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>


                <div class="box">
                    <div class="box-header">
                        <h4>Coupon code</h4>
                    </div>
                    <p class="text-muted">If you have a coupon code, please enter it in the box below.</p>
                    <form>
                        <div class="input-group">

                            <input type="text" class="form-control">

                            <span class="input-group-btn">

                                <button class="btn btn-primary" type="button"><i class="fa fa-gift"></i></button>

                            </span>
                        </div>
                        <!-- /input-group -->
                    </form>
                </div>

            </div>
            <!-- /.col-md-3 -->

        </div>
        <!-- /.container -->
    </div>
</asp:Content>
